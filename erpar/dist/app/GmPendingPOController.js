/* 
 * gmcOrdersPendingPO.js : Controller Class used for the A/R Order Pending PO functionalities
 * @author Jayaprasanth Gurunathan
 */
var gridObj;
let datacollection;
var errMsg = "Something went wrong please try again later";
app.controller('GmPendingPOController', function ($scope, $http, $q,$timeout,$filter,$rootScope,ParentScopes) {
    var AfterAuthentication = function () {
        var deferred = $q.defer();
        while (!KeycloakService.keycloak.authenticated) { }
        deferred.resolve(KeycloakService.keycloak.authenticated);
        return deferred.promise;
    }
    ParentScopes.set('GmPendingPOController',$scope);
    /* Following functions are used from GmDiscrepancyController.js(on) for avoid duplicate codes*/
    /*This used for Discrepancy Dashboard*/
     $rootScope.$on("fnLoadPOEntry", function (event, data) {
         $scope.strOrderId = data;
         $scope.fnLoadPOEntry();
     });
     $rootScope.$on("fnFetchPODetails", function (event, data) {
         $scope.fnFetchPODetails();
     });
     $rootScope.$on("fnFetchComments", function (event) {
         $scope.fnFetchComments();
     });
     $rootScope.$on("fnLoadFiles", function (event,data) {
         $scope.AccessFl = data;
         $scope.fnLoadFiles();
     });
     $rootScope.$on("auditLogModal", function (event) {
         $scope.auditLogModal();
     });
     $rootScope.$on("poDrillDownModal", function (event) {
         $scope.poDrillDownModal();
     });
     $rootScope.$on("fnsaveOrderRevDtls", function (event) {
         $scope.fnsaveOrderRevDtls();
     });
     $rootScope.$on("fnPOCheck", function (event) {
         $scope.fnPOCheck();
     });
     $rootScope.$on("fnSavePOCheck", function (event) {
         $scope.fnSavePOCheck();
     });
     $rootScope.$on("confirmPONumber", function (event) {
         $scope.confirmPONumber();
     });
     $rootScope.$on("ConfirmPOCancel", function (event) {
         $scope.ConfirmPOCancel();
     });
     $rootScope.$on("fnSavePONum", function (event,data) {
        $scope.strPONumDtlsId = data;
         $scope.fnSavePONum();
     });
     $rootScope.$on("fnPOAmtSave", function (event,data) {
         $scope.strPONumDtlsId = data;
         $scope.fnPOAmtSave();
     });
     $rootScope.$on("fnPOAmtValidation", function (event) {
         $scope.fnPOAmtValidation();
     });
     $rootScope.$on("fnFetchPODrillDown", function (event) {
         $scope.fnFetchPODrillDown();
     });
     $rootScope.$on("saveLogComments", function (event) {
         $scope.saveLogComments();
     });
     $rootScope.$on("fnClearComments", function (event) {
         $scope.fnClearComments();
     });
     $rootScope.$on("fnRefreshRightPanel", function (event) {
         $scope.fnRefreshRightPanel();
     });
     $rootScope.$on("fnCloseRightPanel", function (event) {
         $scope.fnCloseRightPanel();
     });
     $rootScope.$on("fnClearPONum", function (event) {
         $scope.fnClearPONum();
     });
     $rootScope.$on("fnDownloadDOPdfURL", function (event,data) {
          $scope.strOrderId = data;
         $scope.fnDownloadDOPdfURL();
     });
    $rootScope.$on("fnValidateSavePOStatus", function (event,data) {
         $scope.fnValidateSavePOStatus(data);
     });
    $rootScope.$on("confirmPOStatus", function (event,data) {
         $scope.status = data;
         $scope.confirmPOStatus();
     });
    $(document).ready(function () {
        //this function is used to active left link class
       activeLeftLinkClass();
        $http.get(varJsonURL).then(function (response) { }, function (response) {
            $scope.errorMsg = response.data.message;
            sessionStorage.setItem("errorMsg", ($scope.errorMsg));
            $scope.errorHandler();
        });

        $("#po-entry-main").hide();
        
        //this is for loader functionality
        $("#preloader-spin").hide();
        $("#PODetails").hide();
        $("#preloader-spin-po").hide();
        $('.loader-hide').hide();
        $('#preloader-for-po').hide();
        $('#preloader-for-do').hide();
        $('#preloader-for-comments').hide();
        $('#preloader-for-poamt').hide();
        $('#preloader-for-ponum').hide();
         $('#reject-po').hide();
         $('#no-data-found').hide();
         $('#orders-pending-po-results').hide();
         $('.dhx_sample-controls').hide();

        var varLoginName = localStorage.getItem('strLoginName');
        var strPartyId = "";
        var strUserId = "";
        var strCompId = "";
        $scope.poSaveFl = 'Y';
        //Read/Write Access
        $scope.AccessFl = 'N';
        $scope.errorMessage = '';
        
        /*Web Service Call for party details from user name*/
        fnLoadUserInfo($http, varLoginName, function (response) {
            if (response.data != undefined) {
                strCompId = response.data[0].strCompanyId;
                strPartyId = response.data[0].strPartyId;
                strUserId = response.data[0].strUserId;
                localStorage.setItem("strPartyId", strPartyId);
                localStorage.setItem("strCompId", strCompId);
                localStorage.setItem("strUserId", strUserId);
                /*Web Service Call for Load Company details  based on login user*/
                fnLoadCompany($http, strPartyId, function (response) {
                    if (response.data != undefined) {
                        $scope.compList = response.data;
                        /*Web Service Call for company information from company id*/
                        fnLoadCompanyInfo($http, strCompId, function (response) {
                            if (response.data != undefined) {
                                $scope.selectedCompany = strCompId;
                                $scope.compInfoList = response.data;
                                $scope.strCurrency = $scope.compInfoList[0].currency;
                                $scope.strcurrencyNm = $scope.compInfoList[0].currencyNm;
                                $scope.strDtFormat = $scope.compInfoList[0].dtFormat;
                                $scope.strCmpCurrFmt = $scope.compInfoList[0].currencyFormat;
                                $scope.strCompanyId = $scope.compInfoList[0].companyId;
                                $scope.strCompanyZone = $scope.compInfoList[0].compZone;
                                $scope.strLanguageId = $scope.compInfoList[0].languageId;
                                $scope.strCompanyLocale = $scope.compInfoList[0].companyLocale;
                                $scope.strPlantId = $scope.compInfoList[0].plantId;
                                localStorage.setItem("strLangId", $scope.strLanguageId);
                                localStorage.setItem("strPlantId", $scope.strPlantId);
                                $scope.fnSetDates($scope.strDtFormat);
                            }
                        });
                    } else {
                        $scope.errorMsg = response.data.message;
                        sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                        $scope.errorHandler();
                    }
                });

                /* This function used to get the dropdown values for PO Status DropDown */
                fnCodeLookUpValuesExclude($http, $scope, 'DOPOST', '', strCompId, function (response) {
                    $scope.strPOStatusList = response.data;
                    console.log("$scope.strPOStatusList=>"+$scope.strPOStatusList);
                });

                /*This function is used to get the update access flag for based on PartyId*/
                fnSecurityEvent($http, strPartyId, 'ORD_PO_ENTRY_ACC', function (secresponse) {
                    if (secresponse.data != undefined && secresponse.data != '') {
                        if (secresponse.data[0].updateAccessFl != undefined) {
                            $scope.AccessFl = secresponse.data[0].updateAccessFl;
                        }
                    }
                });

            } else {
                $scope.errorMsg = response.data.message;
                sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                $scope.errorHandler();
            }
        });

        /*Web Service Call for Load Company details  based on login user*/
        fnLoadDivision($http, function (response) {
            if (response.data.status == undefined) {
                $scope.divisionList = response.data;
            } else {
                $scope.errorMsg = response.data.message;
                sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                $scope.errorHandler();
            }
        });
        
        /* This function used to get the dropdown values for PO*/

        fnCodeLookUpValues($http,$scope,'REVST',function(podropres){
            $scope.poDtlList = podropres.data;
        });
        
        /* This function used to get the dropdown values for DO*/
        
        fnCodeLookUpValues($http,$scope,'RSTDO',function(dodropres){
            $scope.doDtlList = dodropres.data;
        });
        
        /* This function used to get the dropdown values for Reject DropDown value*/
        
        fnCodeLookUpValues($http,$scope,'REDOPO',function(rejdropres){
             $scope.rejCommentsLogList = rejdropres.data;
        });
        
        /* This function used to get the dropdown values for order mode DropDown value*/
        var ordMode = '';
        var ordIpadMode= '';

        fnCodeLookUpValues($http,$scope,'ORDMO',function(response){
        	ordMode = response.data;
            fnCodeLookUpValues($http,$scope,'ORDMOD',function(response){
        	ordIpadMode= response.data;
        	$scope.strOrderModeList = ordMode.concat(ordIpadMode);
            });
        });

        
        
        

        $http.get(varJsonURL).then(function (response) {
            varDateCompforSurgery = response.data.dateComparisonforsurgery;
            varDateCompforOrder = response.data.dateComparisonforOrder;
            varSurgeryFromDtMsg = response.data.emptyFromDateforSurgery;
            varSurgeryToDtMag = response.data.emptyToDateforSurgery;
            varOrderFromDtMsg = response.data.emptyFromDateforOrder;
            varOrderToDtMsg = response.data.emptyToDateforOrder;
            varTotalMsg = response.data.varTotalMsg;
        }, function (response) {
            $scope.errorMsg = response.data.message;
            sessionStorage.setItem("errorMsg", ($scope.errorMsg));
            $scope.errorHandler();
        });


    });


    /*Function is used to load report*/
    $scope.fnLoadOrdersPedingPOReport = function () {
        var forms = document.getElementsByClassName('needs-validation');
        $("#pending-po-main").removeClass("col-9").addClass("col-12");
        $("#discrepancy-main").removeClass("col-9").addClass("col-12");
	    $("#po-entry-main").addClass("col-3").hide();

        // Get all form-groups in need of validation
        var validateGroup = document.getElementsByClassName('validate-me');

        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                
                if (form.checkValidity() != false) {
                    $scope.fnClearRedisId();
                    $scope.fnLoadPOReport();
                } else {
                    event.preventDefault();
                    event.stopPropagation();
                }

                //Added validation class to all form-groups in need of validation
                for (var i = 0; i < validateGroup.length; i++) {
                    validateGroup[i].classList.add('was-validated');
                }

            }, false);
        });
    };

    /*Function is used to load the PO report*/
    $scope.fnLoadPOReport = function () {

        var varSurFromDate = "";
        var varSurToDate = "";
        var varOrdFromDate = "";
        var varOrdToDate = "";
        var varTotal = "";
        var varOrderValidateFl = false;
        var varSurgeryValidateFl = false;
        var varValidateMsg = "";
        var varSurDtErrFl = false;
        var varOrdDtErrFl = false;
        var varTotalFl = false;

        var varSurgeryFromDtErrFl = false;
        var varSurgeryToDtErrFl = false;
        var varOrderFromDtErrFl = false;
        var varOrderToDtErrFl = false;

        varSurFromDate = $scope.strSurgeryFDate;
        varSurToDate = $scope.strSurgeryTDate;
        varOrdFromDate = $scope.strOrderFDate;
        varOrdToDate = $scope.strOrderTDate;
        varTotal = $scope.strTotalAmount;

        $("#span-empty_surFdt-err").hide();
        $("#span-empty_surTdt-err").hide();
        $("#span-empty_ordFdt-err").hide();
        $("#span-empty_ordTdt-err").hide();
        $("#span-dt_compare_surFdt-err").hide();
        $("#span-dt_compare_ordFdt-err").hide();
        $("#span-total-err").hide();



        //validate From Date of Surgery Field and assign result to Flag
        if (varSurFromDate == undefined || varSurFromDate == null || varSurFromDate == '') {

            varSurgeryFromDtErrFl = true;
        }
        //validate To Date of Surgery Field and assign result to Flag
        if (varSurToDate == undefined || varSurToDate == null || varSurToDate == '') {

            varSurgeryToDtErrFl = true;
        }
        //validate From Date of Entry Field and assign result to Flag
        if (varOrdFromDate == undefined || varOrdFromDate == null || varOrdFromDate == '') {
            varOrderFromDtErrFl = true;
        }
        //validate To Date of Entry Field and assign result to Flag
        if (varOrdToDate == undefined || varOrdToDate == null || varOrdToDate == '') {
            varOrderToDtErrFl = true;
        }

        //Check each date field and get validation message for missing input field
        if ((varSurgeryFromDtErrFl) && (!varSurgeryToDtErrFl)) {
            $("#span-empty_surFdt-err").show();
            return false;
        }
        if ((!varSurgeryFromDtErrFl) && (varSurgeryToDtErrFl)) {
            $("#span-empty_surTdt-err").show();
            return false;
        }
        if ((varOrderFromDtErrFl) && (!varOrderToDtErrFl)) {
            $("#span-empty_ordFdt-err").show();
            return false;
        }
        if ((!varOrderFromDtErrFl) && (varOrderToDtErrFl)) {
            $("#span-empty_ordTdt-err").show();
            return false;
        }


        //validate Surgery Date and assign result to Flag
        if (varSurFromDate != null && varSurFromDate != undefined && varSurFromDate != '' && varSurToDate != null && varSurToDate != undefined && varSurToDate != '') {
            varSurgeryValidateFl = true;
        }

        //validate Entry Date and assign result to Flag
        if (varOrdFromDate != null && varOrdFromDate != undefined && varOrdFromDate != '' && varOrdToDate != null && varOrdToDate != undefined && varOrdToDate != '') {
            varOrderValidateFl = true;
        }

        //This condition will execute if Validation Surgery flag is true
        if (varSurgeryValidateFl) {
        //dateDiff method is used for identify the date difference for all format 
            dateDiff(varSurFromDate, varSurToDate, $scope.strDtFormat,function (response) {
                if (response != '') {
	                  if (response < 0) {
	                	  varSurDtErrFl = true;
	                	  $("#span-dt_compare_surFdt-err").show();
	                	  return false;
	                  }
                } 
            });
            
        }

        //This condition will execute if Validation Entry flag is true
        if (varOrderValidateFl) {
      	
        	//dateDiff method is used for identify the date difference for all format 
            dateDiff(varOrdFromDate, varOrdToDate, $scope.strDtFormat,function (response) {
                if (response != '') {
	                  if (response < 0) {
	                	  varOrdDtErrFl = true;
	                	  $("#span-dt_compare_ordFdt-err").show();
	                	  return false;
	                  }
                } 
            });
            

        }
        if (isNaN(varTotal) && ((varTotal) != '') && (varTotal != undefined)) {
            varTotalFl = true;
            $("#span-total-err").show();
            return false;
        }
        
        //Load the PO reports
        if((varSurDtErrFl == false) && (varOrdDtErrFl == false)){
	        $("#orders-pending-po-results").html("<div class='w-100 text-center' id='pre-loader-main'><i class='fas fa-spinner fa-pulse fa-2x'></i></div>");
	        $('#orders-pending-po-results').show();
	        $('#no-data-found').hide();
	        
	        /*toggle more button should collapse while click on load button*/
	        if($('#collapseFilter').hasClass('collapse show')){
	    		$('#collapseFilter').removeClass('show');
	    	}
	        
	        if($('#btnCollapseFilter').hasClass('btn btn-xs btn-outline-dark')){
	        	$('#btnCollapseFilter').addClass('btn btn-xs btn-outline-dark collapsed');
	    	}
		//This function is used to split last comma in po number and do id
	       var strPONumber =  $scope.strPONumber;
	       var strOrdId =  $scope.strOrdId;
			if(strPONumber != "" && strPONumber != undefined){
				var lastChar = strPONumber.slice(-1);
				if(lastChar == ',') {
					strPONumber = strPONumber.slice(0, -1);
				}
				$scope.strPONumber = strPONumber.toUpperCase();
			}
			
			if(strOrdId != "" && strOrdId != undefined){
				var lastChar = strOrdId.slice(-1);
				if(lastChar == ',') {
					strOrdId = strOrdId.slice(0, -1);
				}
				$scope.strOrdId = strOrdId.toUpperCase();
            }
            
            // This condition used to set default PO status if Status is not selected
            if($scope.strPOStatusId == undefined || $scope.strPOStatusId == "")
            {
                $scope.strPOStatusIdList = '109540,109541'; // Draft, Pending Approval
            } 
            else
            {
                $scope.strPOStatusIdList = $scope.strPOStatusId;
            }

	        $scope.gmARCustPOModel = {
	            strCompanyId: $scope.strCompanyId,
	            strParentAcc: $scope.strParentAcc,
	            strRepAcc: $scope.strRepAcc,
	            strFieldSales: $scope.strFieldSales,
	            strSalesRep: $scope.strSalesRep,
	            strDivisionId: $scope.strDivisionId,
	            strTotal: $scope.strTotalAmount,
	            strDateType: $scope.strDateType,
	            strStartDate: $scope.strSurgeryFDate,
	            strEndDate: $scope.strSurgeryTDate,
	            strOrderId: $scope.strOrdId,
	            strOrdStartDate: $scope.strOrderFDate,
	            strOrdEndDate: $scope.strOrderTDate,
	            strAccid: $scope.strAccid,
	            strCurrency: $scope.strCurrency,
	            strCmpCurrFmt: $scope.strCmpCurrFmt,
	            strDtFormat: $scope.strDtFormat,
	            strLanguageId: $scope.strLanguageId,
	            strPONumber : $scope.strPONumber,
                strOrdReceiveMode : $scope.strOrderMode,
                strPOStatusList :  $scope.strPOStatusIdList
	        };
	
	        $http.post(url+'loadPOreport/',angular.toJson($scope.gmARCustPOModel)).then(function(response){

	            if (response.data != undefined) {
	                $scope.results = response.data;
	                $scope.fnLoadPOGrid($scope.results);
	            } else {
	                $scope.errorMsg = response.data.message;
	                sessionStorage.setItem("errorMsg", ($scope.errorMsg));
	                $scope.errorHandler();
	            }
	
	        });
        }
    };



    $scope.fnLoadPOGrid = function (response) {
        $('#orders-pending-po-results').empty();
        datacollection = new dhx.DataCollection();

        datacollection.parse(response);

        if (response != '') {
            // Updating new dhtmlx Grid - 6.5.2
            grid = new dhx.Grid("orders-pending-po-results", {
                columns: [{width: 280,id: "strParentAcc",header: [{text: "Parent Account",align: "center"}, { content: "inputFilter"}],align: "left"},
                    {width: 120,id: "strParentAccId",header: [{text: "Parent Account Id",align: "center"}, { content: "inputFilter"}], align: "right"},
                    {width: 180,id: "strParentOrdId",header: [{text: "Parent Order Id",align: "center"}, { content: "inputFilter"}],align: "left"},
                    {width: 70,id: "strHoldFl",header: [],htmlEnable: true,template: function (text, row, col) {
                            if (text == 'Y') {
                                return "<img alt=\"DO on hold status\" title=\"DO on Hold Status\" src=\"/images/hold-icon.png\" border=\"0\" disabled=\"true\">"
                            }
                        },align: "center" },
                    {width: 180,id: "strOrderId",header: [{text: "Order ID",align: "center"}, {content: "inputFilter"
                        }],htmlEnable: true,template: function (text, row, col) {
                            return "<a id='strOrdId' data-orderid='" + text + "' href='javascript:;'><span class='po-update-btn'>" + text + "</span></a></div>";
                        },align: "left"},
                    {width: 130,id: "strOrdDT",header: [{text: "Order Date",align: "center"}, {content: "comboFilter"
                        }],footer: [{text: '<div class="custom_footer">Total</div>'}],align: "left"},
                    {width: 150,id: "strTotal",header: [{text: "Total",align: "center"}, {content: "inputFilter"}],type: "number",template: function (text, row, col) {
                            if (text == 0) {
                                return "0.00"
                            } 
                        else if (text < 0){
                            row.strTotal =  row.strTotal.slice(1);
                            if (row.strTotal % 1 != 0) {
                                    row.strTotal = parseFloat(row.strTotal + '0');
                                    return (row.strTotal).toFixed(2);
                                } else {
                                    return row.strTotal + ".00";
                                }
                        }
                            else {
                                if (row.strTotal % 1 != 0) {
                                    row.strTotal = parseFloat(row.strTotal + '0');
                                    return (row.strTotal).toFixed(2);
                                } else {
                                    return row.strTotal + ".00";
                                }
                            }
                        }, mark: function (cell, data, row, col) { // This is used to add class for cell (Minimum Amount Css)
                            if (row.strTotal < 0)
                                return "amt-min";
                        },footer: [{content: "sum"}],align: "right"},
                    {width: 150,id: "strOrdReceiveMode",header: [{text: "Order Mode",align: "center"}, {content: "selectFilter" }],align: "left"},
                    {width: 180,id: "strPONumber",header: [{text: "PO Number",align: "center"}, {content: "inputFilter"}],align: "left"},
                    {width: 280,id: "strSalesRep",header: [{text: "Sales Rep",align: "center"}, {content: "inputFilter" }],align: "left"},
                    {width: 170,id: "strSurgeryDt",header: [{text: "Surgery Date",align: "center"}, {content: "selectFilter"}],align: "left"},
                    {width: 250,id: "strRepAcc",header: [{text: "Rep Account",align: "center" }, {content: "inputFilter" }],align: "left" },
                    { width: 250,id: "strFieldSales",header: [{text: "Field Sales",align: "center"}, {content: "inputFilter"}],align: "left"},
                    {width: 250,id: "strADName",header: [{text: "AD Name",align: "center"}, {content: "inputFilter"
                        }],align: "left"},
                    {width: 250,id: "strVPName",header: [{text: "VP Name",align: "center"}, {content: "inputFilter"
                        }],align: "left"},
                    {width: 150,id: "strOrdertype",header: [{text: "Order Type",align: "center"}, {content: "selectFilter"}],align: "left"},
                    {width: 160,id: "strAccid",header: [{text: "Rep Account Id",align: "center"}, {content: "inputFilter"
                        }],align: "right"},
                    {width: 150,id: "strPOEnteredDate",header: [{text: "PO Entry Date",align: "center"}, {content: "comboFilter"
                        }],align: "left"},
                   {width: 150,id: "strCustPOUpdatedBy",header: [{text: "PO Entered By",align: "center"}, {content: "inputFilter"
                        }],align: "left"},
                    ],
                data: response,
                resizable: true,
                height: 380,
                dragItem: "column",
                rowHeight: 30,
                keyNavigation: true

            });
            $scope.grid = grid;
            $('.dhx_sample-controls').show();

            /*This function used for handling  events*/
            const events = [
  "afterEditEnd",
   "headerCellClick",
   "cellClick",
   "beforeEditEnd",
   "beforeEditStart"
];
            /*This function for grid event handler*/
            function eventHandler(event, arguments) {

                // Cell click event  to open DO Details
                if (event == 'cellClick') {
                    if (arguments[1].id == "strOrderId") {// while click order id column values open DO details
                        $scope.fnSummaryDO(arguments[0].strOrderId);
                    } else { //used to open right panel
                        $scope.strOrderId = arguments[0].strOrderId;
                        $scope.strParentAccNm = arguments[0].strParentAcc;
                        $scope.poCheckFlag = '';
                        $scope.successMsg = '';
                        $scope.validatePONumMsg = '';
                        $scope.validateCommentsMsg = '';
                        $scope.validatePOAmtMsg = '';
                        $scope.commentsCount = 0;
                        $scope.fnLoadPOEntry(); // Order Details -complted
                        $scope.fnFetchPODetails(); // PO/DO Fetch
                        $scope.fnFetchComments(); // comments fetch
                        $scope.fnLoadFiles();
                    }
                }

            }
            events.forEach(function (event) {
                grid.events.on(event, function () {
                    eventHandler(event, arguments);
                });
            });
             //Used to show/hide default column
            $('input[class="chk_column"]').each(function () {
                if ($(this).prop("checked") == true) {
                    grid.showColumn($(this).val());

                } else {
                    grid.hideColumn($(this).val());
                }
            });
              //Used to show/hide based on checkbox
            $('.chk_column').click(function(){
                if($(this).prop("checked") == true){
                   grid.showColumn($(this).val());

                }
                else{
                   grid.hideColumn($(this).val());
                }
            });
            

        } else {
            $('#no-data-found').show();
           $('.dhx_sample-controls').hide();
            $('#orders-pending-po-results').hide();
        }

    };
 //This Function is used to export excel
    $scope.exportXlsx = function () {
        grid.config.columns.splice(3, 1);
        grid.export.xlsx({
            name: "Order-pending-po-list",
            url: "//export.dhtmlx.com/excel"
        });
        $scope.fnLoadPOGrid($scope.results);
    };

    

    function fnCustomTypes(divRef, gObj, setHeader, setColIds, setInitWidths, setColAlign,
        setColTypes, setColSorting, enableTooltips, setFilter, gridHeight,
        format, footerExportFL) {

        gObj.setHeader(setHeader);
        var colCount = setHeader.length;

        gObj.setInitWidths(setInitWidths);
        gObj.setColAlign(setColAlign);
        gObj.setColTypes(setColTypes);
        gObj.setColSorting(setColSorting);
        gObj.enableTooltips(enableTooltips);
        gObj.attachHeader(setFilter);
        gObj.setColumnIds(setColIds);

        if (footerExportFL)
            fnAttachExptFooter(divRef, colCount, gObj);

        gObj.enableAutoHeight(true, gridHeight, true);

        return gObj;
    }
    
    /*toggle button class add and remove*/
    $scope.fnColapseFilter = function () {
    	if($('#collapseFilter').hasClass('collapse show')){
    		$('#collapseFilter').removeClass('show');
    	}else{
    		$('#collapseFilter').addClass('collapse show');
    	}
    	
    	if($('#btnCollapseFilter').hasClass('btn btn-xs btn-outline-dark collapsed')){
    		$('#btnCollapseFilter').removeClass('collapsed');
    	}else{
    		$('#btnCollapseFilter').addClass('btn btn-xs btn-outline-dark collapsed');
    	}
    	
    };
   
    /*Web Service Call for Parent Account details  based on login user*/
    $scope.fnSearchParentAccount = function () {
        var strSearchValue = $scope.strParentAccId.toLowerCase();
        if (strSearchValue.length >= 3 ) {
        	$scope.hidethisacc = false;
            var strScreenRedisKey = 'parentAccountRedis';
            $http.get(microappUrl+'loadRedisDtls'+'/' + strScreenRedisKey + '/' + strSearchValue + '/' + '100' + '/' + $scope.strCompanyId).then(function (response) {
            	if (response.data != "") {
                    $scope.parentAccList = response.data;
                    $("#list-group-acc").addClass("searchResults");
                }else{
                    $scope.parentAccList = [{"ID":"","NAME":"No Data Found"}];
                }
            });
        }else{
        	$scope.hidethisacc = true;
            $("#list-group-acc").removeClass("searchResults");
        }
    };
    /*Web Service Call for Rep Account details  based on login user*/
    $scope.fnSearchRepAccount = function () {
        var strSearchValue = $scope.strRepAccId.toLowerCase();
        
        if (strSearchValue.length >= 3 ) {
        	$scope.hidethisrep = false;
            var strScreenRedisKey = 'repAccountRedis';
            $http.get(microappUrl+'loadRedisDtls'+'/' + strScreenRedisKey + '/' + strSearchValue + '/' + '100' + '/' + $scope.strCompanyId).then(function (response) {
            	if (response.data != "") {
                    $scope.repAccList = response.data;
                    $("#list-group-rep").addClass("searchResults");
                }else{
                    $scope.repAccList = [{"ID":"","NAME":"No Data Found"}];
                }
            });

        }else{
        	$scope.hidethisrep = true;
            $("#list-group-rep").removeClass("searchResults");
        }
    };
    /*Web Service Call for Field Sales details  based on login user*/
    $scope.fnSearchFieldSales = function () {
        var strSearchValue = $scope.strFieldSalesId.toLowerCase();
        
        if (strSearchValue.length >= 3 ) {
        	$scope.hidethisfield = false;
            var strScreenRedisKey = 'distRedis';
            $http.get(microappUrl+'loadRedisDtls'+'/' + strScreenRedisKey + '/' + strSearchValue + '/' + '10' + '/' + $scope.strCompanyId).then(function (response) {
            	if (response.data != "") {
                    $scope.fieldSalesList = response.data;
                    $("#list-group-field").addClass("searchResults");
                }else{
                    $scope.fieldSalesList = [{"ID":"","NAME":"No Data Found"}];
                }
            });

        }else{
        	$scope.hidethisfield = true;
            $("#list-group-field").removeClass("searchResults");
        }
    };
    /*Web Service Call for Sales Rep details  based on login user*/
    $scope.fnSearchSalesRep = function () {
        var strSearchValue = $scope.strSalesRepId.toLowerCase();
       
        if (strSearchValue.length >= 3 ) {
        	$scope.hidethissales = false;
            var strScreenRedisKey = 'salesRepRedis';
            $http.get(microappUrl+'loadRedisDtls'+'/' + strScreenRedisKey + '/' + strSearchValue + '/' + '10' + '/' + $scope.strCompanyId).then(function (response) {
                if (response.data != "") {
                    $scope.salesRepList = response.data;
                    $("#list-group-sales").addClass("searchResults");
                }else{
                    $scope.salesRepList = [{"ID":"","NAME":"No Data Found"}];
                }
            });

        }else{
        	$scope.hidethissales = true;
            $("#list-group-sales").removeClass("searchResults");
        }
    };

    /*This is used for set the date picker details */
    $scope.fnSetDates = function (strDateFormat) {
        var strDtFormat = (strDateFormat).toLowerCase();
        
        //below line commented for default date setting in month first date and current date
        /*var today = new Date();
	    var monthFDate = ('0'+(today.getMonth()+1)).slice(-2)+'/'+ '01' +'/'+ today.getFullYear();
	    var currentDate = ('0'+(today.getMonth()+1)).slice(-2)+'/'+('0'+(today.getDate())).slice(-2)+'/'+ today.getFullYear();
	    
	    var ordFDt = $filter('date')(new Date(monthFDate), strDateFormat);
	    var ordTDt = $filter('date')(new Date(currentDate), strDateFormat);*/
        
        $(".inputDatePicker").datepicker('destroy');
        $('.inputDatePicker').attr("placeholder", strDtFormat);
        $('.inputDatePicker').datepicker({
            format: strDtFormat,
            autoclose: true,
        });
        
	    
        $("#surgeryFDate").datepicker();
        $("#surgeryTDate").datepicker();
        $("#orderFDate").datepicker();
        $("#orderTDate").datepicker();
        /*$("#orderFDate").datepicker("setDate",ordFDt);
        $("#orderTDate").datepicker("setDate",ordTDt);*/
	    
        /*Calendar icon click focus input fields*/
        $('.surgeryFDate').click(function () {
            $("#surgeryFDate").focus();
            $scope.fnValidateFSurgDate();
        });

        $('.surgeryTDate').click(function () {
            $("#surgeryTDate").focus();
            $scope.fnValidateTSurgDate();
        });

        $('.orderFDate').click(function () {
            $("#orderFDate").focus();
            $scope.fnValidateFOrdDate();
        });

        $('.orderTDate').click(function () {
            $("#orderTDate").focus();
            $scope.fnValidateTOrdDate();
        });

    };


    //onchange company dropdown to get company details
    $scope.fnchangeCompany = function (strCompId) {
        $scope.strCompanyId = strCompId;
        $http.get(microappUrl + 'loadCompanyInfo/' + strCompId).then(function (response) {
            if (response.data != undefined) {
                $scope.compInfoList = response.data;
                $scope.strCurrency = $scope.compInfoList[0].currency;
                $scope.strcurrencyNm = $scope.compInfoList[0].currencyNm;
                $scope.strDtFormat = $scope.compInfoList[0].dtFormat;
                $scope.strCmpCurrFmt = $scope.compInfoList[0].cmpCurrFmt;
                $scope.strCompanyId = $scope.compInfoList[0].companyId;
                $scope.strCompanyZone = $scope.compInfoList[0].compZone;
                $scope.strLanguageId = $scope.compInfoList[0].languageId;
                $scope.strCompanyLocale = $scope.compInfoList[0].companyLocale;
                $scope.fnSetDates($scope.strDtFormat);
                $scope.fnClearField();
                
                $("#span-empty_surFdt-err").hide();
                $("#span-empty_surTdt-err").hide();
                $("#span-empty_ordFdt-err").hide();
                $("#span-empty_ordTdt-err").hide();
                $("#span-dt_compare_surFdt-err").hide();
                $("#span-dt_compare_ordFdt-err").hide();
                $("#span-total-err").hide();

            }
        });
        $("#pending-po-main").removeClass("col-9").addClass("col-12");
        $("#discrepancy-main").removeClass("col-9").addClass("col-12");
        $("#po-entry-main").addClass("col-3").hide();
    };

    //clear the all field
    $scope.fnClearField = function () {
        $scope.strDivisionId = "";
        $scope.strParentAccId = "";
        $scope.strFieldSalesId = "";
        $scope.strSalesRepId = "";
        $scope.strSurgeryFDate = "";
        $scope.strSurgeryTDate = "";
        $scope.strOrdId = "";
        $scope.strAccid = "";
        $scope.strTotalAmount = "";
        $scope.strRepAccId = "";
        $scope.strRepAcc = "";
        $scope.strFieldSales = "";
        $scope.strSalesRep = "";
        $scope.strParentAcc = "";
        $scope.strPONumber= "";
        $scope.strOrderMode="";
        $scope.strOrderFDate="";
        $scope.strOrderTDate="";
        $scope.strPOStatusId="";
    };
    
  //clear the date filter field while sort with DO id
    $scope.fnClearDateField = function (strOrdId) {
    	if(strOrdId != ""){
    	      $scope.strOrderFDate = "";
    	     $scope.strOrderTDate = "";
    	}
    };

    //Set id for Parent account search auto search
    $scope.fnParentAccItem = function ($event) {
        $scope.strParentAcc = angular.element($event.target).attr("data-id");
        $scope.strParentAccId = angular.element($event.target).attr("data-name");
        $scope.hidethisacc = true;    

    };

    //Set id for Rep account search auto search
    $scope.fnRepAccItem = function ($event) {
        $scope.strRepAcc = angular.element($event.target).attr("data-id");
        $scope.strRepAccId = angular.element($event.target).attr("data-name");
        $scope.hidethisrep = true;    

    };

    //Set id for Field sales search auto search
    $scope.fnFieldSalesItem = function ($event) {
        $scope.strFieldSales = angular.element($event.target).attr("data-id");
        $scope.strFieldSalesId = angular.element($event.target).attr("data-name");
        $scope.hidethisfield = true;    

    };

    //Set id for sales Rep search auto search
    $scope.fnSalesRepItem = function ($event) {
        $scope.strSalesRep = angular.element($event.target).attr("data-id");
        $scope.strSalesRepId = angular.element($event.target).attr("data-name");
        $scope.hidethissales = true;           
    };

    // this function used to load PO details
    $scope.fnLoadPOEntry = function () {
        $scope.commetsCount = 0;
        $scope.validateMsg = '';
        $("#preloader-spin").show();

        $scope.gmARCustPOModel = {
            strCompanyId: $scope.strCompanyId,
            strOrderId: $scope.strOrderId
        };
        $scope.strCustomerPo = '';
        $scope.strPOAmt = '';
        $http.post(url + 'loadOrderDetails', angular.toJson($scope.gmARCustPOModel)).then(function (response) {
            if (response.data.status == undefined) {
                $("#pending-po-main").removeClass("col-12").addClass("col-9");
                $("#discrepancy-main").removeClass("col-12").addClass("col-9");
                $("#po-entry-main").addClass("col-3").show();
                $("#preloader-spin").hide();
                $('#PODetails').show();
                $('#strPOAmt').removeClass('error-field');
                $('#span-po-amt-err').hide();
                $timeout(function () {
                    $scope.successMsg = '';
                }, 5000);
                $scope.strOrderId = response.data[0].strOrderId;
                $scope.strAcctId = response.data[0].strAccid;
                $scope.strTotal = response.data[0].strTotal;
                if($scope.strTotal < 0){
                    $scope.strMinTotal = $scope.strTotal.slice(1);
                }
                $scope.strOrderTypeName = response.data[0].strOrderTypeName;
                $scope.strOrdertype = response.data[0].strOrdertype;
                $scope.strShipCost = response.data[0].strShipCost;
                $scope.strCustomerPo = response.data[0].strCustPONumber;
                $scope.strPOAmt = response.data[0].strPOAmt;
                $scope.strPOStatus = response.data[0].strPOStatus;
                $scope.strPONumDtlsId = response.data[0].strPONumDtlsId;
                $scope.strBackOrderCnt = response.data[0].strBackOrderCnt;
                $scope.strDirBckOrdIds = response.data[0].strDirBckOrdIds;
                $scope.strOrdReceiveMode = response.data[0].strOrdReceiveMode;
                $scope.fnValidateAccessFlag();
                 $scope.fnDownloadDOPdfURL();
            }
            else{
                 $timeout(function () {
                    $scope.errorMessage = '';
                }, 5000);
                $("#pending-po-main").removeClass("col-12").addClass("col-9");
                $("#discrepancy-main").removeClass("col-12").addClass("col-9");
                $("#po-entry-main").addClass("col-3").show();
                $("#preloader-spin").hide();
                $('#PODetails').show();
                $scope.errorMessage = errMsg;  
            }

        });
    };
    //This function is used to check write/read access based access,po status and po-detail-id
    $scope.fnValidateAccessFlag = function () {

               $scope.editFl = 'N';
               $scope.statusFl = 'N';
            // 10451-PO Pending Approval,109540-PO Ready For Invoicing,109544-Move to Discrepancy,109548-Processor Error
                if ($scope.AccessFl == 'Y' && ($scope.strPOStatus == '' || $scope.strPOStatus == '109540' || $scope.strPOStatus == '109541' ||  $scope.strPOStatus == '109548')) {
                    $scope.editFl = 'Y';
                }
               
                if ($scope.AccessFl == 'Y' && ($scope.strPOStatus == '109540' || $scope.strPOStatus == '109541' || $scope.strPOStatus == '109548')) {
                    $scope.statusFl = 'Y';
                }

                if ($scope.AccessFl == 'Y' && $scope.strPOStatus == '109541') {
                    $('#reject-po').show();
                    $('#ready-btn').removeClass('col-md-6');
                    $('#move-dis-btn').removeClass('col-md-6');
                    $('#ready-btn').addClass('col-md-4');
                    $('#move-dis-btn').addClass('col-md-5');
                }
               else{
                   $('#reject-po').hide();
                   $('#ready-btn').removeClass('col-md-4');
                   $('#move-dis-btn').removeClass('col-md-5');
                   $('#ready-btn').addClass('col-md-6');
                   $('#move-dis-btn').addClass('col-md-6');
                }

            };
    /* This Function is used to get the PO Details*/
    $scope.fnFetchPODetails = function () {
        $scope.poCheckFlag = '';
        $scope.poHistFl ='';
        $scope.doHistFl ='';
        $scope.orderPODtls ='';
         $scope.orderDODtls ='';
        $("#preloader-spin-po").show();
        $scope.gmAROrderRevenueModel = {
            strCompanyId: $scope.strCompanyId,
            strOrderId: $scope.strOrderId,
        };
        $http.post(url + 'loadOrderRevenueDetails', angular.toJson($scope.gmAROrderRevenueModel)).then(function (response) {

            if (response.data.status == undefined) {
                $scope.poHistFl = response.data.strPOHistFL;
                $scope.doHistFl = response.data.strDOHistFL;
                if (response.data.strPODtls != null) {
                    $scope.orderPODtls = response.data.strPODtls;
                } else {
                    $scope.orderPODtls = "";
                }

                if (response.data.strDODtls != null) {
                    $scope.orderDODtls = response.data.strDODtls;
                } else {
                    $scope.orderDODtls = "";
                }
            }
            else{
                $scope.poHistFl = ''; 
                $scope.doHistFl = '';
                $scope.orderPODtls = '';
                $scope.orderDODtls = '';
                $scope.errorMessage = errMsg; 
                $timeout(function () {
                    $scope.errorMessage = '';
                }, 5000);
            }
            $("#preloader-spin-po").hide();
            $('.loader-hide').show();

        });


    };
    /*This Function is used to fetch comments*/
    $scope.fnFetchComments = function () {
        fnLoadComments($http, $scope.strOrderId, '1200', function (loadcommres) { //1200 common log id
            if (loadcommres.data.status == undefined) {
                $scope.commentsList = loadcommres.data;
            } else {
                $scope.errorMessage = errMsg; 
                 $timeout(function () {
                    $scope.errorMessage = '';
                }, 5000);
            }
        });
    };

    /* This function is used open audit model and po and do details history*/

    $scope.auditLogModal = function (auditid) {
        $('#auditLogModal').modal();
        $scope.auditid = auditid;
        $http.get(microappUrl + 'loadAuditTrailHistory/' + $scope.strOrderId + '/' + auditid + '').then(function (response) {
            if (response.data.status == undefined) {
                $scope.auditHistoryList = response.data;
            } else {
                $scope.errorMessage = errMsg; 
                 $timeout(function () {
                    $scope.errorMessage = '';
                }, 5000);
            }
        });
        $('.dismiss').click();

    };

    /* This function is used to open drilldwon model and existing PO number Details*/

    $scope.poDrillDownModal = function () {
        $('#poDrillDownModal').modal();
        $scope.fnFetchPODrillDown();
        $('.dismiss').click();
    };
    
        
    /*This Function is used to save the order po details*/
    $scope.fnsaveOrderRevDtls = function (flag) {
        if (flag == 'PO') {
            $('#preloader-for-po').show();
        } else if (flag == 'DO') {
            $('#preloader-for-do').show();
        }
//       var custPo = $scope.strCustomerPo; // Code commented for PC-2468
//            if($scope.poSaveFl == 'N'){
//              var custPo = '';
//            }
        $scope.gmARCustPOModel = {
            strOrderId: $scope.strOrderId,
            strPODtls: $scope.orderPODtls,
            strDODtls: $scope.orderDODtls,
            strPOAmt: $scope.strPOAmt,
            strCustPONumber: $scope.strCustomerPo,
            strUserId: localStorage.getItem('strUserId')

        };
        $http.post(url + 'saveRevenueDetails', angular.toJson($scope.gmARCustPOModel)).then(function (response) {
            if (response.data.status == undefined) {
                if (flag == 'PO') {
                    $scope.poHistFl = response.data.strPOHistFL;
                } else {
                    $scope.doHistFl = response.data.strDOHistFL;
                }

            }
            else{
               $scope.errorMessage = errMsg; 
                 $timeout(function () {
                    $scope.errorMessage = '';
                }, 5000);
            }
            $('#preloader-for-po').hide();
            $('#preloader-for-do').hide();

        });
    };

    /* This function is used to check the existing PO*/
    $scope.fnPOCheck = function () {
        var poNum = $scope.strCustomerPo;
        var focusFl = 'Y';
        $scope.poCheckFlag = 'N';
        if (focusFl == 'Y')
            $('#preloader-for-ponum').show();
            if($scope.strPONumDtlsId == ''){
                if (poNum.trim() != '') {
                    $scope.fnSavePOCheck();
                }
                else{
                    $('#preloader-for-ponum').hide();
                    return false;
                }
            }
        else{
                $scope.fnSavePOCheck();
            }
            
    };
    $scope.fnSavePOCheck = function () {
        if (parseNull($scope.strCustomerPo) != "") {
            $http.get(url + 'validatePONum/' + $scope.strCustomerPo + '/' + $scope.strCompanyId + '/' + $scope.strOrderId).then(function (response) {
                if (response.data.status == undefined) {
                    var count = response.data;
                    if (count > 0) {
                        $scope.poCheckFlag = 'Y';
                        $scope.confirmPOMsg = "PO Number is already Exists. Do you want to Proceed ?";
                        $('#conPOModal').modal();
                        $('.dismiss').click();
                    } else {
                        $scope.poCheckFlag = 'N';
                        $scope.fnSavePONum();
                    }

                }
                $('#preloader-for-ponum').hide();
            });
        } else {
            $scope.fnSavePONum();
        }

        focusFl = '';
    };
    
    $scope.confirmPONumber = function (){
        $('#preloader-for-ponum').show();
        $scope.poSaveFl = 'Y';
        $scope.fnSavePONum();
    };
    $scope.ConfirmPOCancel = function (){
        $scope.poSaveFl = 'N';
    };
    $scope.fnSavePONum = function (){
        $scope.gmARCustPOModel = {
                    strOrderId: $scope.strOrderId,
                    strPODtls: $scope.orderPODtls,
                    strDODtls: $scope.orderDODtls,
                    strPOAmt: $scope.strPOAmt,
                    strCustPONumber: $scope.strCustomerPo,
                    strUserId: localStorage.getItem('strUserId')
                };
        $http.post(url + 'savePODetails', angular.toJson($scope.gmARCustPOModel)).then(function (response) {
            if (response.data.status == undefined) {
                $scope.strPONumDtlsId = response.data[0].strPONumDtlsId;
                $scope.strPOStatus = response.data[0].strPOStatus;
                $scope.strCustomerPo = response.data[0].strCustPONumber;
                $scope.strTotal = response.data[0].strTotal;
                if ($scope.strTotal < 0) {
                    $scope.strMinTotal = $scope.strTotal.slice(1);
                }
                $scope.strShipCost = response.data[0].strShipCost;
            } else {
                $scope.errorMessage = errMsg;
                $timeout(function () {
                        $scope.errorMessage = '';
                    }, 5000);
            }
            $('#preloader-for-ponum').hide();
             $scope.validatePONumMsg = '';
            $scope.poSaveFl = 'Y';
            $scope.fnValidateAccessFlag();
        });
    };
    
   

    /*This Function is used to save the PO Amount*/
    $scope.fnPOAmtSave = function () {
//        var poAmt = $scope.strPOAmt; // Code commented for PC-2468
        if (!isNaN($scope.strPOAmt)) {
            $('#strPOAmt').removeClass('error-field');
            $('#span-po-amt-err').hide();
//            var custPo = $scope.strCustomerPo;
//            if($scope.poSaveFl == 'N'){
//              var custPo = '';
//            }
            $scope.gmARCustPOModel = {
                strOrderId: $scope.strOrderId,
                strPODtls: $scope.orderPODtls,
                strDODtls: $scope.orderDODtls,
                strPOAmt: $scope.strPOAmt,
                strCustPONumber: $scope.strCustomerPo,
                strUserId: localStorage.getItem('strUserId')

            };
            $('#preloader-for-poamt').show();
            $http.post(url + 'savePODetails', angular.toJson($scope.gmARCustPOModel)).then(function (response) {
               if (response.data.status == undefined) {
                $scope.strPONumDtlsId = response.data[0].strPONumDtlsId;
                $scope.strCustomerPo = response.data[0].strCustPONumber;
                $scope.strPOStatus = response.data[0].strPOStatus;
                $scope.strTotal = response.data[0].strTotal;
                if ($scope.strTotal < 0) {
                    $scope.strMinTotal = $scope.strTotal.slice(1);
                }
                $scope.strShipCost = response.data[0].strShipCost;
            } else {
                $scope.strPONumDtlsId = '';
                $scope.strPOStatus = '';
                $scope.errorMessage = errMsg;
                $timeout(function () {
                            $scope.errorMessage = '';
                        }, 5000);
            }

            $scope.validatePOAmtMsg = '';
            $scope.fnValidateAccessFlag();
            $('#preloader-for-poamt').hide();
            });
        }
    };
        
      /*This function is used to PO Amount validation*/
        $scope.fnPOAmtValidation = function (){
            var poAmt=$scope.strPOAmt;
            if(poAmt.trim() !=''){
                if(isNaN(poAmt)){
                    $('#strPOAmt').addClass('error-field');
                    $('#span-po-amt-err').show();
                }
                else{
                 $('#strPOAmt').removeClass('error-field');
                 $('#span-po-amt-err').hide();               
                }
            }
            else{
                 $('#strPOAmt').removeClass('error-field');
                 $('#span-po-amt-err').hide();               
            }
        };

    /*This Function is used to get Exixting PO Number Details*/

    $scope.fnFetchPODrillDown = function () {
        $scope.gmARCustPOModel = {
            strCustPONumber: $scope.strCustomerPo,
            strOrderId: $scope.strOrderId,
            strDtFormat: $scope.strDtFormat,
            strCompanyId: $scope.strCompanyId
        };
        $scope.drilDownList ='';
        var table = $('#drillDownTable').DataTable();
         table.destroy();
         $http.post(url + 'fetchPODrilldownDetails', angular.toJson($scope.gmARCustPOModel)).then(function (response) {
            if (response != undefined) {
                $scope.drilDownList = response.data;
                 angular.element(document).ready(function() 
                    {
                        table = $('#drillDownTable').DataTable(
                        {
                             "bPaginate" :true,
                             dom: 'Bfrtip',
                             "language": {
                                   "emptyTable": "No data found to display",
                                   "zeroRecords": "No data found to display"
                                 },
                             buttons :[],
                             "order": [],
                             "columnDefs": [ {
                               "targets"  : 'no-sort',
                               "orderable": false
                             }]
                        });
                      
                    }); 
            }
        });
    };

    /*This Function is used to save the common log comments*/

    $scope.saveLogComments = function () {
        var comments = $scope.strComments;
        if (comments!='') {
            $scope.commentsCount = 1;
            $('#preloader-for-comments').show();
            $scope.gmCommonLog = {
                refId:$scope.strOrderId,
                comments: $scope.strComments,
                type: '1200',
                createdBy: localStorage.getItem('strUserId'),
                strTimeZone: $scope.strCompanyZone,
                strDateFormat: $scope.strDtFormat
            };
            
            fnSaveLogComments($http,$scope.gmCommonLog, function (savelogcomres) { //1200-common log id
                if (savelogcomres.data.status == undefined) {
                    fnLoadComments($http, $scope.strOrderId, '1200', function (loadcommres) { //1200 common log id
                        if (loadcommres.data.status == undefined) {
                            $scope.commentsList = loadcommres.data;
                        } else {
                            $scope.errorMsg = response.data.message;
                            sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                            $scope.errorMessage = errMsg;
                            $timeout(function () {
                                $scope.errorMessage = '';
                            }, 5000);
                        }
                     
                    });
                    $scope.validateCommentsMsg = '';
                }
                else{
                     $scope.validateCommentsMsg = '';
                     $scope.errorMessage = errMsg;
                     $timeout(function () {
                         $scope.errorMessage = '';
                     }, 5000);
                }
                   $('#preloader-for-comments').hide();
            });
            $scope.strComments = '';
        } 
        else{
            $scope.strComments ='';
            $('#preloader-for-comments').hide();
        }

    };
    
    /*This function is used to clear the comments*/
    
    $scope.fnClearComments = function (){
        $scope.strComments ='';
    };
    
    /*This function is used refresh the Order details*/

    $scope.fnRefreshRightPanel = function () {
        $scope.fnLoadPOEntry();
        $scope.fnFetchPODetails();
        $scope.fnFetchComments();
    };

    /*This Function used to close the right panel from order report */

    $scope.fnCloseRightPanel = function () {
        $("#pending-po-main").removeClass("col-9").addClass("col-12");
        $("#discrepancy-main").removeClass("col-9").addClass("col-12");
        $("#po-entry-main").addClass("col-3").hide();
    };
    /*this function is used to clear the po number*/

    $scope.fnClearPONum = function () {
        $scope.strCustomerPo = '';
        $scope.poCheckFlag = 'N';
    };

    /*Function used to Reject PO information*/
    $scope.fnRejectPO = function () {
        $('#divCommonCancel').modal();
        $('.dismiss').click();

    };

    /*Function used to Reject PO information*/

    $scope.fnVoidSubmit = function () {
        var savVoidFl = 'Y';
        $timeout(function () {
                    $scope.validateReasonMsg = ''; 
                    $scope.validateReasonCommentsMsg = '';
                }, 6000);
         $scope.validateReasonMsg ='';
         $scope.validateReasonCommentsMsg ='';
        if($scope.strVoidComments == '' || $scope.strVoidComments == undefined){
            savVoidFl ='N';
             $scope.validateReasonCommentsMsg = 'Please Provide Comment';
        }
        if($scope.strVoidReasonId == '' || $scope.strVoidReasonId == undefined){
           savVoidFl ='N';
             $scope.validateReasonMsg = 'Please Select Reason';
           }
        if(savVoidFl == 'Y'){
             $scope.gmCommonCancelLog = {
        strRefId : $scope.strPONumDtlsId,
        strCancelCd:$scope.strVoidReasonId,
        strCancelType: 'REDOPO',
         strComment: $scope.strVoidComments,
         strUserId: localStorage.getItem('strUserId')
        };
        fnCommonCancel($http, $scope.gmCommonCancelLog , function (savelogres) {
            if (savelogres.data.status == undefined) {
                $scope.strVoidTxnId = '';
                $scope.strVoidReasonId = '';
                $scope.strVoidComments = '';
                $('#divCommonCancel').modal('hide');
                $('#reject-po').hide();
                $scope.successMsg = "PO Rejected Successfully..";
                $scope.fnRefreshRightPanel();
            }
            else{
                $scope.errorVoidmsg  = '';
                 $scope.errorMessage = errMsg;
                     $timeout(function () {
                         $scope.errorMessage = '';
                     }, 5000);
            }
        });
        }
//       
    };

    /*This function is used save the PO Status*/

    $scope.fnSavePOStatus = function (status) {
        $scope.validatePONumMsg = '';
        $scope.validatePOAmtMsg = '';
        $scope.validateCommentsMsg = '';
        $scope.status = status;
        $scope.confirmMsg = '';
        var validFl = 'Y';
       
        if ($scope.strCustomerPo.trim() == '') {
            validFl = 'N';
            $scope.validatePONumMsg = 'Please provide PO Number';
        }
        if ($scope.commentsCount == 0) {
            validFl = 'N';
            $scope.validateCommentsMsg = 'Please provide Comments';
        }
           if (validFl == 'Y') {
           $scope.fnValidateSavePOStatus(status);
           }
    };
    
    $scope.fnValidateSavePOStatus = function (status){
        $scope.gmARCustPOModel = {
            strAccid: $scope.strAcctId,
            strCustPONumber: $scope.strCustomerPo,
            strPOStatus: status,
            strUserId: localStorage.getItem('strUserId')
        };
        $scope.confirmheader = '';
        var dir_bck_id = $scope.strDirBckOrdIds;
        var back_ord_msg = '';
        var po_amt_msg = '';
        var po_empty_msg='';
         var msg = '';
      
            if (status == '109542') {
                $scope.confirmheader = 'Ready For Invoicing';
                if ($scope.strOrdertype == 26240232 && $scope.strBackOrderCnt > 0) {
                    if (dir_bck_id.length > 0) {
                        back_ord_msg = 'This Order have following Back Order(s) ' + dir_bck_id + '.';
                    }
                }
                if ($scope.strPOAmt.length > 0) {
                    if ($scope.strPOAmt != $scope.strTotal) {
                        po_amt_msg = ' PO Amount & Total Amount is not matching.';
                    }
                }
                if($scope.strPOAmt.length == 0){
                    po_empty_msg = "without PO Amount";
                }
               
                if(po_amt_msg !='' || back_ord_msg !='' || po_empty_msg !=''){
                    msg = back_ord_msg + po_amt_msg + " Do you want to Proceed Ready For Invoicing "+po_empty_msg+" ?";
                }
            } else if (status == 109544) { //109544 Move to Discrepancy
                $scope.confirmheader = 'Move to Discrepancy';
                if($scope.strPOAmt.length > 0){
                   if ($scope.strPOAmt == $scope.strTotal) {
                    po_amt_msg = 'PO Amount & Total Amount is  matching.';
                } 
                }
                else if($scope.strPOAmt.length == 0){
                    po_empty_msg ="without PO Amount"
                }
                if(po_amt_msg != '' || po_empty_msg !=''){
                     msg =  po_amt_msg + " Do you want to Proceed Move to Discrepancy "+po_empty_msg+" ?";
                }
                
            }

            if (msg != '') {
                $scope.confirmMsg = msg;
                $('#conFModal').modal();
                $('.dismiss').click();
            } else {
                $http.post(url + 'savePOStatus', $scope.gmARCustPOModel).then(function (response) {
                    if (response.data.status == undefined) {
                        $scope.fnRefreshRightPanel();
                    }else{
                         $scope.errorMessage = errMsg;
                     $timeout(function () {
                         $scope.errorMessage = '';
                     }, 5000);
                    }
                });
            }

    };

    //this function is used to save PO status after confirmation

    $scope.confirmPOStatus = function () {
        $scope.gmARCustPOModel = {
            strAccid: $scope.strAcctId,
            strCustPONumber: $scope.strCustomerPo,
            strPOStatus: $scope.status,
            StrUserId: localStorage.getItem('strUserId')

        };
        $http.post(url + 'savePOStatus', $scope.gmARCustPOModel).then(function (response) {
            if (response != undefined) {
                $scope.fnRefreshRightPanel();
            }
        });
    };
    //Clear redis dropdown id 
    $scope.fnClearRedisId = function () {
   	 var strParentAccNm = $scope.strParentAccId;
   	 var strRepAccNm = $scope.strRepAccId;
   	 var strFieldSalesNm = $scope.strFieldSalesId;
   	 var strSalesRepNm = $scope.strSalesRepId;
   	 
   	 if(strParentAccNm == '' ){
   		 $scope.strParentAcc = "";
   	 }
   	 if(strRepAccNm == '' ){
   		 $scope.strRepAcc = ""; 
   	 }
   	 if(strFieldSalesNm == '' ){
   		 $scope.strFieldSales = "";
   	 }
   	 if(strSalesRepNm == '' ){
   		 $scope.strSalesRep = "";
   	 }
   	 
    };

    $scope.fnValidateTotal = function (strTotalAmount) {
        if (strTotalAmount == '') {
            $("#span-total-err").hide();
        }
    };
    
    $scope.fnValidateFSurgDate = function () {
    	$("#span-empty_surFdt-err").hide();
    };
    $scope.fnValidateTSurgDate = function () {
    	$("#span-empty_surTdt-err").hide();
    	$("#span-dt_compare_surFdt-err").hide();
    };
    
    $scope.fnValidateFOrdDate = function () {
    	$("#span-empty_ordFdt-err").hide();
    };
    $scope.fnValidateTOrdDate = function () {
		$("#span-empty_ordTdt-err").hide();
		$("#span-dt_compare_ordFdt-err").hide();
    };

    // this function call used to show uploaded file list and provides drag and drop option to add new file 

    $scope.fnLoadFiles = function () {

        var AccessFl = $scope.AccessFl;
        var strUserId = localStorage.getItem('strUserId');
        var strDtFormat = $scope.strDtFormat;
        var strCompTimeZone = $scope.strCompanyZone;
        var strCompanyLocale =  $scope.strCompanyLocale;

        var viewMode = "list"; // ex: list or grid
        var scaleFactor = 2;
        var autoSendFl = true;
        var refID = $scope.strOrderId;
        var refType = "53018"; // DO
        var refGroupType = "26240412"; // DO
        var refSubtype = "26230732"; // DO document upload

        var uploadURL = microappUrlFile + "arUploadFile";
        var saveURL = microappUrlFile + "saveUploadFile";
        var fetchInfoURL = microappUrlFile + "loadUploadFileDetails";
        var deleteURL = microappUrlFile + "deleteFile";
	var fileUploadDownloadPath = "download-dir/"+strCompanyLocale+"/"+refID;
        var downloadURL = microappUrlFile+"downloadFile/"+fileUploadDownloadPath+"/";
        var uploadPath = fileUploadPath+strCompanyLocale+"//"+refID+"//";
        var doUpdateFlURL = microappUrlFile + "doUpdateFlag";

        var fileDetArr = [];
        var fileNameArr = [];

        var divRef = "dhtmlxvault-file-upload-po";
        var divWidth = ""; // width of file upload div element
        var divHeight = 200; // height of file upload div element

        $scope.gmUploadFilePOModel = {
            strRefGrp: refGroupType, // DO
            strRefId: refID, // Order Id
            strRefType: refType, // DO
            strType: "0", // DO document upload
            strFileTitle: "0", // Upload File Title
            strFileName: "0", // Upload File Name
            strPartNumberId: "0", // Part Number Id
            strDateFormat: strDtFormat // Company Date Format
        };
        // Get uploaded file information based on selected PO order detail       
        fnGetUploadedFiles($scope.gmUploadFilePOModel, fetchInfoURL, function (serverFiles, serverFileNames) {
            fileDetArr = serverFiles; // serverFiles info is stored to fileDetArr to use initial parse on vault
            fileNameArr = serverFileNames; // serverFileNames info is stored to fileNameArr to use duplicate file name check in vault
            //this initDHTMLXValut() call used to show drag and drop file upload option 
            $("#dhtmlxvault-file-upload-po").empty(); 
            var dhtmlVault = initDHTMLXValut(divRef, uploadURL, downloadURL, saveURL, fetchInfoURL, doUpdateFlURL, viewMode, scaleFactor, autoSendFl, divWidth, divHeight, refID, refType, refSubtype, refGroupType, fileDetArr, deleteURL, AccessFl, fileNameArr, strDtFormat, strCompTimeZone, uploadPath, strUserId);
        });


    }
    
  //this function is clear all the field while click on clear button 
    $scope.fnClearAllField = function () {
        $scope.strDivisionId = "";
        $scope.strParentAccId = "";
        $scope.strFieldSalesId = "";
        $scope.strSalesRepId = "";
        $scope.strSurgeryFDate = "";
        $scope.strSurgeryTDate = "";
        $scope.strOrderFDate = "";
        $scope.strOrderTDate = "";
        $scope.strOrdId = "";
        $scope.strAccid = "";
        $scope.strTotalAmount = "";
        $scope.strRepAccId = "";
        $scope.strRepAcc = "";
        $scope.strFieldSales = "";
        $scope.strSalesRep = "";
        $scope.strParentAcc = "";
        $scope.strPONumber = "";
        $scope.strOrderMode= "";
        $scope.strOrderFDate="";
        $scope.strOrderTDate="";
        $scope.strPOStatusId="";
        $scope.hidethisacc = true;
        $("#list-group-acc").removeClass("searchResults");
        $scope.hidethisrep = true;
        $("#list-group-rep").removeClass("searchResults");
        $scope.hidethisfield = true;
        $("#list-group-field").removeClass("searchResults");
        $scope.hidethissales = true;
        $("#list-group-sales").removeClass("searchResults");
    };

  //Summary Report DO Function
    $scope.fnSummaryDO = function (ordID) {
    	var strPartyId =  localStorage.getItem("strPartyId");
        var strCompId  =  localStorage.getItem("strCompId");
        var strLangId  =  localStorage.getItem("strLangId");
        var strPlantId =  localStorage.getItem("strPlantId");
        
        var companyInfo = {};
        //form the company info to pass the URL
        companyInfo.cmpid = strCompId;
        companyInfo.plantid = strPlantId;
        companyInfo.token = "";
        companyInfo.partyid = strPartyId;
        companyInfo.cmplangid = strLangId;
        var StringifyCompanyInfo = JSON.stringify(companyInfo);
     
    	var features = 'directories=no,menubar=no,status=no,titlebar=no,toolbar=no,width=800,height=800';
    	var url='GmEditOrderServlet?hMode=PrintPrice&hOrdId='+ordID;
        window.open(encodeURI(window.location.protocol+'//'+window.location.hostname+'/'+url+'&companyInfo='+StringifyCompanyInfo, 'DO Summary', features));
    };
    // this function is used to download DO Summary PDF 
    $scope.fnDownloadDOPdfURL = function () {
        var strCompanyLocale =  $scope.strCompanyLocale;
	      var doPDFPath = "ipad-do-download/"+strCompanyLocale+"/"+$scope.strOrderId+".pdf";
        var downloadPDFURL = microappUrlFile+"downloadFile/"+doPDFPath;
        console.log(downloadPDFURL);
        $scope.strDOPDFURL = downloadPDFURL;
        //  $('#do-pdf-url').attr("href",downloadPDFURL);  
    };

    //this funtion used to show PO format popun screen
    $scope.fnFetchPOFormat = function(){
        $scope.gmARCustPOModel = {
            strAccid: $scope.strAcctId,
            strDtFormat: $scope.strDtFormat
        };
        $scope.poFormatList = '';
        $scope.strPOFmtParrAccNm = '';
        var table = $('#poFormatTable').DataTable();
        table.destroy();
        $http.post(url + 'fetchPOFormatDrilldownDetails',angular.toJson($scope.gmARCustPOModel)).then(function (response) {
            if (response.data.status == undefined) {
                $scope.poFormatList = response.data;
                $scope.strPOFmtParrAccNm =  $scope.strParentAccNm;
                angular.element(document).ready(function() 
                {
                    table = $('#poFormatTable').DataTable(
                    {
                         "bPaginate" :false,
                         dom: 'Bfrtip',
                         "language": {
                               "emptyTable": "No data found to display",
                               "zeroRecords": "No data found to display"
                             },
                         buttons :[],
                         "order": [],
                         "columnDefs": [ {
                           "targets"  : 'no-sort',
                           "orderable": false
                         }]
                    });
                  
                }); 

            }
            else {
                $scope.errorMsg = response.data.message;
                sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                $scope.errorHandler();
            }
        });
    };

    /* This function is used to open PO Format model */

    $scope.fnPODetailView = function () {
            $('#poFormatModal').modal();
            $scope.fnFetchPOFormat();
            $('.dismiss').click();
    };

});

// this function is used display the hold icon in po report screen 
function eXcell_holdFl(cell) {
    // the eXcell name is defined here
    if (cell) { // the default pattern, just copy it
        this.cell = cell;
        this.grid = this.cell.parentNode.grid;
    }
    this.setValue = function (val) {
        if (val == 'Y') {
            this.setCValue("<img alt=\"DO on hold status\" title=\"DO on Hold Status\" src=\"/images/hold-icon.png\" border=\"0\" disabled=\"true\">",
                val);
        }
    }

    this.getValue = function () {
        return this.cell.childNodes[0].innerHTML; // gets the value
    }
};
eXcell_holdFl.prototype = new eXcell; 
