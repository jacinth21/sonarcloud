/* 
 * GmARInvoiceController.js : This Controller is used for A/R Ready for invoicing functionalities
 * @author Ramachandiran T.S
 */

var gridObj;
var check_rowId = "";
var poDtlsId_rowId = "";
var custPoNUm_rowId = "";
var diffAmt_rowId = "";
var selectRowCount = 0;
app.controller('GmARInvoiceController', function ($scope, $http, $q, $timeout,$filter) {
    var AfterAuthentication = function () {
        var deferred = $q.defer();
        while (!KeycloakService.keycloak.authenticated) {}
        deferred.resolve(KeycloakService.keycloak.authenticated);
        return deferred.promise;
    }

    $(document).ready(function () {
        var url      = (window.location.href).split('#/');
         if( url[1] == 'gmautoinvoice'){
           $("#li-order-pending").removeClass("active");
           $("#li-discrepancy-po").removeClass("active");
           $("#li-auto-invoice").addClass("active"); 
           $("#gmorderpendingpo").removeClass("active");
           $("#gmardisprepancy").removeClass("active");
           $("#gmautoinvoice").addClass("active"); 
          }
         else if( url[1] == 'gmardiscrepancy'){
            $("#li-order-pending").removeClass("active");
            $("#li-auto-invoice").removeClass("active");
            $("#li-discrepancy-po").addClass("active"); 
            $("#gmorderpendingpo").removeClass("active");
            $("#gmautoinvoice").removeClass("active");            
            $("#gmardiscrepancy").addClass("active");
         }
        else{
            $("#li-order-pending").addClass("active");
            $("#li-discrepancy-po").removeClass("active");
            $("#li-auto-invoice").removeClass("active");
            $("#gmautoinvoice").removeClass("active");            
            $("#gmardiscrepancy").removeClass("active");
           $("#gmorderpendingpo").addClass("active");
        }
        $http.get(varJsonURL).then(function (response) {}, function (response) {
            $scope.errorMsg = response.data.message;
            sessionStorage.setItem("errorMsg", ($scope.errorMsg));
            $scope.errorHandler();
        });
        var varLoginName = localStorage.getItem('strLoginName');
        var strPartyId = "";
        var strUserId = "";
        var strCompId = "";

        $('#btn-show').hide();
        $('#no-data-found').hide();
        $("#show-batch").hide();
        $("#show-CheckAll").hide();
        $("#show-batch-confirm").hide();
        $("#show-batch-error").hide();
        $('.dhx_sample-controls').hide();
        $('#show-batch').hide();
        $scope.AccessFlBatch = "N";

        /*Web Service Call for party details from user name*/
        fnLoadUserInfo($http, varLoginName, function (response) {
            if (response.data != undefined) {
                strCompId = response.data[0].strCompanyId;
                strPartyId = response.data[0].strPartyId;
                strUserId = response.data[0].strUserId;
                localStorage.setItem("strPartyId", strPartyId);
                localStorage.setItem("strCompId", strCompId);
                localStorage.setItem("strUserId", strUserId);
                /*Web Service Call for Load Company details  based on login user*/
                fnLoadCompany($http, strPartyId, function (response) {
                    if (response.data != undefined) {
                        $scope.compList = response.data;
                        /*Web Service Call for company information from company id*/
                        fnLoadCompanyInfo($http, strCompId, function (response) {
                            if (response.data != undefined) {
                                $scope.selectedCompany = strCompId;
                                $scope.compInfoList = response.data;
                                $scope.strCurrency = $scope.compInfoList[0].currency;
                                $scope.strcurrencyNm = $scope.compInfoList[0].currencyNm;
                                $scope.strDtFormat = $scope.compInfoList[0].dtFormat;
                                $scope.strCmpCurrFmt = $scope.compInfoList[0].currencyFormat;
                                $scope.strCompanyId = $scope.compInfoList[0].companyId;
                                $scope.strCompanyZone = $scope.compInfoList[0].compZone;
                                $scope.strLanguageId = $scope.compInfoList[0].languageId;
                                $scope.strCompanyLocale = $scope.compInfoList[0].companyLocale;
                                $scope.strPlantId = $scope.compInfoList[0].plantId;
                                localStorage.setItem("strLangId", $scope.strLanguageId);
                                localStorage.setItem("strPlantId", $scope.strPlantId);
                                localStorage.setItem("strCompZone", $scope.strCompanyZone);
                                $scope.fnSetDates($scope.strDtFormat);
                                $scope.fnClearField();
                            }
                        });
                    } else {
                        $scope.errorMsg = response.data.message;
                        sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                        $scope.errorHandler();
                    }
                });
                /*This function is used to get the update access flag for based on PartyId*/
                fnSecurityEvent($http, strPartyId, 'RDY_FOR_INV_ACC', function (secresponse) {
                    if (secresponse.data != undefined && secresponse.data != '') {
                        if (secresponse.data[0].updateAccessFl != undefined) {
                            $scope.AccessFl = secresponse.data[0].updateAccessFl;
                            $scope.AccessFlBatch = secresponse.data[0].updateAccessFl;
                        }
                    }
                });

            } else {
                $scope.errorMsg = response.data.message;
                sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                $scope.errorHandler();
            }
        });

        /*Web Service Call for Load Company details  based on login user*/
        fnLoadDivision($http, function (response) {
            if (response.data.status == undefined) {
                $scope.divisionList = response.data;
            } else {
                $scope.errorMsg = response.data.message;
                sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                $scope.errorHandler();
            }
        });


        $http.get(varJsonURL).then(function (response) {
            console.log("Entered into");
            varInvalidPOMoveToDiscrepancy = response.data.invalidPOMoveToDiscrepancy;
            varEmptyPOFromMoveToDiscrepancy = response.data.emptyPOFromMoveToDiscrepancy;
            varPOMoveToProcessorErr = response.data.poMoveToProcessorErr;
            varEmptyPOMoveToProcessorErr = response.data.emptyPOMoveToProcessorErr;
        }, function (response) {
            $scope.errorMsg = response.data.message;
            sessionStorage.setItem("errorMsg", ($scope.errorMsg));
            $scope.errorHandler();
        });

        $("#preloader-spin").hide();
    });
    /*Function is used to load the PO report*/
    $scope.fnLoadPOReportByStatus = function () {
        var varValidateMsg = "";

        var varPOEnteredFromDate = "";
        var varPOEnteredToDate = "";
        var varPOEnteredDValidateFl = false;
        var varPOEnteredDtErrFl = false;
        var varEnteredFromDtErrFl = false;
        var varEnteredToDtErrFl = false;

        varPOEnteredFromDate = $scope.strPOEnteredFDate;
        varPOEnteredToDate = $scope.strPOEnteredTDate;


        $("#span-empty_poFdt-err").hide();
        $("#span-empty_poTdt-err").hide();
        $("#span-dt_compare_poFdt-err").hide();
        $scope.fnSetRowCountEmpty();

        //validate From Date of PO Entered Field and assign result to Flag
        if (varPOEnteredFromDate == undefined || varPOEnteredFromDate == null || varPOEnteredFromDate == '') {

            varEnteredFromDtErrFl = true;
        }
        //validate To Date of PO Entered Field and assign result to Flag
        if (varPOEnteredToDate == undefined || varPOEnteredToDate == null || varPOEnteredToDate == '') {

            varEnteredToDtErrFl = true;
        }


        //Check PO Entered date field and get validation message for missing input field
        if ((varEnteredFromDtErrFl) && (!varEnteredToDtErrFl)) {
            $("#span-empty_poFdt-err").show();
            return false;
        }
        if ((!varEnteredFromDtErrFl) && (varEnteredToDtErrFl)) {
            $("#span-empty_poTdt-err").show();
            return false;
        }


        //validate Surgery Date and assign result to Flag
        if (varPOEnteredFromDate != null && varPOEnteredFromDate != undefined && varPOEnteredFromDate != '' && varPOEnteredToDate != null && varPOEnteredToDate != undefined && varPOEnteredToDate != '') {
            varPOEnteredDValidateFl = true;
        }

        //This condition will execute if Validation Surgery flag is true
        if (varPOEnteredDValidateFl) {
            //dateDiff method is used for identify the date difference for all format 
            dateDiff(varPOEnteredFromDate, varPOEnteredToDate, $scope.strDtFormat, function (response) {
                if (response != '') {
                    if (response < 0) {
                        varPOEnteredDtErrFl = true;
                        $("#span-dt_compare_poFdt-err").show();
                        return false;
                    }
                }
            });

        }

        //Load the Ready for invoicing reports
        if (varPOEnteredDtErrFl == false) {
            $("#ready-for-invoicing-results").html("<div class='w-100 text-center' id='pre-loader-main'><i class='fas fa-spinner fa-pulse fa-2x'></i></div>");
            $('#ready-for-invoicing-results').show();
            $('#no-data-found').hide();


            /*toggle more button should collapse while click on load button*/
            if ($('#collapseFilter').hasClass('collapse show')) {
                $('#collapseFilter').removeClass('show');
            }

            if ($('#btnCollapseFilter').hasClass('btn btn-xs btn-outline-dark')) {
                $('#btnCollapseFilter').addClass('btn btn-xs btn-outline-dark collapsed');
            }


            $scope.fnClearRedisId();
            $('#show-batch').hide();
            $scope.gmARCustPOModel = {
                strCompanyId: $scope.strCompanyId,
                strParentAcc: $scope.strParentAcc,
                strRepAcc: $scope.strRepAcc,
                strFieldSales: $scope.strFieldSales,
                strSalesRep: $scope.strSalesRep,
                strDivisionId: $scope.strDivisionId,
                strCustPONumber: $scope.strCustPONumber,
                strAccid: $scope.strAccid,
                strPOStatusList: '109542,109546', // Ready for Invoice, Invoice failed
                strLanguageId: $scope.strLanguageId,
                strDtFormat: $scope.strDtFormat,
                strPOEnteredFromDate: $scope.strPOEnteredFDate,
                strPOEnteredToDate: $scope.strPOEnteredTDate
            };

            $http.post(url + 'loadPOByStatus/', angular.toJson($scope.gmARCustPOModel)).then(function (response) {

                if (response.data != undefined) {
                    $scope.results = response.data;
                    $scope.fnLoadReadyForInvoice($scope.results);
                } else {
                    $scope.errorMsg = response.data.message;
                    sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                    $scope.errorHandler();
                }

            });

        }
    };
    $scope.fnLoadReadyForInvoice = function (response) {
        if (response != '') {
            $('#ready-for-invoicing-results').empty();
            // Updating new dhtmlx Grid - 6.5.2
            datacollection = new dhx.DataCollection();

            datacollection.parse(response);
            $('#btn-show').show();
            $scope.fnValidateAccessFlag();

            $(".po-report-title").text("AR Invoicing Report");
            
            if (response.length > 0) {
                
                console.log(response);
                grid = new dhx.Grid("ready-for-invoicing-results", {
                    columns: [{width: 50,id: "chkFl",header: [{text: ""}, {text: "<input id='selectAll'                    class='chckMaster' type='checkbox' style='background: #0288d1 !important;'> "}],
                        editable: true,type: "boolean",sortable: false,htmlEnable: true},
                        {width: 140,id: "strPOEnteredDate",align: "left",header: [{text: "PO Entered Date",align: "center"}, {content: "inputFilter"}]}, 
                        {width: 80,id: "strDOFlag",align: "center",header: [{text: "Files",align: "center"}],htmlEnable: true,
                            template: function (text, row, col) {
                                if (text == 'Y') {
                                    return "<img alt=\"File Upload Status\" data-orderid='" + row.strOrderId + "' data-ponumber='" + row.strCustPONumber + "'  class=\"file-icon\" title=\"File Upload Status\"  src=\"/images/icon-letter.png\" border=\"0\" disabled=\"true\">"
                                }
                        } },
                        {width: 80,id: "strOrdReceiveMode",align: "center",header: [{text: "iPad PDF",align: "center"}],htmlEnable: true,
                            template: function (text, row, col) {
                                if (text == '26230683') {
                                    return "<img alt='DO pdf Details' src='/images/pdf_icon.gif' title='DO pdf Details'>"
                                }
                        }},
                        {width: 280,id: "strParentAcc",align: "left",header: [{text: "Parent Account",align: "center" }, { content: "inputFilter"}]},
                        {width: 150,id: "strParentAccId",align: "right",header: [{text: "Parent Account Id",align: "center"}, {content: "inputFilter" }]},
                        {width: 70,id: "strHoldFl",align: "center",header: [],htmlEnable: true,
                            template: function (text, row, col) {
                                if (text == 'Y') {
                                    return "<img alt='DO on hold status' title=\"DO on Hold Status\" src=\"/images/hold-icon.png\" border=\"0\" disabled=\"true\">"
                                }
                        }},
                        {width: 180,id: "strCustPONumber",align: "left",header: [{text: "PO Number",align: "center"}, {content: "inputFilter"}],footer: [{text: '<div class="custom_footer">Total</div>'}]},
                        {width: 150,id: "strPOAmt",align: "right",header: [{text: "PO Amount",align: "center"}, {
                        content: "inputFilter" }],type: "number",template: function (text, row, col) {
                                if (row.strPOAmt == 0) {
                                    return "0.00"
                                } else if (row.strPOAmt < 0) {
                                    row.strPOAmt =  row.strPOAmt.slice(1);
                                    if (row.strPOAmt % 1 != 0) {
                                        row.strPOAmt =  parseFloat(row.strPOAmt + '0');
                                        return (row.strPOAmt).toFixed(2);
                                    } else {
                                        return row.strPOAmt + ".00";
                                    }
                                } else {
                                    if (row.strPOAmt % 1 != 0) {
                                        row.strPOAmt = parseFloat(row.strPOAmt + '0');
                                        return (row.strPOAmt).toFixed(2);
                                    } else {
                                        return row.strPOAmt + ".00";
                                    }
                                }
                            },
                            mark: function (cell, data, row, col) { // This is used to add class for cell (Minimum Amount Css)
                                if (row.strPOAmt < 0)
                                    return "amt-min";
                            },footer: [{content: "sum"}] },
                        {width: 150,id: "strTotal",align: "right",header: [{text: "Order Amount",align: "center"}, {content: "inputFilter" }],type: "number",template: function (text, row, col) {
                                if (row.strTotal == 0) {
                                    return "0.00"
                                } else if (row.strTotal < 0) {
                                    row.strTotal =  row.strTotal.slice(1);
                                    if (row.strTotal % 1 != 0) {
                                        row.strTotal = parseFloat(row.strTotal + '0');
                                        return (row.strTotal).toFixed(2);
                                    } else {
                                        return row.strTotal + ".00";
                                    }
                                } else {
                                    if (row.strTotal % 1 != 0) {
                                        row.strTotal = parseFloat(row.strTotal + '0');
                                        return (row.strTotal).toFixed(2);
                                    } else {
                                        return row.strTotal + ".00";
                                    }
                                }
                            },
                            mark: function (cell, data, row, col) {//used to add class for cell (Minimum Amount Css)
                                if (row.strTotal < 0)
                                    return "amt-min";
                            },footer: [{content: "sum"}]},
                        {width: 150,id: "strDiffAmt",header: [{text: "Diff",align: "center"}, {content: "inputFilter"}],type: "number",template: function (text, row, col) {
                                if (row.strDiffAmt == 0) {
                                    return "0.00"
                                } else if (row.strDiffAmt < 0) {
                                    row.strDiffAmt =  row.strDiffAmt.slice(1);
                                    if (row.strDiffAmt % 1 != 0) {
                                        row.strDiffAmt = parseFloat(row.strDiffAmt + '0');
                                        return (row.strDiffAmt).toFixed(2);
                                    } else {
                                        return row.strDiffAmt + ".00";
                                    }
                                } else {
                                    if (row.strDiffAmt % 1 != 0) {
                                        row.strDiffAmt = parseFloat(row.strDiffAmt + '0');
                                        return (row.strDiffAmt).toFixed(2);
                                    } else {
                                        return row.strDiffAmt + ".00";
                                    }
                                }
                            },
                            mark: function (cell, data, row, col) { // used to add class for cell (Minimum Amount Css)
                                if (row.strDiffAmt < 0)
                                    return "amt-min";
                            }
                        },
                        {width: 180,id: "strOrderId",align: "left", header: [{text: "DO ID",align: "center" }, {
                         content: "inputFilter"}],htmlEnable: true,template: function (text, row, col) {
                                return "<a class='strOrdId' data-orderid='" + text + "' data-order-amt='" + row.strTotal + "' data-diff-amt='" + row.strDiffAmt + "' data-po-amt='" + row.strPOAmt + "'  data-podetail-id='" + row.strPONumDtlsId + "'><span class='po-update-btn'>" + text + "</span></a></div>";
                            }
                        },
                        {width: 280,id: "strRepAcc",align: "left",header: [{text: "Rep Account",align: "center"}, {
                        content: "inputFilter"}] },
                        {width: 150,id: "strDivisionNm",align: "left",header: [{text: "Division",align: "center"}, {
                        content: "inputFilter"}]},
                        {width: 250,id: "strFieldSales",align: "left",header: [{text: "Field Sales",align: "center"}, {content: "inputFilter"}]},
                        {width: 250,id: "strSalesRep",align: "left",header: [{text: "Sales Rep",align: "center"}, {
                        content: "inputFilter"}]},
                        {width: 150,id: "strPOStatusUpdatedBy",align: "left",header: [{text: "PO Status Updated By",
                        align: "center"}, {content: "inputFilter"}]},
                        {width: 150,id: "strPOStatusUpdatedDate",align: "left",header: [{text: "PO Status Updated Date"}, {content: "inputFilter"}]},
                        {width: 150,id: "strCustPOUpdatedBy",align: "left",header: [{text: "PO Entered By"}, {content: "inputFilter"}]}
                    ],
                    data: datacollection,
                    dragItem: "column",
                    rowHeight: 30,
                    keyNavigation: true,
                    height:380,
                    rowCss: function (row) {
                        return row.strCustomcss
                    },
                });
                datacollection.forEach(function (item, index, array) {
                    if (item.strChkHoldFl == 'Y') {
                        datacollection.update(item.id, {
                            strCustomcss: "dis-disabled"
                        });
                    }
                });

                $scope.grid = grid;

                /*This function used for handling  events*/
                const events = [
  "afterEditEnd",
   "headerCellClick",
   "cellClick",
   "beforeEditEnd",
   "beforeEditStart",
   "FilterChange"
];
                /*This function for grid event handler*/
                function eventHandler(event, arguments) {

                    // Cell click event  to open pdf viewer
                    if (event == 'cellClick') {

                        if (arguments[1].id == "strOrderId") {// while click order column values open Associated Model
                            $scope.strOrderAmt = arguments[0].strTotal;
                            $scope.strDiffAmt = arguments[0].strDiffAmt;
                            $scope.strPoAmt = arguments[0].strPOAmt;
                            $scope.fnAssociatedOrdersListModal(arguments[0].strPONumDtlsId);
                        }
                        if (arguments[1].id == "strDOFlag") {// while click file icon open fileupload model popup
                            if(arguments[0].strDOFlag=='Y'){
                            
                            $scope.strOrderId = arguments[0].strOrderId;
                            $scope.strCustomerPo = arguments[0].strCustPONumber;
                            $scope.strOrderandPO = "";
                            if ($scope.strCustomerPo != "") {
                                $scope.strOrderandPO += $scope.strOrderId + " (" + $scope.strCustomerPo + ")";
                            } else {

                                $scope.strOrderandPO = $scope.strOrderId;
                            }
                    /*this method call used to show file upload list when click "F" icon*/
                            $scope.fnUploadFileListModal();
                            }
                    /*Load right panel*/
                        }
                        if (arguments[1].id == "strOrdReceiveMode") {// while click pdf icon open pdf for ipad orders
                            if(arguments[0].strOrdReceiveMode=='26230683'){
                                 $scope.strOrderId = arguments[0].strOrderId;
                                 $scope.fnOpenDOPDF();
                            }
                        }
                    }

                    // Check all checkbox function
                    if (event == 'headerCellClick') {
                        var cnt = 0;
                        if (arguments[0].id == "chkFl") { // checkall checkbox 
                            $(".chckMaster").change(function () {
                                var chkflval = this.checked;
                                if (grid.data.getLength() == 1) {
                                    datacollection.forEach(function (item, index, array) {
                                        if (item.strChkHoldFl == 'Y') {
                                            datacollection.update(item.id, {
                                                chkFl: false
                                            });
                                        } else {
                                            datacollection.update(item.id, {
                                                chkFl: chkflval
                                            });
                                        }
                                        if (item.chkFl) {
                                            cnt = parseInt(cnt) + 1;
                                        }
                                    });
                                    $(".dhx_checkbox__input").prop("checked", chkflval);
                                } else {
                                    $(".dhx_checkbox__input").prop("checked", chkflval);
                                    datacollection.forEach(function (item, index, array) {
                                        if (item.strChkHoldFl == 'Y') {
                                            datacollection.update(item.id, {
                                                chkFl: false
                                            });
                                        } else {
                                            datacollection.update(item.id, {
                                                chkFl: chkflval
                                            });
                                        }
                                        if (item.chkFl) {
                                            cnt = parseInt(cnt) + 1;
                                        }
                                    });
                                }
                                $("#invoice-selected-row-count").html("No.Of row(s) selected <span class='pl-1 font-weight-bold'>" + cnt + "<span>");
                                $(".chckMaster").off("change");
                            });

                        }
                    }


                    // event for checkall by line item checkbox
                    if (event == 'afterEditEnd') {
                        if (arguments[2].id == "chkFl") {
                            var cnt = 0;
                            datacollection.forEach(function (item, index, array) {
                                if (item.chkFl) {
                                    cnt = parseInt(cnt) + 1;
                                }
                            });

                            if (grid.data.getLength() == cnt) {
                                $(".chckMaster").prop("checked", true);
                            } else {
                                $(".chckMaster").prop("checked", false);
                            }
                            $("#invoice-selected-row-count").html("No.Of row(s) selected <span class='pl-1 font-weight-bold'>" + cnt + "<span>");
                        }
                    }
                    //This event is used change checkbox event after filter
                    if(event == 'FilterChange'){
                        var cnt = 0;
                            datacollection.forEach(function (item, index, array) {
                                if (item.chkFl) {
                                    cnt = parseInt(cnt) + 1;
                                }
                            });
                         
                            if (grid.data.getLength() == cnt) {
                                $(".chckMaster").prop("checked", true);
                            } else {
                                $(".chckMaster").prop("checked", false);
                            }
                            $("#invoice-selected-row-count").html("No.Of row(s) selected <span class='pl-1 font-weight-bold'>" + cnt + "<span>");
                    }


                }
                events.forEach(function (event) {
                    grid.events.on(event, function () {
                        eventHandler(event, arguments);
                    });
                });
                $('.dhx_sample-controls').show();
                $('#pre-loader-main').hide();
               //Used to show/hide default column
                $('input[class="chk_column"]').each(function () {
                    if ($(this).prop("checked") == true) {
                        grid.showColumn($(this).val());

                    } else {
                        grid.hideColumn($(this).val());
                    }
                });
                 //Used to show/hide based on checkbox
                $('.chk_column').click(function () {
                    if ($(this).prop("checked") == true) {
                        grid.showColumn($(this).val());

                    } else {
                        grid.hideColumn($(this).val());
                    }
                });


            } 
        }
        else {
                $('#ready-for-invoicing-results').hide();
                $("#no-data-found").show();
                $('#btn-show').hide();
                $('.dhx_sample-controls').hide();
            }


    };
    //This function is used to export excel
    $scope.exportXlsx = function () {
        // Removing unwanted columns by array index
	grid.config.columns.splice(0, 1);
        grid.config.columns.splice(1, 2);
        grid.config.columns.splice(2, 2);
        grid.export.xlsx({
            name: "AutoInvoice",
            url: "//export.dhtmlx.com/excel"
        });
        $scope.fnLoadReadyForInvoice($scope.results);
    };
    $scope.fnOpenDOPDF = function () {
        var doPDFPath = "ipad-do-download/"+$scope.strCompanyLocale+"/"+$scope.strOrderId+".pdf";
        var downloadPDFURL = microappUrlFile+"downloadFile/"+doPDFPath;
        window.open(downloadPDFURL);
    };

    /*Function is used to move PO's to discrepancy*/
    $scope.fnMoveDiscrepancy = function () {
        var discrepancyPoId = '';
        var strdiscrepancyPoIdList = '';
        var invalidDiscrepancyPo = '';
        var strInvaliddiscrepancyPoList = '';
        var strDiffAmt = '';
        $scope.strDiscrepancyMsg = "";
        $scope.strdiscrepancyPOList = "";
        var count = 0;
         datacollection.forEach(function(item, index, array) {
				console.log(item);
             if(item.chkFl == true){
                 count++;
                strDiffAmt = item.strDiffAmt;
                 if (strDiffAmt == 0) {
                                       invalidDiscrepancyPo = item.strCustPONumber;
                                       strInvaliddiscrepancyPoList += invalidDiscrepancyPo + ', ';
                                       discrepancyPoId = item.strPONumDtlsId;
                                        strdiscrepancyPoIdList += discrepancyPoId + ',';
                                   } else {
                                       discrepancyPoId = item.strPONumDtlsId;
                                       strdiscrepancyPoIdList += discrepancyPoId + ',';
                                   }
             }
					 	});

        if (count > 0) {
            if (strInvaliddiscrepancyPoList.length > 0) {
                strInvaliddiscrepancyPoList = strInvaliddiscrepancyPoList.substring(0,
                    strInvaliddiscrepancyPoList.length - 2);
                $scope.strDiscrepancyMsg = varInvalidPOMoveToDiscrepancy;
                $scope.strinvalidDiscPO = strInvaliddiscrepancyPoList;
                $scope.strdiscrepancyPOList = strdiscrepancyPoIdList;
                $('#invalidDiscrepancy').modal();
                $('.dismiss').click();
            } else {
                $scope.fnMoveToDicrepancy(strdiscrepancyPoIdList);
            }
        } else {
            $scope.strDiscrepancyMsg = varEmptyPOFromMoveToDiscrepancy;
            $('#emptyInvalidDiscrepancy').modal();
            $('.dismiss').click();
        }

    };


    // [PC-2122] this function used to move selected PO's to Order Pending Screen through Processor Error
    $scope.fnMoveProcessorError = function() {
        var processorErrPoId = '';
        var strProcessorErrPoIdList = '';
        var processorErrPo = '';
        var strProcessorErrPoList = '';
        $scope.strProcessorErrMsg = "";
        var count = 0;
         datacollection.forEach(function (item, index, array) {
             if (item.chkFl == true) {
                 count++;
                 processorErrPo = item.strCustPONumber;
                 strProcessorErrPoList += processorErrPo + ', ';
                 processorErrPoId = item.strPONumDtlsId;
                 strProcessorErrPoIdList += processorErrPoId + ',';
             }
         });

         if (count > 0) {

            if (strProcessorErrPoList.length > 0) {
                strProcessorErrPoList = strProcessorErrPoList.substring(0,
                    strProcessorErrPoList.length - 2);
                $scope.strProcessorErrMsg = varPOMoveToProcessorErr;
                $scope.strPOMoveToProcessorErr = strProcessorErrPoList;
                $scope.strProcessorErrPoIdList = strProcessorErrPoIdList;
                $('#movetoProcessorErr').modal();
                $('.dismiss').click();
            } 
        } else {
            $scope.strProcessorErrMsg = varEmptyPOMoveToProcessorErr;
            $('#emptyMovetoProcessorErr').modal();
            $('.dismiss').click();
        }


    };


    //This function is used to check write/read access based access,po status and po-detail-id
    $scope.fnValidateAccessFlag = function () {
        $scope.statusFl = 'N';
        console.log("accessflag=>" + $scope.AccessFl);
        if ($scope.AccessFl == 'Y') {
            $scope.statusFl = 'Y';
        }

    };

    $scope.fnMoveToDicrepancy = function (strdiscrepancyPoIdList) {
        console.log("Enteredpoidlistinmodal=>" + strdiscrepancyPoIdList);
        $scope.gmARCustPOModel = {
            strOrderPODtlIdList: strdiscrepancyPoIdList, //Order PO Details Id list 
            strUserId: localStorage.getItem('strUserId') // Login user id
        };

        $http.post(url + 'saveDiscrepancyDetails/', angular.toJson($scope.gmARCustPOModel)).then(function (response) {

            if (response.data != undefined) {
                var results = response.data;
                $scope.fnLoadPOReportByStatus();
            } else {
                $scope.errorMsg = response.data.message;
                sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                $scope.errorHandler();
            }

        });

    };


    // [PC-2122] this fuction used to move selected PO's into Processor Error
    $scope.fnMoveToProcessorError = function (strProcessorErrPoIdList) {

        $scope.gmARCustPOModel = {
            strOrderPODtlIdList: strProcessorErrPoIdList, //Order PO Details Id list 
            strUserId: localStorage.getItem('strUserId') // Login user id
        };

        $http.post(url + 'saveProcessorErrorDetails/', angular.toJson($scope.gmARCustPOModel)).then(function (response) {

            if (response.data != undefined) {
                var results = response.data;
                datacollection.forEach(function(item, index, array) {
					 if(item.chkFl == true){
                         datacollection.update(item.id, { strCustomcss: "check-disabled"});
                         datacollection.update(item.id, { strChkHoldFl:'Y'});
                          datacollection.update(item.id, { chkFl:false});
					 }
                });
            } else {
                $scope.errorMsg = response.data.message;
                sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                $scope.errorHandler();
            }

        });

    };


    // this method used to show Associated orders popup
    $scope.fnAssociatedOrdersListModal = function (poDtlsId) {
        $('#associatedOrderModal').modal();
        $scope.fnFetchOrderDetailsByPO(poDtlsId);
        $scope.fnFetchPODetials(poDtlsId);
        $('.dismiss').click();
    };
    // this method is to get the Associated order pop up modal header
    $scope.fnFetchOrderDetailsByPO = function (poDtlsId) {
        $scope.gmARCustPOModel = {
            strPONumDtlsId: poDtlsId,
            strDtFormat: $scope.strDtFormat
        };
        $http.post(url + 'fetchOrdersByPO/', angular.toJson($scope.gmARCustPOModel)).then(function (response) {

            if (response.data != undefined) {
                //		    			$scope.strPOUpdatedDate = $filter('date')(response.data[0].strCustomerPODate, $scope.strDtFormat, $scope.strCompanyZone);
                $scope.strCustomerPo = response.data[0].strCustPONumber;
                $scope.strPOAmt = response.data[0].strPOAmt;
                $scope.strPOEnteredBy = response.data[0].strCustPOUpdatedBy;
                $scope.strPOUpdatedDate = response.data[0].strPOEnteredDate;
            }
        });

    };
    // this method is to get the Associated order pop up modal Body
    $scope.fnFetchPODetials = function (poDtlsId) {
        $scope.gmARCustPOModel = {
            strPONumDtlsId: poDtlsId,
            strDtFormat: $scope.strDtFormat
        };

        var table = $('#OrderDetailsListTable').DataTable();
        table.destroy();
        $http.post(url + 'fetchPODetails/', angular.toJson($scope.gmARCustPOModel)).then(function (response) {
            if ($scope.strOrderAmt - $scope.strPoAmt < 0) {
                document.getElementById("strAstOrdDiff").style.color = 'red';
            } else {
                document.getElementById("strAstOrdDiff").style.color = 'black';
            }
            if (response.data != undefined) {
                $scope.POOrderDtlList = response.data;

                angular.element(document).ready(function () {
                    table = $('#OrderDetailsListTable').DataTable({
                        "retrieve": true,
                        "bPaginate": false,
                        "bInfo": false,
                        "scrollY": "200px",
                        "scrollCollapse": true,
                        "dom": 'Bfrtip',
                        "language": {
                            "emptyTable": "No data found to display",
                            "zeroRecords": "No data found to display"
                        },
                        buttons: [],
                        "order": [],
                        "columnDefs": [{
                            "targets": 'no-sort',
                            "orderable": false
		                                 }]
                    });
                });
            };

        });
    };
    //File upload report testing code
    $scope.fnUploadFileListModal = function () {
        $('#fileUploadModal').modal();
        $scope.fnFetchUploadFile();
        $('.dismiss').click();
    };
    //this method used fetch upload files from DB
    $scope.fnFetchUploadFile = function () {
        var refID = $scope.strOrderId;
        var strDtFormat = $scope.strDtFormat;
        var strCompanyLocale = $scope.strCompanyLocale;
        var strFileUploadDownloadPath = "download-dir/" + strCompanyLocale + "/" + refID;
        $scope.downloadURL = microappUrlFile + "downloadFile/" + strFileUploadDownloadPath + "/";
        var refType = "53018"; // DO
        var refGroupType = "26240412"; // DO
        var strFetchInfoURL = microappUrlFile + "loadUploadFileDetails";
        if ($scope.AccessFl == 'N') {
            $scope.strAccess = 'none';
        }

        $scope.gmUploadFilePOModel = {
            strRefGrp: refGroupType, // DO
            strRefId: refID, // Order Id
            strRefType: refType, // DO
            strType: "0", // DO document upload
            strFileTitle: "0", // Upload File Title
            strFileName: "0", // Upload File Name
            strPartNumberId: "0", // Part Number Id
            strDateFormat: strDtFormat // Company Date Format
        };

        $scope.uploadFileList = '';
        var table = $('#fileUploadListTable').DataTable();
        table.destroy();

        $http.post(strFetchInfoURL, angular.toJson($scope.gmUploadFilePOModel)).then(function (response) {
            if (response != undefined) {
                $scope.uploadFileList = response.data;
                angular.element(document).ready(function () {
                    table = $('#fileUploadListTable').DataTable({
                        "bPaginate": true,
                        "pageLength": 5,
                        dom: 'Bfrtip',
                        "language": {
                            "emptyTable": "No data found to display",
                            "zeroRecords": "No data found to display"
                        },
                        buttons: [],
                        "order": [],
                        "columnDefs": [{
                            "targets": 'no-sort',
                            "orderable": false
                            }]
                    });
                });
            } else {
                $scope.errorMsg = response.data.message;
                sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                $scope.errorHandler();
            }
        });
    };

    //this method used to convert file size into respective storage units
    $scope.getFileSize = function (fileSize) {
        return fnGetFileSize(fileSize);
    };

    // this function used to set respective icon for file name 
    $scope.getFileType = function (fileName, index) {
        $('#filetype' + index).addClass($scope.getFileExtension(fileName));
    }
    // this function used to get file type icon class for respective file
    $scope.getFileExtension = function (fileName) {
        return getFileTypeIcon(fileName.split('.').pop().toLowerCase());
    }
    //onchange company dropdown to get company details
    $scope.fnchangeCompany = function (strCompId) {
        $scope.strCompanyId = strCompId;
        $http.get(microappUrl + 'loadCompanyInfo/' + strCompId).then(function (response) {
            if (response.data != undefined) {
                $scope.compInfoList = response.data;
                $scope.strCurrency = $scope.compInfoList[0].currency;
                $scope.strcurrencyNm = $scope.compInfoList[0].currencyNm;
                $scope.strDtFormat = $scope.compInfoList[0].dtFormat;
                $scope.strCmpCurrFmt = $scope.compInfoList[0].cmpCurrFmt;
                $scope.strCompanyId = $scope.compInfoList[0].companyId;
                $scope.strCompanyLocale = $scope.compInfoList[0].companyLocale;
                $scope.strCompanyZone = $scope.compInfoList[0].compZone;
                $scope.strLanguageId = $scope.compInfoList[0].languageId;
                $scope.strPlantId = $scope.compInfoList[0].plantId;
                localStorage.setItem("strLangId", $scope.strLanguageId);
                localStorage.setItem("strPlantId", $scope.strPlantId);
                localStorage.setItem("strCompZone", $scope.strCompanyZone);
                localStorage.setItem("strCompId", $scope.strCompanyId);

                $scope.fnSetDates($scope.strDtFormat);
                $scope.fnClearField();

            }
        });
    };


    var inputStrPODetails = "";
    var poInputStr = "";
    /*fnFormPODetls - This method is used to form the input string for having diff amount of order and zero amount of order */
    $scope.fnFormPODetls = function () {
        var inpuStrPODetls = "";
        var inputStrPONum = "";

        datacollection.forEach(function (item, index, array) {
            console.log(item);
            if (item.chkFl == true) {
                var id = item.id;
                    strDiffAmt = item.strDiffAmt;
                    if (strDiffAmt != 0) {
                        //for po details id to create batch
                        po_dtls_id = item.strPONumDtlsId;
                        inputStrPODetails += po_dtls_id + ',';
                        //for po number should show on confirm message
                        po_num = item.strCustPONumber;
                        inputStrPONum += po_num + ', ';
                    } else {
                        strPODetailId = item.strPONumDtlsId;
                        poInputStr += strPODetailId + ',';
                    }
            }

        });
  
        if (inputStrPONum == "") {
            inpuStrPODetls = inputStrPODetails + poInputStr;
            $scope.fnAddProcessBatch(inpuStrPODetls);
        } else {
            inputStrPONum = inputStrPONum.substring(0, inputStrPONum.length - 2);
            $scope.strPONum = inputStrPONum;
            $("#show-batch-confirm").modal();
            $('.dismiss').click();
        }
    };

    /*This method to click the OK button in confirm message
     If click OK to concat the having diff amount of PO rows and zero amount of PO rows */
    $scope.fnFormDiffAmtPODetls = function () {
        $('#show-batch-confirm').modal('hide');
        var po_dtls_id = "";
        po_dtls_id = inputStrPODetails + poInputStr;
        $scope.fnAddProcessBatch(po_dtls_id);
    }

    /*This method to click the OK button in confirm message
    If click OK to concat the having diff amount of PO rows and zero amount of PO rows */
    $scope.fnCancelBtnAddProcess = function () {
        $('#show-batch-confirm').modal('hide');
        poInputStr = "";
        inputStrPODetails = "";
    }

    /*This Function is used to create the batch and process the batch */
    $scope.fnAddProcessBatch = function (inpuStrPODetls) {
        if (inpuStrPODetls != "") {
            $scope.inputStr = inpuStrPODetls;
            $scope.strCompanyId = localStorage.getItem("strCompId");
            $scope.strLangId = localStorage.getItem("strLangId");
            $scope.strPlantId = localStorage.getItem("strPlantId");
            $scope.strUserId = localStorage.getItem("strUserId");
            $scope.strPartyId = localStorage.getItem("strPartyId");
            $scope.strCompZone = localStorage.getItem("strCompZone");


            $scope.gmARInvoiceModel = {
                inputStr: $scope.inputStr,
                strCompanyId: $scope.strCompanyId,
                strUserId: $scope.strUserId,
                strCompZone: $scope.strCompZone,
                strPlantId: $scope.strPlantId

            };

            $http.post(url + 'saveBatchProcess', angular.toJson($scope.gmARInvoiceModel)).then(function (response) {

                if (response.data != "") {
                    $scope.fnSetRowCountEmpty();
                    $scope.strMessage = response.data;
                    if (!isNaN($scope.strMessage)) {
                        $('#show-batch').show();
                        $('.dismiss').click();

                        $scope.gmARInvoiceCommonModel = {
                            strMessage: $scope.strMessage,
                            strCompanyInfo: $scope.strCompanyId,
                            strUserId: $scope.strUserId,
                            strCompanyId: $scope.strCompanyId,
                            strLangId: $scope.strLangId,
                            strPlantId: $scope.strPlantId,
                            strPartyId: $scope.strPartyId
                        };
                        $http.post(url + 'arbatchjmscall', angular.toJson($scope.gmARInvoiceCommonModel)).then(function (response) {
                            if (response.data != undefined) {
                                inputStrPODetails = "";
                                 poInputStr = "";
                                datacollection.forEach(function(item, index, array) {
					 if(item.chkFl == true){
                         datacollection.update(item.id, { strCustomcss: "check-disabled"});
                         datacollection.update(item.id, { strChkHoldFl:'Y'});
                         datacollection.update(item.id, { chkFl:false});
					 }
				 });


                            }
                        });
                    } else {
                        inputStrPODetails = "";
                        poInputStr = "";
                    }
                } else {
                    $("#show-batch-error").modal();
                    $('.dismiss').click();
                }
            });
        } else {
            $("#show-batch").hide();
            $("#show-CheckAll").modal();
            $('.dismiss').click();
        }
    };
    
    //clear the all field
    $scope.fnClearField = function () {
        $scope.strDivisionId = "";
        $scope.strParentAccId = "";
        $scope.strFieldSalesId = "";
        $scope.strSalesRepId = "";
        $scope.strCustPONumber = "";
        $scope.strAccid = "";
        $scope.strRepAccId = "";
        $scope.strRepAcc = "";
        $scope.strFieldSales = "";
        $scope.strSalesRep = "";
        $scope.strParentAcc = "";
        $scope.strPOEnteredFDate = "";
        $scope.strPOEnteredTDate = "";
    };


    //this function is clear all the field while click on clear button 
    $scope.fnClearAllField = function () {

        $scope.strDivisionId = "";
        $scope.strParentAccId = "";
        $scope.strParentAcc = "";
        $scope.strRepAccId = "";
        $scope.strRepAcc = "";
        $scope.strAccid = "";
        $scope.strFieldSalesId = "";
        $scope.strFieldSales = "";
        $scope.strSalesRepId = "";
        $scope.strSalesRep = "";
        $scope.strCustPONumber = "";
        $scope.strPOEnteredFDate = "";
        $scope.strPOEnteredTDate = "";
        $scope.hidethisacc = true;
        $("#list-group-acc").removeClass("searchResults");
        $scope.hidethisrep = true;
        $("#list-group-rep").removeClass("searchResults");
        $scope.hidethisfield = true;
        $("#list-group-field").removeClass("searchResults");
        $scope.hidethissales = true;
        $("#list-group-sales").removeClass("searchResults");
    };

    //Clear redis dropdown id 
    $scope.fnClearRedisId = function () {
        var strParentAccNm = $scope.strParentAccId;
        var strRepAccNm = $scope.strRepAccId;
        var strFieldSalesNm = $scope.strFieldSalesId;
        var strSalesRepNm = $scope.strSalesRepId;

        if (strParentAccNm == '') {
            $scope.strParentAcc = "";
        }
        if (strRepAccNm == '') {
            $scope.strRepAcc = "";
        }
        if (strFieldSalesNm == '') {
            $scope.strFieldSales = "";
        }
        if (strSalesRepNm == '') {
            $scope.strSalesRep = "";
        }

    };


    /*toggle button class add and remove*/
    $scope.fnColapseFilter = function () {
        if ($('#collapseFilter').hasClass('collapse show')) {
            $('#collapseFilter').removeClass('show');
        } else {
            $('#collapseFilter').addClass('collapse show');
        }

        if ($('#btnCollapseFilter').hasClass('btn btn-xs btn-outline-dark collapsed')) {
            $('#btnCollapseFilter').removeClass('collapsed');
        } else {
            $('#btnCollapseFilter').addClass('btn btn-xs btn-outline-dark collapsed');
        }

    };

    /*Web Service Call for Parent Account details  based on login user*/
    $scope.fnSearchParentAccount = function () {
        var strSearchValue = $scope.strParentAccId.toLowerCase();
        if (strSearchValue.length >= 3) {
            $scope.hidethisacc = false;
            var strScreenRedisKey = 'parentAccountRedis';
            $http.get(microappUrl + 'loadRedisDtls' + '/' + strScreenRedisKey + '/' + strSearchValue + '/' + '100' + '/' + $scope.strCompanyId).then(function (response) {
                if (response.data != "") {
                    $scope.parentAccList = response.data;
                    $("#list-group-acc").addClass("searchResults");
                } else {
                    $scope.parentAccList = [{
                        "ID": "",
                        "NAME": "No Data Found"
                    }];
                }
            });
        } else {
            $scope.hidethisacc = true;
            $("#list-group-acc").removeClass("searchResults");
        }
    };

    /*Web Service Call for Rep Account details  based on login user*/
    $scope.fnSearchRepAccount = function () {
        var strSearchValue = $scope.strRepAccId.toLowerCase();
        if (strSearchValue.length >= 3) {
            $scope.hidethisrep = false;
            var strScreenRedisKey = 'repAccountRedis';
            $http.get(microappUrl + 'loadRedisDtls' + '/' + strScreenRedisKey + '/' + strSearchValue + '/' + '100' + '/' + $scope.strCompanyId).then(function (response) {
                if (response.data != "") {
                    $scope.repAccList = response.data;
                    $("#list-group-rep").addClass("searchResults");
                } else {
                    $scope.repAccList = [{
                        "ID": "",
                        "NAME": "No Data Found"
                    }];
                }
            });

        } else {
            $scope.hidethisrep = true;
            $("#list-group-rep").removeClass("searchResults");
        }
    };
    /*Web Service Call for Field Sales details  based on login user*/
    $scope.fnSearchFieldSales = function () {
        var strSearchValue = $scope.strFieldSalesId.toLowerCase();
        if (strSearchValue.length >= 3) {
            $scope.hidethisfield = false;
            var strScreenRedisKey = 'distRedis';
            $http.get(microappUrl + 'loadRedisDtls' + '/' + strScreenRedisKey + '/' + strSearchValue + '/' + '100' + '/' + $scope.strCompanyId).then(function (response) {
                if (response.data != "") {
                    $scope.fieldSalesList = response.data;
                    $("#list-group-field").addClass("searchResults");
                } else {
                    $scope.fieldSalesList = [{
                        "ID": "",
                        "NAME": "No Data Found"
                    }];
                }
            });

        } else {
            $scope.hidethisfield = true;
            $("#list-group-field").removeClass("searchResults");
        }
    };
    /*Web Service Call for Sales Rep details  based on login user*/
    $scope.fnSearchSalesRep = function () {
        var strSearchValue = $scope.strSalesRepId.toLowerCase();
        if (strSearchValue.length >= 3) {
            $scope.hidethissales = false;
            var strScreenRedisKey = 'salesRepRedis';
            $http.get(microappUrl + 'loadRedisDtls' + '/' + strScreenRedisKey + '/' + strSearchValue + '/' + '100' + '/' + $scope.strCompanyId).then(function (response) {
                if (response.data != "") {
                    $scope.salesRepList = response.data;
                    $("#list-group-sales").addClass("searchResults");
                } else {
                    $scope.salesRepList = [{
                        "ID": "",
                        "NAME": "No Data Found"
                    }];
                }
            });

        } else {
            $scope.hidethissales = true;
            $("#list-group-sales").removeClass("searchResults");
        }
    };


    /*This is used for set the date picker details */
    $scope.fnSetDates = function (strDateFormat) {
        var strDtFormat = (strDateFormat).toLowerCase();

        $(".inputDatePicker").datepicker('destroy');
        $('.inputDatePicker').attr("placeholder", strDtFormat);
        $('.inputDatePicker').datepicker({
            format: strDtFormat,
            todayHighlight: true,
            autoclose: true,
        });

        $("#poEnteredFDate").datepicker();
        $("#poEnteredTDate").datepicker();

        /*Calendar icon click focus input fields*/
        $('.poEnteredFDate').click(function () {
            $("#poEnteredFDate").focus();
            $scope.fnValidateFPOEntDate();
        });

        /*Calendar icon click focus input fields*/
        $('.poEnteredTDate').click(function () {
            $("#poEnteredTDate").focus();
            $scope.fnValidateTPOEntDate();
        });

    };


    //this function used to remove validation message when click Calendar Icon of PO Entered From Date Field
    $scope.fnValidateFPOEntDate = function () {
        $("#span-empty_poFdt-err").hide();
    };
    //this function used to remove validation message when click Calendar Icon of PO Entered To Date Field
    $scope.fnValidateTPOEntDate = function () {
        $("#span-empty_poTdt-err").hide();
        $("#span-dt_compare_poFdt-err").hide();
    };


    //Set id for Parent account search auto search
    $scope.fnParentAccItem = function ($event) {
        $scope.strParentAcc = angular.element($event.target).attr("data-id");
        $scope.strParentAccId = angular.element($event.target).attr("data-name");
        $scope.hidethisacc = true;

    };

    //Set id for Rep account search auto search
    $scope.fnRepAccItem = function ($event) {
        $scope.strRepAcc = angular.element($event.target).attr("data-id");
        $scope.strRepAccId = angular.element($event.target).attr("data-name");
        $scope.hidethisrep = true;

    };

    //Set id for Field sales search auto search
    $scope.fnFieldSalesItem = function ($event) {
        $scope.strFieldSales = angular.element($event.target).attr("data-id");
        $scope.strFieldSalesId = angular.element($event.target).attr("data-name");
        $scope.hidethisfield = true;

    };

    //Set id for sales Rep search auto search
    $scope.fnSalesRepItem = function ($event) {
        $scope.strSalesRep = angular.element($event.target).attr("data-id");
        $scope.strSalesRepId = angular.element($event.target).attr("data-name");
        $scope.hidethissales = true;
    };

    //Summary Report DO Function for order id in associated order popup modal
    $scope.fnSummaryDO = function (ordID) {
        var strPartyId = localStorage.getItem("strPartyId");
        var strCompId = localStorage.getItem("strCompId");
        var strLangId = localStorage.getItem("strLangId");
        var strPlantId = localStorage.getItem("strPlantId");
        var companyInfo = {};
        //form the company info to pass the URL
        companyInfo.cmpid = strCompId;
        companyInfo.plantid = strPlantId;
        companyInfo.token = "";
        companyInfo.partyid = strPartyId;
        companyInfo.cmplangid = strLangId;
        var StringifyCompanyInfo = JSON.stringify(companyInfo);

        var features = 'directories=no,menubar=no,status=no,titlebar=no,toolbar=no,width=800,height=800';
        var url = 'GmEditOrderServlet?hMode=PrintPrice&hOrdId=' + ordID;
        window.open(encodeURI(window.location.protocol+'//'+window.location.hostname+'/'+url+'&companyInfo='+StringifyCompanyInfo, 'DO Summary', features));
    };
    
    // PC-2541-Show Selected Row Count on Auto Invoice Screen
    $scope.fnSetRowCountEmpty = function () {
        $("#invoice-selected-row-count").empty();
        selectRowCount = 0;
    };
});

// to select all check box when click select all option
function fnChangedFilter(obj) {
    selectRowCount = 0;
    if (obj.checked) {
        for (var i=0; i<gridObj.getRowsNum(); i++){ 
            var id=gridObj.getRowId(i);
            var check_disable_fl = gridObj.cellById(id, check_rowId)
                .isDisabled();
            if (!check_disable_fl) {
                gridObj.cellById(id, check_rowId).setChecked(true);
                selectRowCount++;
            }
        }
    } else {
        for (var i=0; i<gridObj.getRowsNum(); i++){ 
            var id=gridObj.getRowId(i);
            var check_id = gridObj.cellById(id, check_rowId).getValue();
            if (check_id == 1) {
            var check_disable_fl = gridObj.cellById(id, check_rowId)
                .isDisabled();
            if (!check_disable_fl) {
               gridObj.cellById(id, check_rowId).setChecked(false);

            }
            }
        } 
    }
    $("#invoice-selected-row-count").html("No.Of row(s) selected <span class='pl-1 font-weight-bold'>"+ selectRowCount +"<span>");
}


//Description : This function to check the select all check box
function fnSelectAllToggle(varControl) {

    var all_row = gridObj.getAllRowIds(",");
    var all_rowLen = all_row.length; // get the rows id length
    var checked_row = gridObj.getCheckedRows(check_rowId);
    var finalval;

    finalval = eval(checked_row.length);
    if (all_rowLen == finalval) {
        //  objControl.checked = true;// select all check box to be checked
        document.getElementById("selectAll").checked = true; // select all check box to be checked.
    } else {
        //  objControl.checked = false;// select all check box un checked.
        document.getElementById("selectAll").checked = false; // select all check box un checked.
    }
}


// this function is used display the hold icon in ready for invoice screen
function eXcell_holdFl(cell) {
    // the eXcell name is defined here
    if (cell) { // the default pattern, just copy it
        this.cell = cell;
        this.grid = this.cell.parentNode.grid;
    }
    this.setValue = function (val) {
        if (val == 'Y') {
            this.setCValue("<img alt=\"DO on hold status\" title=\"DO on Hold Status\" src=\"/images/hold-icon.png\" border=\"0\" disabled=\"true\" style='cursor:pointer'>",
                val);
        }

    }

    this.getValue = function () {
        return this.cell.childNodes[0].innerHTML; // gets the value
    }
};
eXcell_holdFl.prototype = new eXcell;


// this function is used display the File icon in ready for invoice screen
function eXcell_fileFl(cell) {
    // the eXcell name is defined here
    if (cell) { // the default pattern, just copy it
        this.cell = cell;
        this.grid = this.cell.parentNode.grid;
    }
    this.setValue = function (val) {
        if (val == 'Y') {

            this.setCValue("<img alt=\"File Upload Status\" title=\"File Upload Status\" src=\"/images/icon-letter.png\" border=\"0\" disabled=\"true\" style='cursor:pointer'>",
                val);
        }
    }

    this.getValue = function () {
        return this.cell.childNodes[0].innerHTML; // gets the value
    }
};
eXcell_fileFl.prototype = new eXcell;

// this function is used display the PDF icon in ready for invoice screen
function eXcell_PDFFl(cell) {
    // the eXcell name is defined here
    if (cell) { // the default pattern, just copy it
        this.cell = cell;
        this.grid = this.cell.parentNode.grid;
    }
    this.setValue = function (val) {
        if (val == '26230683') {
            this.setCValue("<img alt=\"iPad PDF\" title=\"iPad PDF\" src=\"/images/pdf_icon.gif\" border=\"0\" disabled=\"true\" style='cursor:pointer'>", val);
        }
    }
    this.getValue = function () {
        return this.cell.childNodes[0].innerHTML; // gets the value
    }
};
eXcell_PDFFl.prototype = new eXcell;