// this function is used to load user details 
function fnLoadUserInfo($http,loginNm,callback){
	$http.get(microappUrl+'loadUserInfo/'+loginNm).then(function (response) {
		if(response != undefined){
			return callback(response);
		}
	});
};

// this function is used to get company list 	
function fnLoadCompany($http,partyId,callback){
	$http.get(microappUrl+'loadCompany/'+partyId+'/105401,105400').then(function (response) {
		if(response != undefined){
			return callback(response);
		}
	});
};

// this function is used to load details 
function fnLoadCompanyInfo($http,varCompId,callback){
	$http.get(microappUrl+'loadCompanyInfo/' + varCompId).then(function (response) {
		if(response != undefined){
			return callback(response);
		}
	});
}; 
	
// this function is used to get devision list
function fnLoadDivision($http,callback){
	$http.get(microappUrl+'loadDivision').then(function (response) {
		if(response != undefined){
			return callback(response);
		}
	});
};

//This event  used to get the security access
function fnSecurityEvent($http,partyid,event,callback) {
    $http.get(microappUrl+'getAccessPermissions/'+partyid+'/'+event).then(function (response) {
    	if (response.data != undefined) {
    		return callback(response);
        }
    });
};

 // this function is used display the hold icon in po report screen 
function eXcell_holdFl(cell) {
	// the eXcell name is defined here
	if (cell) { // the default pattern, just copy it
		this.cell = cell;
		this.grid = this.cell.parentNode.grid;
	}
	this.setValue = function(val) {
		if(val =='Y'){
		this.setCValue("<a href=\"#\"><img alt=\"DO on hold status\" title=\"DO on Hold Status\" src=\"/images/hold-icon.png\" border=\"0\" disabled=\"true\"></a>",
						val);
		}
	}

	this.getValue = function() {
		return this.cell.childNodes[0].innerHTML; // gets the value
	}
};
eXcell_holdFl.prototype = new eXcell; 

//Event is used to save reject comments log

function fnCommonCancel($http,gmCommonCancelLog,callback){
    
       $http.post(microappUrl+'saveCommonCancel',angular.toJson(gmCommonCancelLog)).then(function (response) {
            if(response != undefined){
			return callback(response);
		}
        });
}



//this function is used to send Email of Reject PO
function fnSendEmail($http,gmCommonEmailModel,callback){
    
    $http.post(microappUrl+'sendEmail',angular.toJson(gmCommonEmailModel)).then(function (response) {
         if(response != undefined){
         return callback(response);
     }
     });
}



//this event is used to save commom log commnets

function fnSaveLogComments($http,gmCommonLog,callback){
    $http.post(microappUrl+'saveCommonLog',angular.toJson(gmCommonLog)).then(function (response) {
        if(response != undefined){
			return callback(response);
		}
    });
}
//dateDiff method is used for identify the date difference for all format 
function dateDiff(dt1, dt2, format, callback)
{
	var one_day=1000*60*60*24;
	var date1;
	var date2;
	var Diff;
	if(format==undefined || format==null || format=='' || format=='dd/MM/yyyy' || format=='MM/dd/yyyy'){
		Arrdt1 =  dt1.split("/");
		Arrdt2 =  dt2.split("/");	
	}else{
		Arrdt1 =  dt1.split(".");
		Arrdt2 =  dt2.split(".");
	}
	
	if(format!=undefined && (format=='dd/MM/yyyy' || format=='dd.MM.yyyy')){
		date1 = new Date (Arrdt1[2],Arrdt1[1]-1,Arrdt1[0]); // doing -1 for month as in js the month is from 0-11 and not 1-12
		date2 = new Date (Arrdt2[2],Arrdt2[1]-1,Arrdt2[0]);		
		Diff=Math.ceil((date2.getTime()-date1.getTime())/(one_day)); 
	}else{	
		date1 = new Date (Arrdt1[2],Arrdt1[0]-1,Arrdt1[1]); // doing -1 for month as in js the month is from 0-11 and not 1-12
		date2 = new Date (Arrdt2[2],Arrdt2[0]-1,Arrdt2[1]);		
		Diff=Math.ceil((date2.getTime()-date1.getTime())/(one_day)); 
	}
	if(Diff != undefined){
		return callback(Diff);
	}
}
//this event is used to fetch comments
function fnLoadComments($http,orderid,logtype,callback){
    $http.get(microappUrl + 'loadCallLog/' + orderid + '/'+logtype).then(function (response) {
        if(response != undefined){
			return callback(response);
		}
        
    });
    
}

//this function used to check alpha and special characters in given input
function fnCheckSpecialChracter(inputValue)
{
      if(inputValue.match(/[ ,.a-zA-Z]/g))
      {
      return true;
      }
      else
      {
      return false;
      }
}


//this function is used to get code lookup values
function fnCodeLookUpValues($http,$scope,codeGrp,callback){
    $scope.gmCodeLookup = {
      codeGrp : codeGrp  
    };
    $http.post(microappUrl+'loadCodeLookup',angular.toJson($scope.gmCodeLookup)).then(function (response) {
        if(response != undefined){
			return callback(response);
		}
    });
       
}


// this function used to load file icon type 
function getFileTypeIcon(t, e) {
    switch (t) {
        case "jpg":
        case "jpeg":
        case "gif":
        case "png":
        case "bmp":
        case "tiff":
        case "pcx":
        case "svg":
        case "ico":
            return "fa-file-image-o icon-color-img";
        case "avi":
        case "mpg":
        case "mpeg":
        case "rm":
        case "move":
        case "mov":
        case "mkv":
        case "flv":
        case "f4v":
        case "mp4":
        case "3gp":
        case "wmv":
        case "webm":
        case "vob":
            return "fa-file-video-o";
        case "rar":
        case "zip":
        case "tar":
        case "tgz":
        case "arj":
        case "gzip":
        case "bzip2":
        case "7z":
        case "ace":
        case "apk":
        case "deb":
        case "zipx":
        case "cab":
        case "tar-gz":
        case "rpm":
        case "xar":
            return "fa-file-archive-o";
        case "xlr":
        case "xls":
        case "xlsm":
        case "xlsx":
        case "ods":
        case "csv":
        case "tsv":
            return "fa-file-excel-o icon-color-excel";
        case "doc":
        case "docx":
        case "docm":
        case "dot":
        case "dotx":
        case "odt":
        case "wpd":
        case "wps":
        case "pages":
            return "fa-file-word-o icon-color-word";
        case "wav":
        case "aiff":
        case "au":
        case "mp3":
        case "aac":
        case "wma":
        case "ogg":
        case "flac":
        case "ape":
        case "wv":
        case "m4a":
        case "mid":
        case "midi":
            return "fa-file-audio-o";
        case "pot":
        case "potm":
        case "potx":
        case "pps":
        case "ppsm":
        case "ppsx":
        case "ppt":
        case "pptx":
        case "pptm":
        case "odp":
        case "psd":
            return "fa-file-powerpoint-o";
        case "html":
        case "htm":
        case "eml":
            return "fa-globe";
        case "exe":
            return "fa-desktop";
        case "dmg":
            return "fa-apple";
        case "pdf":
        case "ps":
        case "eps":
            return "fa-file-pdf-o icon-color-pdf";
        case "txt":
        case "djvu":
        case "nfo":
        case "xml":
            return "fa-file-text-o icon-color-text";
        case "msg":
        case "ost":
        case "pst":
            return "fa-envelope-o icon-color-message";
        default:
            return "fa-file-o"
    }
};

//this method used to convert file size into respective storage units
function fnGetFileSize(fileSize)
{
    var inputFileSize = fileSize;

        if (fnCheckSpecialChracter(inputFileSize)) {
            inputFileSize = inputFileSize.substring(0, inputFileSize.indexOf('.'));
            inputFileSize = inputFileSize.replace(/[ ,.]/g, "");
        }
        inputFileSize = inputFileSize * 1024; // file size is multiplied with size 1024 to show file storage units in KB and MB[ex 6KB, 5MB]
        var c = false;
        var a = ["B", "KB", "MB", "GB", "TB", "PB", "EB"];
        for (var e = 0; e < a.length; e++) {
            if (inputFileSize > 1024) {
                inputFileSize = inputFileSize / 1024
            } else {
                if (c === false) {
                    c = e
                }
            }
        }
        if (c === false) {
            c = a.length - 1
        }
        return Math.round(inputFileSize * 100) / 100 + " " + a[c];
}

//this function is used to fetch AD User List
function fnFetchuserInfoValues($http,$scope,codeId,strCompId,callback){
    $scope.gmUserInfoLookup = {
    		strCodeId : codeId,
    		strCompanyId : strCompId
    };
    $http.post(microappUrl+'userInfoLookup',angular.toJson($scope.gmUserInfoLookup)).then(function (response) {
        if(response != undefined){
			return callback(response);
		}
    });
}
//this function is used to active left link class
function activeLeftLinkClass(){
    var url      = (window.location.href).split('#/');
         if( url[1] == 'gmautoinvoice'){
           $("#li-order-pending").removeClass("active");
           $("#li-discrepancy-po").removeClass("active");
           $("#li-auto-invoice").addClass("active"); 
           $("#gmorderpendingpo").removeClass("active");
           $("#gmardiscrepancy").removeClass("active");
           $("#gmautoinvoice").addClass("active"); 
          }
         else if( url[1] == 'gmardiscrepancy'){
            $("#li-order-pending").removeClass("active");
            $("#li-auto-invoice").removeClass("active");
            $("#li-discrepancy-po").addClass("active"); 
            $("#gmorderpendingpo").removeClass("active");
            $("#gmautoinvoice").removeClass("active");            
            $("#gmardiscrepancy").addClass("active");
         }
        else{
            $("#li-order-pending").addClass("active");
            $("#li-discrepancy-po").removeClass("active");
            $("#li-auto-invoice").removeClass("active");
            $("#gmautoinvoice").removeClass("active");            
            $("#gmardiscrepancy").removeClass("active");
           $("#gmorderpendingpo").addClass("active");
        }
}

//function to improve pdf scale obj
function scaleCanvas(canvas, context, width, height) {
	  // assume the device pixel ratio is 1 if the browser doesn't specify it
	  const devicePixelRatio = window.devicePixelRatio || 1;

	  // determine the 'backing store ratio' of the canvas context
	  const backingStoreRatio = (
	    context.webkitBackingStorePixelRatio ||
	    context.mozBackingStorePixelRatio ||
	    context.msBackingStorePixelRatio ||
	    context.oBackingStorePixelRatio ||
	    context.backingStorePixelRatio || 1
	  );

	  // determine the actual ratio we want to draw at
	  const ratio = devicePixelRatio / backingStoreRatio;

	  if (devicePixelRatio !== backingStoreRatio) {
	    // set the 'real' canvas size to the higher width/height
	    canvas.width = width * ratio;
	    canvas.height = height * ratio;

	    // ...then scale it back down with CSS
	    canvas.style.width = width + 'px';
	    canvas.style.height = height + 'px';
	  }
	  else {
	    // this is a normal 1:1 device; just scale it simply
	    canvas.width = width;
	    canvas.height = height;
	    canvas.style.width = '';
	    canvas.style.height = '';
	  }

	  // scale the drawing context so everything will work at the higher ratio
	  context.scale(ratio, ratio);
	}

// function to view pdf online
function fnPdfViewer(modalobj,obj,url,loaderobj,myCanavs){
	 pdfjsLib.getDocument(url).then(doc => {
			 
			var myCanWid=0;
			var myCanHt = 0;
			for(var num=1;num<=doc._pdfInfo.numPages;num++){
			doc.getPage(num).then(page => {
			
				var  canvas = document.createElement('canvas');
				
				var context = canvas.getContext("2d");
				
				var viewport = page.getViewport(1.5);
				myCanWid = parseInt(myCanWid) + parseInt(viewport.width);
				myCanHt = parseInt(myCanHt) + parseInt(viewport.height);
				canvas.width = viewport.width;
				canvas.height = viewport.height;
				myCanavs.appendChild(canvas);
				scaleCanvas(canvas,context,viewport.width,viewport.height);
				page.render({
					canvasContext : context,
					viewport : viewport
				});
				 				
			});
			}
				 			 
			 $(loaderobj).hide();
			 $(obj).removeClass('hide');
		});
}


//this function is used to get code lookup values by company exclude values
function fnCodeLookUpValuesExclude($http,$scope,codeGrp,deptId,companyId,callback){
    $scope.gmCodeLookupExclude = {
      codeGrp : codeGrp,
      deptId  : deptId,
      companyId : companyId
    };
    $http.post(microappUrl+'loadCodeLookupExclude',angular.toJson($scope.gmCodeLookupExclude)).then(function (response) {
        if(response != undefined){
			return callback(response);
		}
    });
       
}

//this function is used to get wikiurl and redirect to wiki page
function fnHelp(wikikey){
    var url = wikiUrl + window[''+wikikey];
    window.open(url, "", "width=1000,height=600");
}
