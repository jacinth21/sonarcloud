var keycloakService = {};
var config = keycloakConfig;

keycloakService.keycloak = Keycloak(config);
keycloakService.token;
keycloakService.parsedToken;
keycloakService.userInfo;
keycloakService.userProfile;

'use strict';

var app = angular.module('ARApplication', [ 'ngRoute','ngFileUpload' ]);
const KeycloakService = keycloakService;
function TokenInjectorInterceptor() {
	return {
		request: function (config) {
			config.headers['Authorization'] = 'Bearer ' + KeycloakService.getCurrentTOken();
			return config;
		},

		requestError: function (config) {
			return config;
		},

		response: function (res) {
			return res;
		},

		responseError: function (res) {
			return res;
		}
	}
}

app.factory('tokenInjector', TokenInjectorInterceptor);
//This factory is used to store/set the $scope Values of One Controller and we can get that $scope value in another Controller 
app.factory('ParentScopes', function ($rootScope) {
    var mem = {};
 
    return {
        set: function (key, value) {
            mem[key] = value;
        },
        get: function (key) {
            return mem[key];
        }
    };
});
app.config(function($httpProvider) { 
	console.log($httpProvider) 
	$httpProvider.interceptors.push('tokenInjector');
});

app.config(function($routeProvider) {

$routeProvider

.when('/', {
	templateUrl : 'template/GmOrdersPendingPO.html',
	controller : 'GmPendingPOController'
	})
.when('/gmorderspendingpo', {
    templateUrl : 'template/GmOrdersPendingPO.html',
    controller : 'GmPendingPOController'
    })
.when('/gmautoinvoice', {
	templateUrl : 'template/GmARInvoice.html',
	controller : 'GmARInvoiceController'
	})
.when('/gmardiscrepancy', {
	templateUrl : 'template/GmARDiscrepancy.html',
	controller : 'GmDiscrepancyController'
	})
.when('/gmrevenuedashboard', {
	templateUrl : 'template/GmARRevenuePDFTrack.html',
	controller : 'GmARRevenuePDFTrackController'
	})
.when('/gmrevenuereport', {
	templateUrl : 'template/GmARRevenuePDFTrack.html',
	controller : 'GmARRevenuePDFTrackController'
	})
.when('/gmdiscdetailreport', {
            templateUrl: 'template/GmDiscrepancyDtlReport.html',
            controller: 'GmDiscrepancyDtlReportController'
        })
	
});

app.controller('GmCommonController', function($scope) {
	var varUserName = localStorage.getItem('userName');
	$scope.varUserName = varUserName;
});

app.controller("GmUIErrorController",function($scope, $http, $q) {
	var AfterAuthentication = function(){
		var deferred = $q.defer();
		while(!KeycloakService.keycloak.authenticated){}
		deferred.resolve(KeycloakService.keycloak.authenticated);
		return deferred.promise;
	}
	var varErrorMsg =sessionStorage.getItem("errorMsg");
	if(varErrorMsg != undefined){
		$scope.varErrorMsg = varErrorMsg;
	}else {
		$scope.varErrorMsg = "The server responded with a status of 400 (Bad Request)";
	}
});

keycloakService.updateToken = function () {
    setInterval(function(){
        keycloakService.keycloak.updateToken(10).success(function () {
            console.log('Token updated');
            console.log(keycloakService.getCurrentTOken());
            localStorage.setItem('keycloak_token', keycloakService.getCurrentTOken());
        }).error(function (e) {
            console.log(e)
        });
    }, (((accessTokenLifespanInMinutes * 60) - 10) * 1000));
}

keycloakService.onAuthentication = function (authenticated) {
    angular.bootstrap(document, ['ARApplication'])
    keycloakService.updateToken();
    keycloakService.token = keycloakService.getCurrentTOken();
    localStorage.setItem('keycloak_token', keycloakService.token);
    console.log("keycloak token");
    console.log(keycloakService.keycloak);
    keycloakService.keycloak.loadUserProfile();
    keycloakService.parsedToken = keycloakService.keycloak.tokenParsed;
    keycloakService.keycloak.loadUserInfo().success(function (userInfo) {
        keycloakService.userInfo = userInfo;
        console.log(keycloakService);
    }).error(function (e) {
        console.log(e);
    });

    keycloakService.keycloak.loadUserProfile().success(function (userProfile) {
        keycloakService.userProfile = userProfile;
        console.log(keycloakService);
        var varUserName = keycloakService.userProfile.firstName +" "+ keycloakService.userProfile.lastName;
        var varCompanyId = keycloakService.userProfile.companyId +" "+ keycloakService.userProfile.companyId;
        var varLoginName = keycloakService.userProfile.username;
        $("#headerUsername").html(varUserName);
        localStorage.setItem("userName", varUserName);
        localStorage.setItem("strLoginName", varLoginName);
        localStorage.setItem("userProfile", userProfile);
    }).error(function (e) {
        console.log(e)
    });
}

keycloakService.onFail = function (e) {
    console.log(e);
}

keycloakService.getCurrentTOken = function () {
    return keycloakService.keycloak.token;
}

keycloakService.logout = function () {
	console.log('hit');
	localStorage.removeItem("keycloak_token");
	keycloakService.keycloak.logout(keycloakConfig.url+'/realms/'+keycloakConfig.realm+'/protocol/openid-connect/logout?redirect_uri='+urlIndex)
}

console.log(keycloakService.keycloak);

keycloakService.keycloak.init({ onLoad: 'login-required' }).success(function() {
    keycloakService.onAuthentication();
    //app initialize 
}).error(keycloakService.onFail);

keycloakService.attachToken = function () {
	keycloakService.updateToken();
    var  localToken =  localStorage.getItem('keycloak_token');
    var bearerToken = null;    
     if(localToken != undefined)
            bearerToken =   {headers:{'Authorization': 'Bearer ' + localStorage.getItem('keycloak_token')}};
    return bearerToken;
}

