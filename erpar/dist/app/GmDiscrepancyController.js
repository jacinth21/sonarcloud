/* 
 * GmDiscrepancyController.js : Controller Class used for the A/R Orders Discrepancy PO Report functionalities
 * @author T.S Ramachandiran
 */
var gridObj;
var grid;
var gridLineItems;
var POArr = new Array(1);
var POArrStatus = new Array(1);
var errMsg = "Something went wrong please try again later";
app.controller('GmDiscrepancyController', function ($scope, $http, $q, $timeout, $filter, $rootScope,ParentScopes) {
$scope.arrDescList =[];
$scope.arrResStauscList =[];


    var AfterAuthentication = function () {
        var deferred = $q.defer();
        while (!KeycloakService.keycloak.authenticated) {}
        deferred.resolve(KeycloakService.keycloak.authenticated);
        return deferred.promise;
    }


    $(document).ready(function () {

        $("#po-entry-main").hide();

        $("#preloader-spin").hide();
        
        $('.dhx_sample-controls').hide();


        $http.get(varJsonURL).then(function (response) {}, function (response) {
            $scope.errorMsg = response.data.message;
            sessionStorage.setItem("errorMsg", ($scope.errorMsg));
            $scope.errorHandler();
        });

        var strPartyId = "";
        var strUserId = "";
        var strCompId = "";
        $scope.AccessFl = 'N';
        /*This variable for set right panel height*/
        var poentry_height = '';
        var search_fil_height = '';
        var grid_height = '';

        strPartyId = localStorage.getItem('strPartyId');
        strCompId = localStorage.getItem('strCompId');
        strUserId = localStorage.getItem('strUserId');

        /*Web Service Call for party details from user name*/
        fnLoadCompany($http, strPartyId, function (response) {
            if (response.data != undefined) {
                $scope.compList = response.data;
                /*Web Service Call for company information from company id*/
                fnLoadCompanyInfo($http, strCompId, function (response) {
                    if (response.data != undefined) {
                        $scope.selectedCompany = strCompId;
                        $scope.compInfoList = response.data;
                        $scope.strCurrency = $scope.compInfoList[0].currency;
                        $scope.strcurrencyNm = $scope.compInfoList[0].currencyNm;
                        $scope.strDtFormat = $scope.compInfoList[0].dtFormat;
                        $scope.strCmpCurrFmt = $scope.compInfoList[0].currencyFormat;
                        $scope.strCompanyId = $scope.compInfoList[0].companyId;
                        $scope.strCompanyZone = $scope.compInfoList[0].compZone;
                        $scope.strLanguageId = $scope.compInfoList[0].languageId;
                        $scope.strCompanyLocale = $scope.compInfoList[0].companyLocale;
                        $scope.fnSetDates($scope.strDtFormat);
                    }
                });
            } else {
                $scope.errorMsg = response.data.message;
                sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                $scope.errorHandler();
            }
        });

        /*This function is used to get the update access flag for based on PartyId*/
        fnSecurityEvent($http, strPartyId, 'ORD_PO_DISC_ACC', function (secresponse) {
            if (secresponse.data != undefined && secresponse.data != '') {
                if (secresponse.data[0].updateAccessFl != undefined) {
                    $scope.AccessFl = secresponse.data[0].updateAccessFl;
                }
            }
        });
        
        /* This function used to get the dropdown of AD */
        fnFetchuserInfoValues($http, $scope,'8000',strCompId, function (response){
        	$scope.userADList = response.data;
        });
        
        /* This function used to get the dropdown of VP */
        fnFetchuserInfoValues($http, $scope,'8001',strCompId, function (response){
        	$scope.userVPList = response.data;
        });


        /*Web Service Call for Load Company details  based on login user*/
        fnLoadDivision($http, function (response) {
            if (response.data.status == undefined) {
                $scope.divisionList = response.data;
            } else {
                $scope.errorMsg = response.data.message;
                sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                $scope.errorHandler();
            }
        });


    });
    /* This function used to get the dropdown values for PO*/

    fnCodeLookUpValues($http, $scope, 'REVST', function (podropres) {
        $scope.poDtlList = podropres.data;
    });
    
    fnCodeLookUpValues($http, $scope, 'DISTYP', function (descType) {
        $scope.descTypeVal(descType.data);
        $scope.arrDescList.push('Choose one');
            angular.forEach(descType.data, function(value, key) {
	        $scope.arrDescList.push(value.codeNm);
           }); 
        $scope.descTypeDtlList = descType.data;
    });
    
    fnCodeLookUpValues($http, $scope, 'DISSTS', function (descStatus) {
        $scope.descStatus(descStatus.data);
        $scope.arrResStauscList.push('Choose one');
            angular.forEach(descStatus.data, function(value, key) {
	        $scope.arrResStauscList.push(value.codeNm);
           }); 
        $scope.descStatusDtlList = descStatus.data;
    });
    
    $scope.descTypeVal = function (desType) {
    	 for (var i = 0; i < desType.length; i++) {
    		 POArr[i] = new Array(desType[i].codeId,desType[i].codeNm);
    	 }
    };
    
    $scope.descStatus = function (desStatus) {
   	 for (var i = 0; i < desStatus.length; i++) {
   		 POArrStatus[i] = new Array(desStatus[i].codeId,desStatus[i].codeNm);
   	 }
   };
    
    /* This function used to get the dropdown values for DO*/

    fnCodeLookUpValues($http, $scope, 'RSTDO', function (dodropres) {
        $scope.doDtlList = dodropres.data;
    });
    /* This function used to get the dropdown values for Collector*/
    fnCodeLookUpValues($http, $scope, 'COLLTR', function (podropres) {
        $scope.collectorList = podropres.data;
    });
    /* This function used to get the dropdown values for Resolution*/
    fnCodeLookUpValues($http, $scope, 'DOPORE', function (podropres) {
        $scope.DOPORE = podropres.data;
    });

    /* This function used to get the dropdown values for Category*/
    fnCodeLookUpValues($http, $scope, 'DOPOCA', function (podropres) {
        $scope.DOPOCA = podropres.data;
    });
    
    /* Redis
     * Web Service Call for Parent Account details  based on selected Company in company dropdown*/
    $scope.fnSearchParentAccount = function () {
        var strSearchValue = $scope.strParentAccId.toLowerCase();
        if (strSearchValue.length >= 3) {
            $scope.hidethisacc = false;
            var strScreenRedisKey = 'parentAccountRedis';
            $http.get(microappUrl + 'loadRedisDtls' + '/' + strScreenRedisKey + '/' + strSearchValue + '/' + '100' + '/' + $scope.strCompanyId).then(function (response) {
                if (response.data != "") {
                    $scope.parentAccList = response.data;
                    $("#list-group-acc").addClass("searchResults");
                } else {
                    $scope.parentAccList = [{
                        "ID": "",
                        "NAME": "No Data Found"
                    }];
                }
            });
        } else {
            $scope.hidethisacc = true;
            $("#list-group-acc").removeClass("searchResults");
        }
    };
    /* Redis
     * Web Service Call for Rep Account details  based on selected Company in company dropdown*/
    $scope.fnSearchRepAccount = function () {
        var strSearchValue = $scope.strRepAccId.toLowerCase();
        if (strSearchValue.length >= 3) {
            $scope.hidethisrep = false;
            var strScreenRedisKey = 'repAccountRedis';
            $http.get(microappUrl + 'loadRedisDtls' + '/' + strScreenRedisKey + '/' + strSearchValue + '/' + '100' + '/' + $scope.strCompanyId).then(function (response) {
                if (response.data != "") {
                    $scope.repAccList = response.data;
                    $("#list-group-rep").addClass("searchResults");
                } else {
                    $scope.repAccList = [{
                        "ID": "",
                        "NAME": "No Data Found"
                    }];
                }
            });

        } else {
            $scope.hidethisrep = true;
            $("#list-group-rep").removeClass("searchResults");
        }
    };
    /* Redis
     * Web Service Call for Field Sales details  based on selected Company in company dropdown*/
    $scope.fnSearchFieldSales = function () {
        var strSearchValue = $scope.strFieldSalesId.toLowerCase();
        if (strSearchValue.length >= 3) {
            $scope.hidethisfield = false;
            var strScreenRedisKey = 'distRedis';
            $http.get(microappUrl + 'loadRedisDtls' + '/' + strScreenRedisKey + '/' + strSearchValue + '/' + '100' + '/' + $scope.strCompanyId).then(function (response) {
                if (response.data != "") {
                    $scope.fieldSalesList = response.data;
                    $("#list-group-field").addClass("searchResults");
                } else {
                    $scope.fieldSalesList = [{
                        "ID": "",
                        "NAME": "No Data Found"
                    }];
                }
            });

        } else {
            $scope.hidethisfield = true;
            $("#list-group-field").removeClass("searchResults");
        }
    };
    /* Redis
     * Web Service Call for Sales Rep details  based on selected Company in company dropdown*/
    $scope.fnSearchSalesRep = function () {
        var strSearchValue = $scope.strSalesRepId.toLowerCase();
        if (strSearchValue.length >= 3) {
            $scope.hidethissales = false;
            var strScreenRedisKey = 'salesRepRedis';
            $http.get(microappUrl + 'loadRedisDtls' + '/' + strScreenRedisKey + '/' + strSearchValue + '/' + '100' + '/' + $scope.strCompanyId).then(function (response) {
                if (response.data != "") {
                    $scope.salesRepList = response.data;
                    $("#list-group-sales").addClass("searchResults");
                } else {
                    $scope.salesRepList = [{
                        "ID": "",
                        "NAME": "No Data Found"
                    }];
                }
            });

        } else {
            $scope.hidethissales = true;
            $("#list-group-sales").removeClass("searchResults");
        }
    };
    
    //Set id for Parent account search auto search
    $scope.fnParentAccItem = function ($event) {
        $scope.strParentAcc = angular.element($event.target).attr("data-id");
        $scope.strParentAccId = angular.element($event.target).attr("data-name");
        $scope.hidethisacc = true;

    };

    //Set id for Rep account search auto search
    $scope.fnRepAccItem = function ($event) {
        $scope.strRepAcc = angular.element($event.target).attr("data-id");
        $scope.strRepAccId = angular.element($event.target).attr("data-name");
        $scope.hidethisrep = true;

    };

    //Set id for Field sales search auto search
    $scope.fnFieldSalesItem = function ($event) {
        $scope.strFieldSales = angular.element($event.target).attr("data-id");
        $scope.strFieldSalesId = angular.element($event.target).attr("data-name");
        $scope.hidethisfield = true;

    };

    //Set id for sales Rep search auto search
    $scope.fnSalesRepItem = function ($event) {
        $scope.strSalesRep = angular.element($event.target).attr("data-id");
        $scope.strSalesRepId = angular.element($event.target).attr("data-name");
        $scope.hidethissales = true;
    };
    
    /*This is used for set the date picker details */
    $scope.fnSetDates = function (strDateFormat) {
        var strDtFormat = (strDateFormat).toLowerCase();

        var today = new Date();


        $(".inputDatePicker").datepicker('destroy');
        $('.inputDatePicker').attr("placeholder", strDtFormat);
        $('.inputDatePicker').datepicker({
            format: strDtFormat,
            autoclose: true,
        });


        $("#discrepancyFDate").datepicker();
        $("#discrepancyTDate").datepicker();
        $("#resolutionFDate").datepicker();
        $("#resolutionTDate").datepicker();
        $("#orderFDate").datepicker();
        $("#orderTDate").datepicker();
        $("#remindMeDate").datepicker();

        /*Calendar icon click focus input fields*/
        $('.discrepancyFDate').click(function () {
            $("#discrepancyFDate").focus();
            $scope.fnValidateFDisDate();
        });

        $('.discrepancyTDate').click(function () {
            $("#discrepancyTDate").focus();
            $scope.fnValidateTDisDate();
        });

        $('.resolutionFDate').click(function () {
            $("#resolutionFDate").focus();
            $scope.fnValidateFResDate();
        });

        $('.resolutionTDate').click(function () {
            $("#resolutionTDate").focus();
            $scope.fnValidateTResDate();
        });
        $('.orderFDate').click(function () {
            $("#orderFDate").focus();
            $scope.fnValidateFOrdDate();
        });

        $('.orderTDate').click(function () {
            $("#orderTDate").focus();
            $scope.fnValidateTOrdDate();
        });
        $('.remindMeDate').click(function () {
            $("#remindMeDate").focus();
        });

    };

    //Clear redis dropdown id 
    $scope.fnClearRedisId = function () {
        var strParentAccNm = $scope.strParentAccId;
        var strRepAccNm = $scope.strRepAccId;
        var strFieldSalesNm = $scope.strFieldSalesId;
        var strSalesRepNm = $scope.strSalesRepId;

        if (strParentAccNm == '') {
            $scope.strParentAcc = "";
        }
        if (strRepAccNm == '') {
            $scope.strRepAcc = "";
        }
        if (strFieldSalesNm == '') {
            $scope.strFieldSales = "";
        }
        if (strSalesRepNm == '') {
            $scope.strSalesRep = "";
        }

    };


    //Function is used to validate Discrepency From date 
    $scope.fnValidateFDisDate = function () {
        $("#span-empty_disFdt-err").hide();
    };
  //Function is used to validate Discrepency To date 
    $scope.fnValidateTDisDate = function () {
        $("#span-empty_disTdt-err").hide();
        $("#span-dt_compare_disFdt-err").hide();
    };
  //Function is used to validate Resolution From date 
    $scope.fnValidateFResDate = function () {
        $("#span-empty_resFdt-err").hide();
    };
  //Function is used to validate resolution To date 
    $scope.fnValidateTResDate = function () {
        $("#span-empty_resTdt-err").hide();
        $("#span-dt_compare_resFdt-err").hide();
    };
  //Function is used to validate Order From date 
    $scope.fnValidateFOrdDate = function () {
    	$("#span-empty_ordFdt-err").hide();
    };
  //Function is used to validate Order To date 
    $scope.fnValidateTOrdDate = function () {
		$("#span-empty_ordTdt-err").hide();
		$("#span-dt_compare_ordFdt-err").hide();
    };

    /*Function is used to load Discrepency report*/
    $scope.fnLoadDiscrepancyPOReport = function () {
        var forms = document.getElementsByClassName('needs-validation');
        $("#discrepancy-main").removeClass("col-9").addClass("col-12");
        //Right Panel ID
        $("#po-entry-main").addClass("col-3").hide();

        // Get all form-groups in need of validation
        var validateGroup = document.getElementsByClassName('validate-me');

        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {

                if (form.checkValidity() != false) {
                    $scope.fnClearRedisId();
                    $scope.fnLoadDisPOReport();
                } else {
                    event.preventDefault();
                    event.stopPropagation();
                }

                //Added validation class to all form-groups in need of validation
                for (var i = 0; i < validateGroup.length; i++) {
                    validateGroup[i].classList.add('was-validated');
                }

            }, false);
        });
    };

    //clear the all field
    $scope.fnClearField = function () {
    	
    	$scope.strDivisionId = "";
        $scope.strParentAccId = "";
        $scope.strFieldSalesId = "";
        $scope.strSalesRepId = "";
        $scope.strDisStartDate = "";
        $scope.strDisEndDate = "";
        $scope.strResStartDate = "";
        $scope.strResEndDate = "";
        $scope.strCustPONumber = "";
        $scope.strOrdId = "";
        $scope.strAccid = "";
        $scope.strRepAccId = "";
        $scope.strRepAcc = "";
        $scope.strFieldSales = "";
        $scope.strSalesRep = "";
        $scope.strParentAcc = "";
        $scope.strCollectorId = "";
        $scope.strResolutionId = "";
        $scope.strCategoryId = "";
        $scope.strOrderFDate="";
        $scope.strOrderTDate="";
        $scope.strADId= "";
        $scope.strVPId= "";
        $("#span-empty_disFdt-err").hide();
        $("#span-empty_disTdt-err").hide();
        $("#span-empty_resFdt-err").hide();
        $("#span-empty_resTdt-err").hide();
        $("#span-empty_ordFdt-err").hide();
        $("#span-empty_ordTdt-err").hide();
        $("#span-dt_compare_disFdt-err").hide();
        $("#span-dt_compare_resFdt-err").hide();
        $("#span-dt_compare_ordFdt-err").hide();
        $scope.hidethisacc = true;
        $("#list-group-acc").removeClass("searchResults");
        $scope.hidethisrep = true;
        $("#list-group-rep").removeClass("searchResults");
        $scope.hidethisfield = true;
        $("#list-group-field").removeClass("searchResults");
        $scope.hidethissales = true;
        $("#list-group-sales").removeClass("searchResults");
        $scope.hidethisad = true;
        $("#list-group-ad").removeClass("searchResults");
        $scope.hidethisvp = true;
        $("#list-group-vp").removeClass("searchResults");
    };

    //clear the date filter field while sort with DO id
    $scope.fnClearDateField = function (strOrdId) {
        if (strOrdId != "") {
            $scope.strResStartDate = "";
            $scope.strResEndDate = "";
        }
    };


    /*Function is used to load the Discrepency PO report*/
    $scope.fnLoadDisPOReport = function () {

        var disFromDate = "";
        var disToDate = "";
        var resFromDate = "";
        var resToDate = "";
        var ordFromDate = "";
        var ordToDate = "";
        var ResolutionValidateFl = false;
        var DiscrepancyValidateFl = false;
        var OrderValidateFl = false;
        var ValidateMsg = "";
        var DisDtErrFl = false;
        var ResDtErrFl = false;
        var OrdDtErrFl = false;

        var DiscrepancyFromDtErrFl = false;
        var DiscrepancyToDtErrFl = false;
        var ResolutionFromDtErrFl = false;
        var ResolutionToDtErrFl = false;
        var varOrderFromDtErrFl = false;
        var varOrderToDtErrFl = false;

        disFromDate = $scope.strDisStartDate;
        disToDate = $scope.strDisEndDate;
        resFromDate = $scope.strResStartDate;
        resToDate = $scope.strResEndDate;
        ordFromDate = $scope.strOrderFDate;
        ordToDate = $scope.strOrderTDate;
        $("#span-empty_disFdt-err").hide();
        $("#span-empty_disTdt-err").hide();
        $("#span-empty_resFdt-err").hide();
        $("#span-empty_resTdt-err").hide();
        $("#span-empty_ordFdt-err").hide();
        $("#span-empty_ordTdt-err").hide();
        $("#span-dt_compare_disFdt-err").hide();
        $("#span-dt_compare_resFdt-err").hide();
        $("#span-dt_compare_ordFdt-err").hide();



        //validate From Date of Discrepancy Field and assign result to Flag
        if (!parseNull(disFromDate) ) {

            DiscrepancyFromDtErrFl = true;
        }
        //validate To Date of Discrepancy Field and assign result to Flag
        if (!parseNull(disToDate)) {

            DiscrepancyToDtErrFl = true;
        }
        //validate From Date of Resolution Field and assign result to Flag
        if (!parseNull(resFromDate)) {
            ResolutionFromDtErrFl = true;
        }
        //validate To Date of Resolution Field and assign result to Flag
        if (!parseNull(resToDate)) {
            ResolutionToDtErrFl = true;
        }
      //validate From Date of Order Field and assign result to Flag
        if (!parseNull(ordFromDate)) {
            varOrderFromDtErrFl = true;
        }
        //validate To Date of Order Field and assign result to Flag
        if (!parseNull(ordToDate)) {
            varOrderToDtErrFl = true;
        }

        //Check each date field and get validation message for missing input field
        if ((DiscrepancyFromDtErrFl) && (!DiscrepancyToDtErrFl)) {
            $("#span-empty_disFdt-err").show();
            return false;
        }
        if ((!DiscrepancyFromDtErrFl) && (DiscrepancyToDtErrFl)) {
            $("#span-empty_disTdt-err").show();
            return false;
        }
        if ((ResolutionFromDtErrFl) && (!ResolutionToDtErrFl)) {
            $("#span-empty_resFdt-err").show();
            return false;
        }
        if ((!ResolutionFromDtErrFl) && (ResolutionToDtErrFl)) {
            $("#span-empty_resTdt-err").show();
            return false;
        }
        if ((varOrderFromDtErrFl) && (!varOrderToDtErrFl)) {
            $("#span-empty_ordFdt-err").show();
            return false;
        }
        if ((!varOrderFromDtErrFl) && (varOrderToDtErrFl)) {
            $("#span-empty_ordTdt-err").show();
            return false;
        }


        //validate Discrepancy Date and assign result to Flag
        if (parseNull(disFromDate) && parseNull(disToDate)) {
            DiscrepancyValidateFl = true;
        }

        //validate Resolution Date and assign result to Flag
        if (parseNull(resFromDate) && parseNull(resToDate)) {
            ResolutionValidateFl = true;
        }
        //validate Order Date and assign result to Flag
        if (parseNull(ordFromDate) && parseNull(ordToDate)) {
            OrderValidateFl = true;
        }

        //This condition will execute if Validation Discrepancy flag is true
        if (DiscrepancyValidateFl) {
            //dateDiff method is used for identify the date difference for all format 
            dateDiff(disFromDate, disToDate, $scope.strDtFormat, function (response) {
                if (response != '') {
                    if (response < 0) {
                        DisDtErrFl = true;
                        $("#span-dt_compare_disFdt-err").show();
                        return false;
                    }
                }
            });

        }

        //This condition will execute if Validation Entry flag is true
        if (ResolutionValidateFl) {

            //dateDiff method is used for identify the date difference for all format 
            dateDiff(resFromDate, resToDate, $scope.strDtFormat, function (response) {
                if (response != '') {
                    if (response < 0) {
                        ResDtErrFl = true;
                        $("#span-dt_compare_resFdt-err").show();
                        return false;
                    }
                }
            });


        }
        //This condition will execute if Validation Entry flag is true
        if (OrderValidateFl) {
      	
        	//dateDiff method is used for identify the date difference for all format 
            dateDiff(ordFromDate, ordToDate, $scope.strDtFormat,function (response) {
                if (response != '') {
	                  if (response < 0) {
	                	  OrdDtErrFl = true;
	                	  $("#span-dt_compare_ordFdt-err").show();
	                	  return false;
	                  }
                } 
            });
            

        }


        //Load the Discrepency PO reports
        if ((DisDtErrFl == false) && (ResDtErrFl == false) && (OrdDtErrFl == false)) {
            $("#discrepency-dashboard-report").html("<div class='w-100 text-center' id='preloader-spin-report'><i class='fas fa-spinner fa-pulse fa-2x'></i></div>");
            $('#discrepency-dashboard-report').show();
            $('#no-data-found').hide();

            /*toggle more button should collapse while click on load button*/
            if ($('#collapseFilter').hasClass('collapse show')) {
                $('#collapseFilter').removeClass('show');
            }

            if ($('#btnCollapseFilter').hasClass('btn btn-xs btn-outline-dark')) {
                $('#btnCollapseFilter').addClass('btn btn-xs btn-outline-dark collapsed');
            }
            var strPONumber = $scope.strCustPONumber;
            var strOrdId = $scope.strOrdId;
            if (strPONumber != "" && strPONumber != undefined) {
                var lastChar = strPONumber.slice(-1);
                if (lastChar == ',') {
                    $scope.strCustPONumber = strPONumber.slice(0, -1);
                }
            }
    	if (strOrdId != "" && strOrdId != undefined) {
                var lastChar = strOrdId.slice(-1);
                if (lastChar == ',') {
                    $scope.strOrdId = strOrdId.slice(0, -1);
                }
            }

            if($scope.strReadyForResId == true)
            {
                $scope.strReadyForResVal = 'Y';
            }
            else{
                 $scope.strReadyForResVal = 'N';
             }          

            if($scope.strHFutureRemId == true)
            {
                $scope.strHFutureRemVal = 'Y';
            }
         else{
                 $scope.strHFutureRemVal = 'N';
             }

            $scope.gmARDiscrepencyModel = {
                strCompanyId: $scope.strCompanyId,
                strParentAcc: $scope.strParentAcc,
                strRepAcc: $scope.strRepAcc,
                strFieldSales: $scope.strFieldSales,
                strSalesRep: $scope.strSalesRep,
                strDivisionId: $scope.strDivisionId,
                strCustPONumber: $scope.strCustPONumber,
                strDateType: $scope.strDateType,
                strDisStartDate: $scope.strDisStartDate,
                strDisEndDate: $scope.strDisEndDate,
                strOrderId: $scope.strOrdId,
                strResStartDate: $scope.strResStartDate,
                strResEndDate: $scope.strResEndDate,
                strAccid: $scope.strAccid,
                strCurrency: $scope.strCurrency,
                strCmpCurrFmt: $scope.strCmpCurrFmt,
                strDtFormat: $scope.strDtFormat,
                strLanguageId: $scope.strLanguageId,
                strCollectorId: $scope.strCollectorId,
                strCategoryId: $scope.strCategoryId,
                strPOStatusList: '109544', //status id for move to descrepency
                strResolutionId: $scope.strResolutionId,
                strOrdStartDate: $scope.strOrderFDate,
	            strOrdEndDate: $scope.strOrderTDate,
	            strADId : $scope.strADId,
	            strVPId: $scope.strVPId,
		        strReadyForResId : $scope.strReadyForResVal,
                strHFutureRemId : $scope.strHFutureRemVal,
                strCompZone : $scope.strCompanyZone
            };
            
            $http.post(url + 'loadDiscrepancyDetails/', angular.toJson($scope.gmARDiscrepencyModel)).then(function (response) {

                if (response.data != undefined) {
                    $scope.results = response.data;
                    $scope.fnLoadDiscrepncyDetailGrid($scope.results);
                } else {
                    $scope.errorMsg = response.data.message;
                    sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                    $scope.errorHandler();
                }

            });
        }
    };
    // this is to load the Discrepency Details grid
    $scope.fnLoadDiscrepncyDetailGrid = function (response) {

        $('#discrepency-dashboard-report').empty();
        $('#preloader-spin-report').hide();
        if (response != '') {
            // Updating new dhtmlx Grid - 6.5.2
            datacollection = new dhx.DataCollection();
    
            datacollection.parse(response);
           
            grid = new dhx.Grid("discrepency-dashboard-report", {
                columns: [{width: 150,id: "strCustPONumber",header: [{text: "PO Number",align: "center" }, {
                                content: "inputFilter"}],align: "left",footer: [{text: '<div class="custom_footer">Total</div>'}]},
                    {width: 140,id: "strPOAmt",align: "right",header: [{text: "PO Amount",align: "center"}, {
                            content: "inputFilter" }],type: "number",template: function (text, row, col) {
                            if (row.strPOAmt == 0) {
                                return "0.00"
                            } else if (row.strPOAmt < 0) {
                                row.strPOAmt =  row.strPOAmt.slice(1);
                                if (row.strPOAmt % 1 != 0) {
                                    row.strPOAmt = parseFloat(row.strPOAmt + '0');
                                    return (row.strPOAmt).toFixed(2);
                                } else {
                                    return row.strPOAmt + ".00";
                                }
                            } else {
                                if (row.strPOAmt % 1 != 0) {
                                    row.strPOAmt = parseFloat(row.strPOAmt + '0');
                                    return (row.strPOAmt).toFixed(2);
                                } else {
                                    return row.strPOAmt + ".00";
                                }
                            }},mark: function (cell, data, row, col) {//used to add class for cell(Minimum Amount Css)
                            if (row.strPOAmt < 0)
                                return "amt-min";
                            },footer: [{content: "sum"}]},
                    {width: 140,id: "strTotal",align: "right",header: [{text: "Order Amount",align: "center" }, {
                            content: "inputFilter"}],type: "number",template: function (text, row, col) {
                            if (row.strTotal == 0) {
                                return "0.00"
                            } else if (row.strTotal < 0) {
                                row.strTotal =  row.strTotal.slice(1);
                                if (row.strTotal % 1 != 0) {
                                    row.strTotal = parseFloat(row.strTotal + '0');
                                    return (row.strTotal).toFixed(2);
                                } else {
                                    return row.strTotal + ".00";
                                }
                            } else {
                                if (row.strTotal % 1 != 0) {
                                    row.strTotal = parseFloat(row.strTotal + '0');
                                    return (row.strTotal).toFixed(2);
                                } else {
                                    return row.strTotal + ".00";
                                }
                            }},mark: function (cell, data, row, col) { // add class for cell (Minimum Amount Css)
                            if (row.strTotal < 0)
                                return "amt-min";
                        },footer: [{content: "sum"}]},
                    { width: 140,id: "strDiffAmt",align: "right",header: [{text: "Diff",align: "center"}, {
                            content: "inputFilter"}], type: "number", template: function (text, row, col) {
                            if (row.strDiffAmt == 0) {
                                return "0.00"
                            } else if (row.strDiffAmt < 0) {
                                row.strDiffAmt =  row.strDiffAmt.slice(1);
                                if (row.strDiffAmt % 1 != 0) {
                                    row.strDiffAmt = parseFloat(row.strDiffAmt + '0');
                                    return (row.strDiffAmt).toFixed(2);
                                } else {
                                    return row.strDiffAmt + ".00";
                                }
                            } else {
                                if (row.strDiffAmt % 1 != 0) {
                                    row.strDiffAmt = parseFloat(row.strDiffAmt + '0');
                                    return (row.strDiffAmt).toFixed(2);
                                } else {
                                    return row.strDiffAmt + ".00";
                                }
                            }}, mark: function (cell, data, row, col) {//add class for cell (Minimum Amount Css)
                            if (row.strDiffAmt < 0)
                                return "amt-min";
                        }},
                    {width: 150,id: "strParentOrdId",header: [{text: "Parent Order Id",align: "center"}, {
                            content: "inputFilter"}],align: "center"},
                    {width: 150,id: "strOrderId",header: [{text: "DO ID",align: "center"}, {content: "inputFilter"
                        }],htmlEnable: true,template: function (text, row, col) {
                            return "<a id='strOrdId' data-orderid='" + text + "' href='javascript:;'><span class='po-update-btn'>" + text + "</span></a></div>";
                        },align: "left"},
                    {width: 150,id: "strCategoryNm",header: [{text: "Category",align: "center"}, {content: "selectFilter"}],align: "left"},
                    {width: 50,id: "strPOResStatus",header: [],htmlEnable: true,
                        template: function (text, row, col) {
                            if (row.strPOResolvefl == 'Y') {
                                return "<span id='gridholdicon' class='mdi mdi-checkbox-marked-circle' style='cursor:pointer;color:#008000;font-size:15px;' title='Resolved'></span>";
                            } else if (row.strPODiscRemdate >= 0 && row.strPODiscRemdate != "") {
                                return "<i class='material-icons' style='cursor:pointer;font-size:16px;color:#FF4500' title='add_alert'>add_alert</i>";
                            }
                        },align: "center"},
                    {width: 150,id: "strAccid",header: [{text: "Rep Account Id",align: "center"}, {content: "inputFilter"
                        }], align: "right"},
                    {width: 250,id: "strRepAcc",header: [{text: "Rep Account",align: "center"}, {
                            content: "inputFilter"}],align: "left"},
                    {width: 250,id: "strSalesRep",header: [{text: "Sales Rep",align: "center"}, {
                            content: "inputFilter"}],align: "left" },
                    {width: 250,id: "strParentAcc",header: [{text: "Parent Account",align: "center" }, {
                            content: "inputFilter" }],align: "left"},
                    {width: 150,id: "strResDate",header: [{text: "Resolution Date",align: "center"}, {
                            content: "comboFilter" }],align: "left"},
                    {width: 150,id: "strDisdate",header: [{text: "Discrepancy Date",align: "center"}, {
                            content: "comboFilter"}], align: "left"},
                    {width: 150,id: "strCollectorNm",header: [{text: "Collector Name",align: "center"}, {
                            content: "inputFilter"}],align: "left"},
                    {width: 150,id: "strOrdDT",header: [{text: "Order Date", align: "center"}, {
                            content: "comboFilter" }],align: "left"},
                    {width: 150,id: "strADName",header: [{text: "AD Name",align: "center"}, {
                            content: "inputFilter"}],align: "left"},
                    {width: 150,id: "strVPName",header: [{text: "VP Name",align: "center"}, {
                            content: "inputFilter"}],align: "left"},
                    {width: 250,id: "strPOEnteredDate",header: [{text: "PO Entered Date",align: "center"}, {
                            content: "comboFilter"}],align: "left"},
                    {width: 250,id: "strCustPOUpdatedBy",header: [{text: "PO Updated By",align: "center"}, {
                            content: "inputFilter"}],align: "left"}],
                data: response,
                resizable: true,
                height: 380,
                dragItem: "column",
                keyNavigation: true,
                rowHeight: 30
            });
            $scope.grid = grid;
            $('.dhx_sample-controls').show();

            /*This function used for handling  events*/
            const events = [
  "afterEditEnd",
   "headerCellClick",
   "cellClick",
   "beforeEditEnd",
   "beforeEditStart"
];
            /*This function for grid event handler*/
            function eventHandler(event, arguments) {

                // Cell click event  to open DO Details
                if (event == 'cellClick') {
                    if (arguments[1].id == "strOrderId") {// while click order id column values open DO details
                        var doOrderId = arguments[0].strParentOrdId;
                        if (parseNull(doOrderId) == "") {
                            doOrderId = arguments[0].strOrderId;
                        }
                        $scope.fnSummaryDO(doOrderId);
                    } else { //used to open right panel
                        $scope.strPONumDtlsId = arguments[0].strPONumDtlsId;
                        $scope.strDefaultCollector = arguments[0].strDefaultCollector;
                        $scope.strOrdMode = arguments[0].strOrdReceiveMode;
                        $scope.strAccount = arguments[0].strAccid;
                        $scope.strCustPONum = arguments[0].strCustPONumber;
                        $scope.strCustomerPo = arguments[0].strCustPONumber;
                        $scope.strOrderId = arguments[0].strOrderId;
                        $scope.strParentAccNm = arguments[0].strParentAcc;
                        $scope.strPOAmt = arguments[0].strPOAmt;
                        $scope.poCheckFlag = '';
                        $scope.successMsg = '';
                        $scope.validatePONumMsg = '';
                        $scope.validateCommentsMsg = '';
                        $scope.validatePOAmtMsg = '';
                        $scope.commentsCount = 0;
                        poentry_height = $('#discrepancy-main').height() + 15;
                        $('#po-entry-main').height(poentry_height);
                        $scope.fnLoadPOIdList();
                        $scope.fnLoadPOEntry();
                        $scope.fnFetchPODetails();
                        $scope.fnFetchComments();
                        $scope.fnDownloadDOPdfURL();
                        $scope.fnLoadFiles();
                        $scope.fnLineItemLable();

                    }
                }

            }
            events.forEach(function (event) {
                grid.events.on(event, function () {
                    eventHandler(event, arguments);
                });
            });
            //Used to show/hide default column
            $('input[class="chk_column"]').each(function () {
                if ($(this).prop("checked") == true) {
                    grid.showColumn($(this).val());

                } else {
                    grid.hideColumn($(this).val());
                }
            });
              //Used to show/hide based on checkbox
            $('.chk_column').click(function(){
                if($(this).prop("checked") == true){
                   grid.showColumn($(this).val());

                }
                else{
                   grid.hideColumn($(this).val());
                }
            });
            
        } else {
            $('#no-data-found').show();
            $('.dhx_sample-controls').hide();
            $('#discrepency-dashboard-report').hide();
        }

    };
    // This function is used to download excel 
    $scope.exportXlsx = function () {
        grid.config.columns.splice(7, 1);
        grid.export.xlsx({
            name: "Discrepancy-report",
            url: "//export.dhtmlx.com/excel"
        });
        $scope.fnLoadDiscrepncyDetailGrid($scope.results);
    };
    //set lable Add details and view details
    $scope.fnLineItemLable = function(){
    	
    	$scope.gmARDiscrepancyModel={ 
    			strAccId: $scope.strAccount,
    			strPONumber : $scope.strCustPONum
    	};
	    $http.post(url + 'loadDiscrepancyLineItems/', angular.toJson($scope.gmARDiscrepancyModel)).then(function (response) {
	
	        if (response.data != undefined) {
	            var results = response.data;
	            var lineItemResult = JSON.stringify(results);
	            if((lineItemResult.indexOf("110180") == -1) && (lineItemResult.indexOf("110181") == -1)){
	            	$scope.strLineItemLabel = "Add Details";
	            }else{
	            	$scope.strLineItemLabel = "View Details";
	            }
	        } else {
	            $scope.errorMsg = response.data.message;
	            sessionStorage.setItem("errorMsg", ($scope.errorMsg));
	            $scope.errorHandler();
	        }
	    });
    };
    //this method not belongs to here - discrepency line item model
    $scope.fnPOLineItemModel = function () {
        $('#POLineItemModal').modal();
        $scope.POLineItemModal();
        
    };
    $scope.POLineItemModal = function(){
    	
    	$scope.gmARDiscrepancyModel={ 
    			strAccId: $scope.strAccount,
    			strPONumber : $scope.strCustPONum
    	};
    $http.post(url + 'loadDiscrepancyLineItems/', angular.toJson($scope.gmARDiscrepancyModel)).then(function (response) {

        if (response.data != undefined) {
            var results = response.data;
            $scope.fnLoadPODiscrepencyLineItems(results);
        } else {
            $scope.errorMsg = response.data.message;
            sessionStorage.setItem("errorMsg", ($scope.errorMsg));
            $scope.errorHandler();
        }

    });
    };
    // this is to load the Discrepency line item entry Details grid
    $scope.fnLoadPODiscrepencyLineItems = function(response){
        $('#poDiscrepencyLineItems').empty();
    datacollection = new dhx.DataCollection();
        
    
            datacollection.parse(response);
        
        angular.forEach(response, function (data) { 
            if(parseNull(data.strDescTypeId)){
                data.strDescTypeIdName = $scope.descTypeDtlList.find((a) => a.codeId == data.strDescTypeId).codeNm;
            }
            else{
               data.strDescTypeIdName = 'Choose one';
            }
            if(parseNull(data.strStatusId)){
              data.strStatusIdName = $scope.descStatusDtlList.find((a) => a.codeId == data.strStatusId).codeNm;
            }
            else{
               data.strStatusIdName = 'Choose one';
            }
                 });
            
            gridLineItems = new dhx.Grid("poDiscrepencyLineItems", {
                columns: [
                    {width: 150,id: "strPnum",header: [{text: "DO Part#",align: "center" }, {
                                content: "inputFilter"}],align: "left"},
                    {width: 150,id: "strPartDesc",header: [{text: "Part Description",align: "center" }, {
                                content: "inputFilter"}],align: "left",footer: [{text: '<div class="custom_footer">Total</div>'}]},
                    {width: 90,id: "strItemQty",header: [{text: "DO Qty",align: "center" }, {
                                content: "inputFilter"}],align: "right",footer: [{content: "sum"}]},
                    {width: 120,id: "strItemPrice",align: "right",header: [{text: "DO Amount",align: "center"}, {
                            content: "inputFilter" }],type: "number",template: function (text, row, col) {
                            if (row.strItemPrice == 0) {
                                return "0.00"
                            } else if (row.strItemPrice < 0) {
                                row.strItemPrice =  row.strItemPrice.slice(1);
                                if (row.strItemPrice % 1 != 0) {
                                    row.strItemPrice = parseFloat(row.strItemPrice + '0');
                                    return (row.strItemPrice).toFixed(2);
                                } else {
                                    return row.strItemPrice + ".00";
                                }
                            } else {
                                if (row.strItemPrice % 1 != 0) {
                                    row.strItemPrice = parseFloat(row.strItemPrice + '0');
                                    return (row.strItemPrice).toFixed(2);
                                } else {
                                    return row.strItemPrice + ".00";
                                }
                            }},mark: function (cell, data, row, col) {//used to add class for cell(Minimum Amount Css)
                            if (row.strItemPrice < 0)
                                return "amt-min";
                            },footer: [{content: "sum"}]},
                    {width: 150,id: "strExtDoAmt",align: "right",header: [{text: "Ext DO Amount",align: "center" }, {
                            content: "inputFilter"}],type: "number",template: function (text, row, col) {
                            if (row.strExtDoAmt == 0) {
                                return "0.00"
                            } else if (row.strExtDoAmt < 0) {
                                row.strExtDoAmt =  row.strExtDoAmt.slice(1);
                                if (row.strExtDoAmt % 1 != 0) {
                                    row.strExtDoAmt = parseFloat(row.strExtDoAmt + '0');
                                    return (row.strExtDoAmt).toFixed(2);
                                } else {
                                    return row.strExtDoAmt + ".00";
                                }
                            } else {
                                if (row.strExtDoAmt % 1 != 0) {
                                    row.strExtDoAmt = parseFloat(row.strExtDoAmt + '0');
                                    return (row.strExtDoAmt).toFixed(2);
                                } else {
                                    return row.strExtDoAmt + ".00";
                                }
                            }},mark: function (cell, data, row, col) { // add class for cell (Minimum Amount Css)
                            if (row.strExtDoAmt < 0)
                                return "amt-min";
                        },footer: [{content: "sum"}]},
                    {width: 150,id: "strPOPartNum",header: [{text: "PO Part#",align: "center" }, {
                                content: "inputFilter"}],align: "left",type: "string",editable:true,footer: [{text: '<div class="custom_footer">Total</div>'}]},
                    {width: 90,id: "strPOPartQty",header: [{text: "PO Qty",align: "center" }, {
                                content: "inputFilter"}],align: "right",editable:true,footer: [{content: "sum"}]},
                    {width: 120,id: "strPOPrice",align: "right",header: [{text: "PO Amount",align: "center"}, {
                            content: "inputFilter" }],type: "number",editable:true,template: function (text, row, col) {
                            if (row.strPOPrice == 0) {
                                return "0.00"
                            } else if (row.strPOPrice < 0) {
                                row.strPOPrice =  row.strPOPrice.slice(1);
                                if (row.strPOPrice % 1 != 0) {
                                    row.strPOPrice = parseFloat(row.strPOPrice + '0');
                                    return (row.strPOPrice).toFixed(2);
                                } else {
                                    return row.strPOPrice + ".00";
                                }
                            } else {
                                if (row.strPOPrice % 1 != 0) {
                                    row.strPOPrice = parseFloat(row.strPOPrice + '0');
                                    return (row.strPOPrice).toFixed(2);
                                } else {
                                    return row.strPOPrice + ".00";
                                }
                            }},mark: function (cell, data, row, col) {//used to add class for cell(Minimum Amount Css)
                            if (row.strPOPrice < 0)
                                return "amt-min";
                            },footer: [{content: "sum"}]},
                    { width: 150,id: "strExtPoAmt",align: "right",header: [{text: "Ext PO Amount",align: "center"}, {
                            content: "inputFilter"}], type: "number", template: function (text, row, col) {
                            if (row.strExtPoAmt == 0) {
                                return "0.00"
                            } else if (row.strExtPoAmt < 0) {
                                row.strDiffAmt =  row.strExtPoAmt.slice(1);
                                if (row.strExtPoAmt % 1 != 0) {
                                    row.strExtPoAmt = parseFloat(row.strExtPoAmt + '0');
                                    return (row.strExtPoAmt).toFixed(2);
                                } else {
                                    return row.strExtPoAmt + ".00";
                                }
                            } else {
                                if (row.strExtPoAmt % 1 != 0) {
                                    row.strExtPoAmt = parseFloat(row.strExtPoAmt + '0');
                                    return (row.strExtPoAmt).toFixed(2);
                                } else {
                                    return row.strExtPoAmt + ".00";
                                }
                            }}, mark: function (cell, data, row, col) {//add class for cell (Minimum Amount Css)
                            if (row.strExtPoAmt < 0)
                                return "amt-min";
                        },footer: [{content: "sum"}]},
                    {width: 155,id: "strDescTypeIdName",header: [{text: "Discrepancy Type",align: "center"}, {
                            content: "selectFilter"}],align: "left",type: "string",editable:true, editorType: "combobox",options: $scope.arrDescList,template: function (text, row, col) {
                                if(text == ''){
                                    return 'Choose one';
                                }
                                else{
                                  return text;
                                }
                            }
                            },
                    
                    {width: 136,id: "strStatusIdName",header: [{text: "Resolution Type",align: "center"}, {
                            content: "selectFilter"}],align: "left",type: "string",editable:true, editorType: "combobox",options: $scope.arrResStauscList,template: function (text, row, col) {
                                if(text == ''){
                                    return 'Choose one';
                                }
                                else{
                                  return text;
                                }
                            } }],
                data: response,
                dragItem: "column",
                margin: 'auto',
                keyNavigation: true,
                rowHeight: 30,
				autoEmptyRow: true,
                selection: "row",
                selection: "complex",
                resizable: true

            });
         var order_ids='';
        var drpDwn = [];
        datacollection.forEach(function (item, index, array) {
            ordId = item.strOrderid;
    		poDtlsId = item.strPODtlsId;
    		
    		if(order_ids.indexOf(ordId) == -1){
    			order_ids += ordId + ',';
    			drpDwn.push({
	                 "ID": ordId,
	                 "Name": ordId + ','
	             });
    		}
              });
             var strLastOrder = drpDwn.pop();
        strLastOrder = strLastOrder.Name;
        strLastOrder = strLastOrder.substring(0, strLastOrder.length - 1);
        drpDwn.push({
            "ID": ordId,
            "Name": strLastOrder
        });
       
      
         $scope.strOrderIdList = drpDwn;
        $scope.poDtlsId = poDtlsId;
            $scope.grid = gridLineItems;

        
        $('#POLineItemModal').modal();
    };
    // This function is used to download excel 
    $scope.exportLineItemXlsx = function () {
        gridLineItems.export.xlsx({
            name: "Discrepancy-LineItem-report",
            url: "//export.dhtmlx.com/excel"
        });
    };
    
    $scope.fnSubmit= function () {
    	var detailString = '';
    	var pnum_input_string = '';
    	var errMsg = '';
		var msg = '';
		var invalidPartMsg = '';
		var pnum_inputString = '';
        var doPnum = '';
         var doQty = '';
        var doPrice = '';      
        var poPnum = '';
        var poQty = '';
        var poPrice = '';
        var descType = '';
        var poStatus = '';
        var ordItemDtlsId = '';
        var poDtlsId = '';
         var charMsg = 0;
        var distypeMsg = 0;
        
                

		 gridLineItems.data.forEach(function (item, index, array) {
            $scope.errMsg = '';
    		$scope.msg = '';
    		$scope.invalidPartMsg = '';
    		
		    doPnum = item.strPnum;
    		doQty = item.strItemQty;
    		doPrice = item.strItemPrice;
		    poPnum = item.strPOPartNum;
    		poQty = item.strPOPartQty;
    		poPrice = item.strPOPrice;
            if(parseNull(item.strDescTypeIdName) && item.strDescTypeIdName != 'Choose one'){
               descType =  $scope.descTypeDtlList.find((a) => a.codeNm == item.strDescTypeIdName).codeId;
            }
             else{
                descType  = '';
             }
             if(parseNull(item.strStatusIdName) && item.strStatusIdName !='Choose one'){
               poStatus =  $scope.descStatusDtlList.find((a) => a.codeNm == item.strStatusIdName).codeId;
            }
             else{
                poStatus  = '';
             }
    		ordItemDtlsId = item.strOrdLineItemDtlsId;
    		poDtlsId = item.strPODtlsId;

             if ((isNaN(poQty) && ((poQty) != '') && (poQty != undefined)) || (isNaN(poPrice) && ((poPrice) != '') && (poPrice != undefined))) {
	    			charMsg++;
	    		}
             if(poPnum != '' || poQty != '' || poPrice != ''){
		    		if((descType == '') || (poStatus == '')){
		    			distypeMsg++;
		    		}
	    		}
             if(!parseNull(ordItemDtlsId)){
	    			ordItemDtlsId = 0; 
	    		}

	    		if(poPnum != ''){
	    			pnum_inputString += poPnum + ',';
	    		}
	    		if(!parseNull(poDtlsId)){
	    			poDtlsId = $scope.poDtlsId;
	    		}
	    	
	    		if(charMsg == 0 && distypeMsg == 0){
	    			if(poPnum != '' || poQty != '' || poPrice != ''){
		    		//form the JSON object to save the line item entry details
					detailString = detailString +
					    '{"podtlid":"' + poDtlsId + '",' +
					    '"dopart":"' + doPnum + '",' +
					    '"doqty":"' + doQty + '",' +
					    '"doprice":"' + doPrice + '",' +
					    '"popart":"' + poPnum + '",' +
					    '"poqty":"' + poQty + '",' +
					    '"poprice":"' + poPrice + '",' +
					    '"podistyp":"' + descType + '",' +
					    '"postat":"' + poStatus + '",' +
					    '"ordlinitm":"' + ordItemDtlsId + '"},';
	    			}
	    		}

    		});
	        if(charMsg > 0){
	        	$scope.errMsg = " Invalid char found, please provide values in numbers ";
	            $("#char-err-msg").modal();
	            return false;
	        }else if(distypeMsg > 0){
	        	$scope.msg = " Please select the Discrepancy Type and Resolution ";
    			$("#err-msg").modal();
    			return false;
	        }
	       	if(detailString != ''){
	    		detailString = detailString.substring(0, detailString.length - 1);
				detailString = '[' + detailString + ']';
				
				$scope.gmARDiscrepancyModel = {
	    			 strPOLineItemDtls: detailString,
	    			 strPnumInputStr: pnum_inputString,
	    			 strUserId: localStorage.getItem('strUserId')
	    	           
	    	        };
		    	 //the webservice used to save the line item entries
		        $http.post(url + 'saveDiscrepencyLineItems', angular.toJson($scope.gmARDiscrepancyModel)).then(function (response) {
		           if(response.data != undefined){
		        	  var msgDtls = JSON.stringify(response.data);
			        	if (msgDtls.indexOf('The following Part(s) not Available') == -1) {
			        		$scope.validateReasonMsg = 'Data saved successfully'; 
		            	$scope.POLineItemModal();
		            	$timeout(function () {
		                    $scope.validateReasonMsg = ''; 
		                }, 6000);
		            }else{
		            	$scope.validateReasonMsg = msgDtls;
		            	$timeout(function () {
		                    $scope.validateReasonMsg = ''; 
		                }, 6000);
		            }
		           }
		       });
        	}
    };
    
    //This function used to revised the do details to po details 
    $scope.fnUpdatePObyDO= function () {
    	  
        var do_pnum = '';
        var do_qty = '';
        var do_price = '';
      
        gridLineItems.data.forEach(function (item, index, array) {
            do_pnum = item.strPnum;
            doQty = item.strItemQty;
            doPrice = item.strItemPrice;
            if(parseNull(do_pnum) && parseNull(doQty) && parseNull(doPrice)){
                gridLineItems.data.update(item.id,{strPOPartNum:do_pnum});
                gridLineItems.data.update(item.id,{strPOPartQty:doQty});
                gridLineItems.data.update(item.id,{strPOPrice:doPrice});
                gridLineItems.data.update(item.id,{strDescTypeId:110166});
                gridLineItems.data.update(item.id,{strDescTypeIdName:"Revised PO"});
                gridLineItems.data.update(item.id,{strStatusId:110181});
                gridLineItems.data.update(item.id,{strStatusIdName:"Resolved"});
            }
        });
    };
    
    
    //This function is used to remove the row 
    $scope.fnRemoveRow= function () {
	    var cell = gridLineItems.selection.getCell();
        var po_dtlsId = cell.row.strPODtlsId;
        var ord_itemDtlsId = cell.row.strOrdLineItemDtlsId;
        var do_pnum = cell.row.strPnum;
		
		 if (!parseNull(do_pnum)) {
                  $scope.gmARDiscrepancyModel = {
	    			 strPOLineItemDtls: ord_itemDtlsId,
	    			 strUserId: localStorage.getItem('strUserId')
	    	        };
	        $http.post(url + 'removeDiscrepencyLineItems', angular.toJson($scope.gmARDiscrepancyModel)).then(function (response) {
	            if (response.data != undefined) {
				if (cell.row) {
					gridLineItems.data.remove(cell.row.id);
				}				
	            }
	        });
		 }else{
			 $scope.validateReasonMsg = "Cannot delete existing rows";
         	$timeout(function () {
                 $scope.validateReasonMsg = ''; 
             }, 6000);
		 }
        
	};
	
    //onchange company dropdown to get company details
    $scope.fnchangeCompany = function (strCompId) {
        $scope.strCompanyId = strCompId;
        $http.get(microappUrl + 'loadCompanyInfo/' + strCompId).then(function (response) {
            if (response.data != undefined) {
                $scope.compInfoList = response.data;
                $scope.strCurrency = $scope.compInfoList[0].currency;
                $scope.strcurrencyNm = $scope.compInfoList[0].currencyNm;
                $scope.strDtFormat = $scope.compInfoList[0].dtFormat;
                $scope.strCmpCurrFmt = $scope.compInfoList[0].cmpCurrFmt;
                $scope.strCompanyId = $scope.compInfoList[0].companyId;
                $scope.strCompanyZone = $scope.compInfoList[0].compZone;
                $scope.strLanguageId = $scope.compInfoList[0].languageId;
                $scope.strCompanyLocale = $scope.compInfoList[0].companyLocale;
                $scope.fnSetDates($scope.strDtFormat);
                $scope.fnClearField();

                $("#span-empty_disFdt-err").hide();
                $("#span-empty_disTdt-err").hide();
                $("#span-empty_resFdt-err").hide();
                $("#span-empty_resTdt-err").hide();
                $("#span-empty_ordFdt-err").hide();
                $("#span-empty_ordTdt-err").hide();
                $("#span-dt_compare_disFdt-err").hide();
                $("#span-dt_compare_resFdt-err").hide();
                $("#span-dt_compare_ordFdt-err").hide();

            }
        });
        $("#discrepancy-main").removeClass("col-9").addClass("col-12");
        //Right Panel ID
        $("#po-entry-main").addClass("col-3").hide();
    };

    //this function is clear all the field while click on clear button 
    $scope.fnClearAllField = function () {
        $scope.strDivisionId = "";
        $scope.strParentAccId = "";
        $scope.strFieldSalesId = "";
        $scope.strSalesRepId = "";
        $scope.strDisStartDate = "";
        $scope.strDisEndDate = "";
        $scope.strResStartDate = "";
        $scope.strResEndDate = "";
        $scope.strCustPONumber = "";
        $scope.strOrdId = "";
        $scope.strAccid = "";
        $scope.strRepAccId = "";
        $scope.strRepAcc = "";
        $scope.strFieldSales = "";
        $scope.strSalesRep = "";
        $scope.strParentAcc = "";
        $scope.strCollectorId = "";
        $scope.strResolutionId = "";
        $scope.strCategoryId = "";
        $scope.strOrderFDate="";
        $scope.strOrderTDate="";
        $scope.strADId= "";
        $scope.strVPId= "";
        $("#span-empty_disFdt-err").hide();
        $("#span-empty_disTdt-err").hide();
        $("#span-empty_resFdt-err").hide();
        $("#span-empty_resTdt-err").hide();
        $("#span-empty_ordFdt-err").hide();
        $("#span-empty_ordTdt-err").hide();
        $("#span-dt_compare_disFdt-err").hide();
        $("#span-dt_compare_resFdt-err").hide();
        $("#span-dt_compare_ordFdt-err").hide();
        $scope.hidethisacc = true;
        $("#list-group-acc").removeClass("searchResults");
        $scope.hidethisrep = true;
        $("#list-group-rep").removeClass("searchResults");
        $scope.hidethisfield = true;
        $("#list-group-field").removeClass("searchResults");
        $scope.hidethissales = true;
        $("#list-group-sales").removeClass("searchResults");
        $scope.hidethisad = true;
        $("#list-group-ad").removeClass("searchResults");
        $scope.hidethisvp = true;
        $("#list-group-vp").removeClass("searchResults");
    };
    //Summary Report DO Function for order id in Descrepency Dashboard grid
  
    $scope.fnSummaryDO = function (ordID) {
    	if(ordID.indexOf(',') != -1){
    		ordID = ordID.slice(0, -1);
    	}
         var strPartyId = localStorage.getItem("strPartyId");
        var strCompId = localStorage.getItem("strCompId");
        var strLangId = localStorage.getItem("strLangId");
        var strPlantId = localStorage.getItem("strPlantId");
        var companyInfo = {};
        //form the company info to pass the URL
        companyInfo.cmpid = strCompId;
        companyInfo.plantid = strPlantId;
        companyInfo.token = "";
        companyInfo.partyid = strPartyId;
        companyInfo.cmplangid = strLangId;
        var StringifyCompanyInfo = JSON.stringify(companyInfo);

        var features = 'directories=no,menubar=no,status=no,titlebar=no,toolbar=no,width=800,height=800';
        var url = 'GmEditOrderServlet?hMode=PrintPrice&hOrdId=' + ordID;
        window.open(encodeURI(window.location.protocol + '//' + window.location.hostname + '/' + url + '&companyInfo=' + StringifyCompanyInfo, 'DO Summary', features));
    };

    
    /* This function used to fetch the po detail id list for Collector,Category,Resolution,Remind Me Days */
    $scope.fnLoadPOIdList = function () {
        $scope.collectorHistFl = '';
        $scope.categoryHistFl = '';
        $scope.resolutionHistFl = '';
        $scope.strRemindMedays = '';
        $scope.errorMessage = '';
        $scope.gmARDiscrepancyModel = {
            strPONumDtlsId: $scope.strPONumDtlsId
        };
        $http.post(url + 'fetchPODiscrepencyIdList', angular.toJson($scope.gmARDiscrepancyModel)).then(function (response) {
            if (response.data.status == undefined) {
                $scope.strPONumDtlsId = response.data.strOrderPODtlId;
                $scope.collectorHistFl = response.data.strCollectorHistoryFl;
                $scope.categoryHistFl = response.data.strPoDiscCategoryHisFl;
                $scope.resolutionHistFl = response.data.strPoDiscResolutionHisFl;
                $scope.strRemindMedays = response.data.strPoDiscRemaindDays;
                if(parseNull($scope.strRemindMedays)){
                  $scope.strRemindMeDate =  $filter('date')(response.data.strPoDiscRemaindDate,$scope.strDtFormat,$scope.strCompanyZone);
                  $('#remindMeDate').datepicker("setDate", $scope.strRemindMeDate);   
                }else{
                    $('#remindMeDate').datepicker("setDate", new Date()); 
                } 
                $scope.strPOStatus = response.data.strCustPoStatus;
                $scope.strAcctId = response.data.strAccountId;
                $scope.strCustomerPo = response.data.strCustPoId;
                $scope.strPOAmt = response.data.strCustPoAmt;
                $scope.strCollectorPOId = parseNull(response.data.strCollectorId);
                if(!parseNull($scope.strCollectorPOId)){
                    $scope.strCollectorPOId = $scope.strDefaultCollector;
                }
                $scope.strCategoryPOId = parseNull(response.data.strPoDiscCategoryType);
                $scope.strResolutionPOId = parseNull(response.data.strPoDiscResolutionType);
                $scope.fnValidateAccessFlagForDis();
            }
        });
    }
  
    /* This function used to Save the Collector,Category,Resolution Details */
    $scope.fnSavePOCollectorDetails = function (savType) {
        $scope.errorMessage = '';
        if(savType == 'Category'){
            $('#preloader-for-category').show();
        }
        else if(savType == 'Collector'){
            $('#preloader-for-collector').show();
        }
        else if(savType == 'Resolution'){
           $('#preloader-for-resolution').show(); 
        }
        $scope.gmARDiscrepancyModel = {
            strPONumDtlsId: $scope.strPONumDtlsId,
            strCollectorId: $scope.strCollectorPOId,
            strCategoryId: $scope.strCategoryPOId,
            strResolutionId: $scope.strResolutionPOId,
            strUserId: localStorage.getItem('strUserId'),
            strCompanyId: localStorage.getItem('strCompId'),
            strPlantId: localStorage.getItem('strPartyId'),
            strCompZone: $scope.strCompanyZone
        };
        
        $http.post(url + 'savePODiscrepencyDetails', angular.toJson($scope.gmARDiscrepancyModel)).then(function (response) {
            if (response.data.status == undefined) {
                $scope.strPONumDtlsId = response.data.strOrderPODtlId;
                $scope.collectorHistFl = response.data.strCollectorHistoryFl;
                $scope.categoryHistFl = response.data.strPoDiscCategoryHisFl;
                $scope.resolutionHistFl = response.data.strPoDiscResolutionHisFl;
                $scope.strRemindMedays = response.data.strPoDiscRemaindDays;
                if(parseNull($scope.strRemindMedays)){
                  $scope.strRemindMeDate =  $filter('date')( response.data.strPoDiscRemaindDate,$scope.strDtFormat,$scope.strCompanyZone);
                  $('#remindMeDate').datepicker("setDate", $scope.strRemindMeDate);   
                }else{
                    $('#remindMeDate').datepicker("setDate", new Date()); 
                }
                $scope.strPOStatus = response.data.strCustPoStatus;
                $scope.strAcctId = response.data.strAccountId;
                $scope.strCustomerPo = response.data.strCustPoId;
                $scope.strPOAmt = response.data.strCustPoAmt;
                if(!parseNull($scope.strCollectorPOId)){
                    $scope.strCollectorPOId = $scope.strDefaultCollector;
                }
                $scope.strCategoryPOId = parseNull(response.data.strPoDiscCategoryType);
                $scope.strResolutionPOId = parseNull(response.data.strPoDiscResolutionType);
            }
            else{
                $scope.errorMessage = errMsg;  
                 $timeout(function () {
                    $scope.errorMessage = '';
                }, 5000);
            }
        if(savType == 'Category'){
            $('#preloader-for-category').hide();
        }
        else if(savType == 'Collector'){
            $('#preloader-for-collector').hide();
        }
        else if(savType == 'Resolution'){
           $('#preloader-for-resolution').hide(); 
        }
            $scope.fnValidateAccessFlagForDis();
        });
    };
    //This function is used calculate days
    $scope.fnCalculateDays = function () {
         var currentdate = $filter('date')(new Date(),$scope.strDtFormat,$scope.strCompanyZone);
        $scope.strRemindMedays = '';
        var pickdate = $scope.strRemindMeDate;
        dateDiff(currentdate, pickdate, $scope.strDtFormat, function (response) {
           if(response>0){
              $scope.strRemindMedays = response;
              $scope.fnRemindMeDaysSave(); 
               $('#span-remind-greater-err').addClass('hide');
           }
            else if(response<0){
               $('#span-remind-greater-err').removeClass('hide');
            }
            else if(response==0){
                $('#span-remind-greater-err').addClass('hide');
            }
        });
    };
    
    /* This function used to Save the Remind Me Days */
    $scope.fnRemindMeDaysSave = function () {
        $('#preloader-for-remindme').show();
        $scope.gmARDiscrepancyModel = {
            strPONumDtlsId: $scope.strPONumDtlsId,
            strRemindMedays: $scope.strRemindMedays,
            strDtFormat: $scope.strDtFormat,
            strUserId: localStorage.getItem('strUserId'),
            strCompanyId: localStorage.getItem('strCompId'),
            strPlantId: localStorage.getItem('strPartyId'),
            strCompZone: $scope.strCompanyZone
        };
        $http.post(url + 'saveRemindMeDiscrepencyDetails', angular.toJson($scope.gmARDiscrepancyModel)).then(function (response) {
            if (response.data.status == undefined) {
                $scope.strPONumDtlsId = response.data.strOrderPODtlId;
                $scope.collectorHistFl = response.data.strCollectorHistoryFl;
                $scope.categoryHistFl = response.data.strPoDiscCategoryHisFl;
                $scope.resolutionHistFl = response.data.strPoDiscResolutionHisFl;
                $scope.strRemindMedays = response.data.strPoDiscRemaindDays;
                if(parseNull($scope.strRemindMedays)){
                  $scope.strRemindMeDate =  $filter('date')(response.data.strPoDiscRemaindDate,$scope.strDtFormat,$scope.strCompanyZone);
                  $('#remindMeDate').datepicker("setDate", $scope.strRemindMeDate);   
                }else{
                    $('#remindMeDate').datepicker("setDate", new Date()); 
                }
                $scope.strPOStatus = response.data.strCustPoStatus;
                $scope.strAcctId = response.data.strAccountId;
                $scope.strCustomerPo = response.data.strCustPoId;
                $scope.strPOAmt = response.data.strCustPoAmt;
                if(!parseNull($scope.strCollectorPOId)){
                    $scope.strCollectorPOId = $scope.strDefaultCollector;
                }
                $scope.strCategoryPOId = parseNull(response.data.strPoDiscCategoryType);
                $scope.strResolutionPOId = parseNull(response.data.strPoDiscResolutionType);
                $scope.fnValidateAccessFlagForDis();
            }
            else{
                $scope.errorMessage = errMsg;  
                 $timeout(function () {
                    $scope.errorMessage = '';
                }, 5000);
            }
            $('#preloader-for-remindme').hide();
        });
    };
    /* This function used to fetch POLog Details for Collector,Category and Resolution */
    $scope.fnFetchPOLogDetails = function (histype){
        $('#poDetailLogModal').modal();
        if(histype == '109661'){
            $scope.logheadermsg ='Collector Log Details';
        }
        else if(histype == '109662'){
            $scope.logheadermsg ='Category Log Details';
        }
        else if(histype == '109663'){
            $scope.logheadermsg ='Resolution Log Details';
        }
        $scope.gmARDiscrepancyModel = {
            strPONumDtlsId: $scope.strPONumDtlsId,
            strPODtlHisType : histype,
            strDtFormat: $scope.strDtFormat
        };
        $http.post(url + 'fetchPOLogDetails', angular.toJson($scope.gmARDiscrepancyModel)).then(function (response){
            if (response.data.status == undefined) {
                $scope.poLogDetlList = response.data;
            }
            else{
                $scope.errorMessage = errMsg;  
                 $timeout(function () {
                    $scope.errorMessage = '';
                }, 5000);
            }
            });
        $('.dismiss').click();
    };
    
    $scope.fnValidateAccessFlagForDis = function(){
        $scope.editDisFl ='N';
        
        if($scope.AccessFl == 'Y' && $scope.strPOStatus == 109544) {//Move to Discrepancy Status
            $scope.editDisFl = 'Y';
        }
    }; 
/*
    This is used to validate category,PO Number,Comments,Resolution
*/
     $scope.fnSaveReadyPOStatus = function (status) {
        $scope.commentsCount = ParentScopes.get('GmPendingPOController').commentsCount;
        $scope.validatePONumMsg = '';
        $scope.validateCommentsMsg = '';
        $scope.validateCategoryMsg = '';
        $scope.validateResolutionMsg = '';
        $scope.confirmMsg = '';
        var validFl = 'Y';
        var msg = '';
         if ($('#strCustomerPo').val().trim() == '') {
            validFl = 'N';
            $scope.validatePONumMsg = 'Please provide PO Number';
        }
        if ($scope.commentsCount == 0) {
            validFl = 'N';
            $scope.validateCommentsMsg = 'Please provide Comments';
        }
        if (!parseNull($scope.strCategoryPOId)) {
             validFl = 'N';
            $scope.validateCategoryMsg = 'Please Select Category';
        }
        if (!parseNull($scope.strResolutionPOId)) {
             validFl = 'N';
            $scope.validateResolutionMsg = 'Please Select Resolution';
        }
         if (validFl == 'Y') {
             $scope.fnValidateSavePOStatus(status);
               $scope.fnRefreshRightPanel();
         }
    };

   
     /*This function is used to validate the Remind validation*/
        $scope.fnRemindMeValidation = function (){
            var remdays=$scope.strRemindMedays;
            if(remdays.trim() !=''){
                if(isNaN(remdays)){
                    $('#strRemindMedays').addClass('error-field');
                    $('#span-po-remind-days-err').show();
                }
                else{
                 $('#strRemindMedays').removeClass('error-field');
                 $('#span-po-remind-days-err').hide();           
                }
            }
            else{
                 $('#strRemindMedays').removeClass('error-field');
                 $('#span-po-remind-days-err').hide();               
            }
        };
//this funtion used to show PO format popun screen
    $scope.fnFetchPOFormat = function () {
        $scope.gmARCustPOModel = {
            strAccid: $scope.strAcctId,
            strDtFormat: $scope.strDtFormat
        };
        $scope.poFormatList = '';
        $scope.strPOFmtParrAccNm = '';
        var table = $('#poFormatTable').DataTable();
        table.destroy();
        $http.post(url + 'fetchPOFormatDrilldownDetails', angular.toJson($scope.gmARCustPOModel)).then(function (response) {
            if (response.data.status == undefined) {
                $scope.poFormatList = response.data;
                $scope.strPOFmtParrAccNm = $scope.strParentAccNm;
                angular.element(document).ready(function () {
                    table = $('#poFormatTable').DataTable(
                        {
                            "bPaginate": false,
                            dom: 'Bfrtip',
                            "language": {
                                "emptyTable": "No data found to display",
                                "zeroRecords": "No data found to display"
                            },
                            buttons: [],
                            "order": [],
                            "columnDefs": [{
                                "targets": 'no-sort',
                                "orderable": false
                            }]
                        });

                });

            }
            else {
                $scope.errorMsg = response.data.message;
                sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                $scope.errorHandler();
            }
        });
    };

    /* This function is used to open PO Format model */
    $scope.fnDiscPODetailView = function () {
        $('#poFormatModal').modal();
        $scope.fnFetchPOFormat();
        $('.dismiss').click();
    };
    // this function call used to show uploaded file list and provides drag and drop option to add new file 

    $scope.fnLoadFiles = function () {

        var AccessFl = $scope.AccessFl;
        var strUserId = localStorage.getItem('strUserId');
        var strDtFormat = $scope.strDtFormat;
        var strCompTimeZone = $scope.strCompanyZone;
        var strCompanyLocale =  $scope.strCompanyLocale;

        var viewMode = "list"; // ex: list or grid
        var scaleFactor = 2;
        var autoSendFl = true;
        var refID = $scope.strOrderId;
        var refType = "53018"; // DO
        var refGroupType = "26240412"; // DO
        var refSubtype = "26230732"; // DO document upload

        var uploadURL = microappUrlFile + "arUploadFile";
        var saveURL = microappUrlFile + "saveUploadFile";
        var fetchInfoURL = microappUrlFile + "loadUploadFileDetails";
        var deleteURL = microappUrlFile + "deleteFile";
	    var fileUploadDownloadPath = "download-dir/"+strCompanyLocale+"/"+refID;
        var downloadURL = microappUrlFile+"downloadFile/"+fileUploadDownloadPath+"/";
        var uploadPath = fileUploadPath+strCompanyLocale+"//"+refID+"//";
        var doUpdateFlURL = microappUrlFile + "doUpdateFlag";

        var fileDetArr = [];
        var fileNameArr = [];

        var divRef = "dhtmlxvault-file-upload-po";
        var divWidth = ""; // width of file upload div element
        var divHeight = 200; // height of file upload div element

        $scope.gmUploadFilePOModel = {
            strRefGrp: refGroupType, // DO
            strRefId: refID, // Order Id
            strRefType: refType, // DO
            strType: "0", // DO document upload
            strFileTitle: "0", // Upload File Title
            strFileName: "0", // Upload File Name
            strPartNumberId: "0", // Part Number Id
            strDateFormat: strDtFormat // Company Date Format
        };
        // Get uploaded file information based on selected PO order detail       
        fnGetUploadedFiles($scope.gmUploadFilePOModel, fetchInfoURL, function (serverFiles, serverFileNames) {
            fileDetArr = serverFiles; // serverFiles info is stored to fileDetArr to use initial parse on vault
            fileNameArr = serverFileNames; // serverFileNames info is stored to fileNameArr to use duplicate file name check in vault
            //this initDHTMLXValut() call used to show drag and drop file upload option 
            $("#dhtmlxvault-file-upload-po").empty(); 
            var dhtmlVault = initDHTMLXValut(divRef, uploadURL, downloadURL, saveURL, fetchInfoURL, doUpdateFlURL, viewMode, scaleFactor, autoSendFl, divWidth, divHeight, refID, refType, refSubtype, refGroupType, fileDetArr, deleteURL, AccessFl, fileNameArr, strDtFormat, strCompTimeZone, uploadPath, strUserId);
        });


    }
    
     $scope.fnValidateSavePOStatus = function (status){
         $scope.status = status;
         $scope.strTotal = ParentScopes.get('GmPendingPOController').strTotal;
         $scope.strOrdertype = ParentScopes.get('GmPendingPOController').strOrdertype;
         $scope.strBackOrderCnt = ParentScopes.get('GmPendingPOController').strBackOrderCnt;
        $scope.gmARCustPOModel = {
            strAccid: $scope.strAcctId,
            strCustPONumber: $scope.strCustomerPo,
            strPOStatus: status,
            strUserId: localStorage.getItem('strUserId')
        };
        $scope.confirmheader = '';
        var dir_bck_id = ParentScopes.get('GmPendingPOController').strDirBckOrdIds;
        var back_ord_msg = '';
        var po_amt_msg = '';
        var po_empty_msg='';
         var msg = '';
      
            if (status == '109542') {
                $scope.confirmheader = 'Ready For Invoicing';
                if ($scope.strOrdertype == 26240232 && $scope.strBackOrderCnt > 0) {
                    if (dir_bck_id.length > 0) {
                        back_ord_msg = 'This Order have following Back Order(s) ' + dir_bck_id + '.';
                    }
                }
                if(!parseNull($scope.strPOAmt)){
                    po_empty_msg = "without PO Amount";
                }
                if(parseNull($scope.strPOAmt)){
                if ($scope.strPOAmt.length > 0) {
                    if ($scope.strPOAmt != $scope.strTotal) {
                        po_amt_msg = ' PO Amount & Total Amount is not matching.';
                    }
                }
                }
                if(!parseNull($scope.strPOAmt)){
                    po_empty_msg = "without PO Amount";
                }
               
                if(po_amt_msg !='' || back_ord_msg !='' || po_empty_msg !=''){
                    msg = back_ord_msg + po_amt_msg + " Do you want to Proceed Ready For Invoicing "+po_empty_msg+" ?";
                }
            } 

            if (msg != '') {
                $scope.confirmMsg = msg;
                $('#conFModal').modal();
                $('.dismiss').click();
            } else {
                $http.post(url + 'savePOStatus', $scope.gmARCustPOModel).then(function (response) {
                    if (response.data.status == undefined) {
                        $scope.fnRefreshRightPanel();
                    }else{
                         $scope.errorMessage = errMsg;
                     $timeout(function () {
                         $scope.errorMessage = '';
                     }, 5000);
                    }
                });
            }
    };

    //this function is used to save PO status after confirmation

    $scope.confirmReadyPOStatus = function () {
        $scope.gmARCustPOModel = {
            strAccid: $scope.strAcctId,
            strCustPONumber: $scope.strCustomerPo,
            strPOStatus: $scope.status,
            StrUserId: localStorage.getItem('strUserId')

        };
        $http.post(url + 'savePOStatus', $scope.gmARCustPOModel).then(function (response) {
            if (response != undefined) {
                $scope.fnRefreshRightPanel();
            }
        });
    };
    
    /* This function used to clear the Comments */
    $scope.fnClearComments = function () {
        $scope.strComments ='';
    };
    /*This Function is used to save the common log comments*/

    $scope.saveLogComments = function () {
        var comments = $scope.strComments;
        if (comments!='') {
            $scope.commentsCount = 1;
            $('#preloader-for-comments').show();
            $scope.gmCommonLog = {
                refId:$scope.strOrderId,
                comments: $scope.strComments,
                type: '1200',
                createdBy: localStorage.getItem('strUserId'),
                strTimeZone: $scope.strCompanyZone,
                strDateFormat: $scope.strDtFormat
            };
            
            fnSaveLogComments($http,$scope.gmCommonLog, function (savelogcomres) { //1200-common log id
                if (savelogcomres.data.status == undefined) {
                    fnLoadComments($http, $scope.strOrderId, '1200', function (loadcommres) { //1200 common log id
                        if (loadcommres.data.status == undefined) {
                            $scope.commentsList = loadcommres.data;
                        } else {
                            $scope.errorMsg = response.data.message;
                            sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                            $scope.errorMessage = errMsg;
                            $timeout(function () {
                                $scope.errorMessage = '';
                            }, 5000);
                        }
                     
                    });
                    $scope.validateCommentsMsg = '';
                }
                else{
                     $scope.validateCommentsMsg = '';
                     $scope.errorMessage = errMsg;
                     $timeout(function () {
                         $scope.errorMessage = '';
                     }, 5000);
                }
                   $('#preloader-for-comments').hide();
            });
            $scope.strComments = '';
        } 
        else{
            $scope.strComments ='';
            $('#preloader-for-comments').hide();
        }

    };
    /*This Function is used to fetch comments*/
    $scope.fnFetchComments = function () {
        fnLoadComments($http, $scope.strOrderId, '1200', function (loadcommres) { //1200 common log id
            if (loadcommres.data.status == undefined) {
                $scope.commentsList = loadcommres.data;
            } else {
                $scope.errorMessage = errMsg; 
                 $timeout(function () {
                    $scope.errorMessage = '';
                }, 5000);
            }
        });
    };
     
   
    /* Following functions are used from GmPendingController.js(emit)*/
    /* This function used to load PO Order details(Collector,Category,Resoltion) */
    $scope.fnLoadPOEntry = function () {
        $scope.errorMessage='';
        $rootScope.$emit("fnLoadPOEntry", $scope.strOrderId);
        $scope.strAcctId = ParentScopes.get("GmPendingPOController").strAcctId;
        $scope.strPONumDtlsId = ParentScopes.get("GmPendingPOController").strPONumDtlsId;
        $scope.errorMessage = ParentScopes.get('GmPendingPOController').errorMessage;
    };
    /* This function used to load PO details */
    $scope.fnFetchPODetails = function () {
        $scope.errorMessage='';
        $rootScope.$emit("fnFetchPODetails");
        $scope.errorMessage = ParentScopes.get('GmPendingPOController').errorMessage;
    };
    
    /* This function used to show the audit log history*/
    $scope.auditLogModal = function () {
        $rootScope.$emit("auditLogModal");
    };
    /* This function used to show the Existing PO Details */
    $scope.poDrillDownModal = function () {
        $rootScope.$emit("poDrillDownModal");
    };
    /* This function used to save the Order Revenue Details */
    $scope.fnsaveOrderRevDtls = function () {
        $scope.errorMessage='';
        $rootScope.$emit("fnsaveOrderRevDtls");
        $scope.errorMessage = ParentScopes.get('GmPendingPOController').errorMessage;
        console.log($scope.errorMessage);
        if(!parseNull($scope.errorMessage)){
           $('#err-div').removeClass('hide');
           $('#err-div').focus();
        }
    };
    /* This function used to check the new PO with existing PO  */
    $scope.fnPOCheck = function () {
        $rootScope.$emit("fnPOCheck");
    };
    /* This function used to check the save PO Number  */
    $scope.fnSavePOCheck = function () {
        $rootScope.$emit("fnSavePOCheck");
    };
    /* This function used to Cofirmation for PO Number  */
    $scope.confirmPONumber = function () {
        $scope.errorMessage='';
        $rootScope.$emit("confirmPONumber");
        $scope.strCustomerPo = ParentScopes.get('GmPendingPOController').strCustomerPo;
        $scope.errorMessage = ParentScopes.get('GmPendingPOController').errorMessage;
    };
    /* This function used to Cancel for PO Number  */
    $scope.ConfirmPOCancel = function () {
        $rootScope.$emit("ConfirmPOCancel");
    };
    /* This function used to save the PO Number  */
    $scope.fnSavePONum = function () {
        $scope.errorMessage='';
        $rootScope.$emit("fnSavePONum",$scope.strCustomerPo);
        $scope.errorMessage = ParentScopes.get('GmPendingPOController').errorMessage;
        $scope.strCustomerPo = ParentScopes.get('GmPendingPOController').strCustomerPo;
    };
    /* This function used to save the PO Amount */
    $scope.fnPOAmtSave = function () {
        $scope.errorMessage='';
        $rootScope.$emit("fnPOAmtSave");
        $scope.errorMessage = ParentScopes.get('GmPendingPOController').errorMessage;
        $scope.strCustomerPo = ParentScopes.get('GmPendingPOController').strCustomerPo;
    };
    /* This function used to validation for PO Amount */
    $scope.fnPOAmtValidation = function () {
        $scope.errorMessage='';
        $rootScope.$emit("fnPOAmtValidation");
    };
    /* This function used to get the existing PO Details */
    $scope.fnFetchPODrillDown = function () {
        $rootScope.$emit("fnFetchPODrillDown");
    };
    
    /* This function used to refresh the right panel */
    $scope.fnRefreshRightPanel = function () {
        $rootScope.$emit("fnRefreshRightPanel");
        $scope.fnLoadPOIdList();
    };
    /* This function used to refresh the right panel */
    $scope.fnCloseRightPanel = function () {
        $rootScope.$emit("fnCloseRightPanel");
    };
    /* This function used to clear the PO Number */
    $scope.fnClearPONum = function () {
        $rootScope.$emit("fnClearPONum");
    };
   /* This function used to download ipad orders pdf */
    $scope.fnDownloadDOPdfURL = function () {
        $rootScope.$emit("fnDownloadDOPdfURL",$scope.strOrderId);
    };
    
});
//this function is used to perform action when a cell is editing
function doOnCellEdit(stage, rowId, cellInd, nValue, oValue) {
    var po_part_num = grid.getColIndexById("strPOPartNum");
    var po_qty = grid.getColIndexById("strPOPartQty"); 
    var po_price = grid.getColIndexById("strPOPrice");

	//Highlight data when edit cell
	if (stage == 1 && (cellInd == po_part_num || cellInd == po_qty || cellInd == po_price )){
		var c=this.editor.obj;			
		if(c!=null) c.select();
	}
		
	return true;
}
