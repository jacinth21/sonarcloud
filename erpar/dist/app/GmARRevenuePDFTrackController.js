app.controller('GmARRevenuePDFTrackController', function ($scope, $http, $q, $timeout,$filter,$location) {
	$scope.arrPoList =[];
	$scope.arrOverrideList =[];
	let grid;
	let datacollection;
	var overrideDOBeforeEdit;
	var overridePOBeforeEdit;
	var commentsBeforeEdit;
    var AfterAuthentication = function () {
        var deferred = $q.defer();
        while (!KeycloakService.keycloak.authenticated) {}
        deferred.resolve(KeycloakService.keycloak.authenticated);
        return deferred.promise;
    }
	    var AfterAuthentication = function () {
        var deferred = $q.defer();
        while (!KeycloakService.keycloak.authenticated) {}
        deferred.resolve(KeycloakService.keycloak.authenticated);
        return deferred.promise;
    }
    
    $(document).ready(function () {
        $http.get(varJsonURL).then(function (response) {}, function (response) {
            $scope.errorMsg = response.data.message;
            sessionStorage.setItem("errorMsg", ($scope.errorMsg));
            $scope.errorHandler();
        });
        
       //Enable drag model popup
          $('#pdfDrillDownModal').draggable();
        
        var varLoginName = localStorage.getItem('strLoginName');
        var strPartyId = "";
        var strUserId = "";
        var strCompId = "";
        $scope.AccessFl = 'N';
        $scope.editFl= false;
        
        /*Web Service Call for party details from user name*/
        fnLoadUserInfo($http, varLoginName, function (response) {
            if (response.data != undefined) {
                strCompId = response.data[0].strCompanyId;
                strPartyId = response.data[0].strPartyId;
                strUserId = response.data[0].strUserId;
                localStorage.setItem("strPartyId", strPartyId);
                localStorage.setItem("strCompId", strCompId);
                localStorage.setItem("strUserId", strUserId);
                /*Web Service Call for Load Company details  based on login user*/
                fnLoadCompany($http, strPartyId, function (response) {
                    if (response.data != undefined) {
                        $scope.compList = response.data;
                        /*Web Service Call for company information from company id*/
                        fnLoadCompanyInfo($http, strCompId, function (response) {
                            if (response.data != undefined) {
                                $scope.selectedCompany = strCompId;
                                $scope.compInfoList = response.data;
                                $scope.strCompanyZone = $scope.compInfoList[0].compZone;
                                $scope.strDtFormat = $scope.compInfoList[0].dtFormat;
                                $scope.strCurrency = $scope.compInfoList[0].currency;
                                $scope.strcurrencyNm = $scope.compInfoList[0].currencyNm;
                                $scope.strCmpCurrFmt = $scope.compInfoList[0].currencyFormat;
                                $scope.strCompanyId = $scope.compInfoList[0].companyId;
                                $scope.strLanguageId = $scope.compInfoList[0].languageId;
                                $scope.strCompanyLocale = $scope.compInfoList[0].companyLocale;
                                $scope.strPlantId = $scope.compInfoList[0].plantId;
                                localStorage.setItem("strLangId", $scope.strLanguageId);
                                localStorage.setItem("strPlantId", $scope.strPlantId);
                                localStorage.setItem("strCompZone", $scope.strCompanyZone);
                                // Calling dashboard  widget 
                                $scope.fnFetchDashboardWidgets();
                                $scope.fnSetDates($scope.strDtFormat);
                            }
                        });
                    } else {
                        $scope.errorMsg = response.data.message;
                        sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                        $scope.errorHandler();
                    }
                });
                
                
                /* This function used to get the dropdown values for state DropDown */    
                fnCodeLookUpValuesExclude($http,$scope,'STATE','',strCompId,function(response){
                     $scope.statelist = response.data;
                });
                
                /*This function is used to get the update access flag for based on PartyId*/
                fnSecurityEvent($http, strPartyId, 'ORD_PO_ENTRY_ACC', function (secresponse) {
                    if (secresponse.data != undefined && secresponse.data != '') {
                        if (secresponse.data[0].updateAccessFl != undefined) {
                            $scope.AccessFl = secresponse.data[0].updateAccessFl;
                            if($scope.AccessFl == 'Y'){
                            	$scope.editFl= true;
                            }else{
                            	$scope.editFl= false;
                            }
                        }
                    }
                });

            } else {
                $scope.errorMsg = response.data.message;
                sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                $scope.errorHandler();
            }
            
           
            
        });
        
        
        


/* This function used to get the dropdown values for category DropDown */    
        fnCodeLookUpValues($http,$scope,'REVCA',function(response){
             $scope.categorylist = response.data;
        });

/* This function used to get the dropdown values for Aging DropDown */    
        fnCodeLookUpValues($http,$scope,'REVAG',function(response){
             $scope.aginglist = response.data;
        });


 /* This function used to get the dropdown values for PO*/  
        fnCodeLookUpValues($http,$scope,'REVST',function(podropres){
        	 $scope.arrPoList.push('Choose one');
            angular.forEach(podropres.data, function(value, key) {
	        $scope.arrPoList.push(value.codeNm);
           });            
            $scope.poDtlList = podropres.data;
        });
        
 /* This function used to get the dropdown values for DO*/ 
        fnCodeLookUpValues($http,$scope,'RSTDO',function(dodropres){
        	 $scope.arrOverrideList.push('Choose one');
	     angular.forEach(dodropres.data, function(value, key) {
	        $scope.arrOverrideList.push(value.codeNm);
           });  
            $scope.doDtlList = dodropres.data;
        });
        
        // template load by path
        if($location.path() == '/gmrevenuereport'){
        	 $('.overlldiv').hide();
        	 $('.reportdiv').show();
        	 $scope.headerName ='Revenue Track Report';
        }else{
        	 $('.overlldiv').show();
        	 $('.reportdiv').hide();
        	 $scope.headerName ='Revenue Track Dashboard';
        }
       $("#preloader-spin").hide();
       $('#no-data-found').hide();

       $http.get(varJsonURL).then(function (response) {
          
           varDateCompforOrder = response.data.dateComparisonforOrder;
           varOrderFromDtMsg = response.data.emptyFromDateforOrder;
           varOrderToDtMsg = response.data.emptyToDateforOrder;
       }, function (response) {
           $scope.errorMsg = response.data.message;
           sessionStorage.setItem("errorMsg", ($scope.errorMsg));
           $scope.errorHandler();
       });
       
    });
    
    // Function used to fetch widgets details
    $scope.fnFetchDashboardWidgets = function () {
    	
    	$scope.fnFetchPendingApprovalWidgets();
    	$scope.fnFetchunRecognizeWidgets();
    	$scope.fnFetchNoPdfWidgets();
	};

	// Function used to fetch pending approval widget counts
	$scope.fnFetchPendingApprovalWidgets = function () {
		
		$('#div-for-pendappcnt').hide();
		$('#div-for-pendapptodaycnt').hide();
		$('#div-for-pendapponecnt').hide();
		$('#div-for-pendapptwocnt').hide();
		$('#div-for-pendappgreatertwocnt').hide();
		$('#preloader-for-pendappcnt').show();
		$('#preloader-for-pendapptodaycnt').show();
		$('#preloader-for-pendapponecnt').show();
		$('#preloader-for-pendapptwocnt').show();
		$('#preloader-for-pendappgreatertwocnt').show();
		
		// 110120 Recognized
		// Aging as Today
		fnFetchRevenueCnt('0','110120',function (strPendAppTodayCnt) {
			$scope.strPendAppTodayCnt = strPendAppTodayCnt;
			$('#preloader-for-pendapptodaycnt').hide();
			$('#div-for-pendapptodaycnt').show();
			
			// Aging as one day
			fnFetchRevenueCnt('1','110120',function (strPendAppOneCnt) {
				$scope.strPendAppOneCnt = strPendAppOneCnt;
				$('#preloader-for-pendapponecnt').hide();
				$('#div-for-pendapponecnt').show();
				
				// Aging as two days
				fnFetchRevenueCnt('2','110120',function (strPendAppTwoCnt) {
					$scope.strPendAppTwoCnt = strPendAppTwoCnt;
					$('#preloader-for-pendapptwocnt').hide();
					$('#div-for-pendapptwocnt').show();
					
					// Aging as greater than two days
					fnFetchRevenueCnt('3','110120',function (strPendAppGreaterTwocnt) {
						$scope.strPendAppGreaterTwocnt = strPendAppGreaterTwocnt;
						$('#preloader-for-pendappgreatertwocnt').hide();
						$('#div-for-pendappgreatertwocnt').show();
						
						// Pending approval total cnt 
						$scope.strPendAppCnt = parseInt(strPendAppTodayCnt)
												+parseInt(strPendAppOneCnt)
												+parseInt(strPendAppTwoCnt)
												+parseInt(strPendAppGreaterTwocnt);
						$('#preloader-for-pendappcnt').hide();
						$('#div-for-pendappcnt').show();
						
					});
		        
				});
	        
			});
        
		});
		
	};
	
	// Function used to fetch unrecognized widget counts
	 $scope.fnFetchunRecognizeWidgets = function () {
		 $('#div-for-unreccnt').hide();
		 $('#div-for-unrectodaycnt').hide();
		 $('#div-for-unreconecnt').hide();
		 $('#div-for-unrectwocnt').hide();
		 $('#div-for-unrecgreatertwocnt').hide();
		 $('#preloader-for-unreccnt').show();
		 $('#preloader-for-unrectodaycnt').show();
		 $('#preloader-for-unreconecnt').show();
		 $('#preloader-for-unrectwocnt').show();
		 $('#preloader-for-unrecgreatertwocnt').show();
		 
		 // 110121 - unRecognized
		// Aging as Today
		 fnFetchRevenueCnt('0','110121',function (strUnRecTodayCnt) {
				$scope.strUnRecTodayCnt = strUnRecTodayCnt;
				$('#preloader-for-unrectodaycnt').hide();
				$('#div-for-unrectodaycnt').show();
				
				// Aging as one day
				fnFetchRevenueCnt('1','110121',function (strUnRecOneCnt) {
					$scope.strUnRecOneCnt = strUnRecOneCnt;
					$('#preloader-for-unreconecnt').hide();
					$('#div-for-unreconecnt').show();
					
					// Aging as two days
					fnFetchRevenueCnt('2','110121',function (strUnRecTwoCnt) {
						$scope.strUnRecTwoCnt = strUnRecTwoCnt;
						$('#preloader-for-unrectwocnt').hide();
						$('#div-for-unrectwocnt').show();
						
						// Aging as greater than two days
						fnFetchRevenueCnt('3','110121',function (strUnRecGreaterTwocnt) {
							$scope.strUnRecGreaterTwocnt = strUnRecGreaterTwocnt;
							$('#preloader-for-unrecgreatertwocnt').hide();
							$('#div-for-unrecgreatertwocnt').show();
				        
							// Unrecognized total count
							$scope.strUnRecCnt = parseInt(strUnRecTodayCnt)
													+parseInt(strUnRecOneCnt)
													+parseInt(strUnRecTwoCnt)
													+parseInt(strUnRecGreaterTwocnt);
							$('#preloader-for-unreccnt').hide();
							$('#div-for-unreccnt').show();
							
						});
			        
					});
		        
				});
	        
			});
	 };
	 
	// Function used to fetch No pdf widget counts
	 $scope.fnFetchNoPdfWidgets = function () {
		
		 $('#div-for-nopdfcnt').hide();
		 $('#div-for-nopdftodaycnt').hide();
		 $('#div-for-nopdfonecnt').hide();
		 $('#div-for-nopdftwocnt').hide();
		 $('#div-for-nopdfgreatertwocnt').hide();
		 $('#preloader-for-nopdfcnt').show();
		 $('#preloader-for-nopdftodaycnt').show();
		 $('#preloader-for-nopdfonecnt').show();
		 $('#preloader-for-nopdftwocnt').show();
		 $('#preloader-for-nopdfgreatertwocnt').show();
		 
		// 110122 - unRecognized no pdf
		// Aging as today
		 fnFetchRevenueCnt('0','110122',function (strNoPdfTodayCnt) {
				$scope.strNoPdfTodayCnt = strNoPdfTodayCnt;
				$('#preloader-for-nopdftodaycnt').hide();
				$('#div-for-nopdftodaycnt').show();
				
				// Aging as one day
				fnFetchRevenueCnt('1','110122',function (strNoPdfOneCnt) {
					$scope.strNoPdfOneCnt = strNoPdfOneCnt;
					$('#preloader-for-nopdfonecnt').hide();
					$('#div-for-nopdfonecnt').show();
					
					// Aging as two days
					fnFetchRevenueCnt('2','110122',function (strNoPdfTwoCnt) {
						$scope.strNoPdfTwoCnt = strNoPdfTwoCnt;
						$('#preloader-for-nopdftwocnt').hide();
						$('#div-for-nopdftwocnt').show();
						
						// Aging as greater than two days
						fnFetchRevenueCnt('3','110122',function (strNoPdfGreaterTwocnt) {
							$scope.strNoPdfGreaterTwocnt = strNoPdfGreaterTwocnt;
							$('#preloader-for-nopdfgreatertwocnt').hide();
							$('#div-for-nopdfgreatertwocnt').show();
				        
							// No pdf total count
							$scope.strNoPdfCnt = parseInt(strNoPdfTodayCnt)
												+parseInt(strNoPdfOneCnt)
												+parseInt(strNoPdfTwoCnt)
												+parseInt(strNoPdfGreaterTwocnt);
							$('#preloader-for-nopdfcnt').hide();
							$('#div-for-nopdfcnt').show();
							
						});
			        
					});
		        
				});
	        
			});
	 };
	 
	 
	// Function used to fetch widget count based on aging & category values
	 function fnFetchRevenueCnt (aging,category,callback) {
		 
		 $scope.gmARRevenueTrackModel = {
				 strCompanyId: $scope.strCompanyId,
				 strAging    : aging,
				 strCategory : category,
				 strCompZone: $scope.strCompanyZone,
	             strPlantId: $scope.strPlantId,
	             strDtFormat:  $scope.strDtFormat
		 };
		 
		 $http.post(url+'fetchRevenueTrackCnt/',angular.toJson($scope.gmARRevenueTrackModel)).then(function(response){
	            if (response.data != undefined) {
	                return callback(response.data);
	            } else{
	            	return callback(0);
	            }
	        });
		 
	 };
	 
	 
	 /*Web Service Call for Field Sales details  based on login user*/
	    $scope.fnSearchFieldSales = function () {
	        var strSearchValue = $scope.strFieldSalesId;
	        if (strSearchValue.length >= 3 ) {
	        	$scope.hidethisfield = false;
	            var strScreenRedisKey = 'distRedis';
	            $http.get(microappUrl+'loadRedisDtls'+'/' + strScreenRedisKey + '/' + strSearchValue + '/' + '10' + '/' + $scope.strCompanyId).then(function (response) {
	            	if (response.data != "") {
	                    $scope.fieldSalesList = response.data;
	                    $("#list-group-field").addClass("searchResults");
	                }else{
	                    $scope.fieldSalesList = [{"ID":"","NAME":"No Data Found"}];
	                }
	            });

	        }else{
	        	$scope.hidethisfield = true;
	            $("#list-group-field").removeClass("searchResults");
	        }
	    };
	    
	    
	    /*Web Service Call for Sales Rep details  based on login user*/
	    $scope.fnSearchSalesRep = function () {
	        var strSearchValue = $scope.strSalesRepId;

	        if (strSearchValue.length >= 3 ) {
	        	$scope.hidethissales = false;
	            var strScreenRedisKey = 'salesRepRedis';
	            $http.get(microappUrl+'loadRedisDtls'+'/' + strScreenRedisKey + '/' + strSearchValue + '/' + '10' + '/' + $scope.strCompanyId).then(function (response) {
	                if (response.data != "") {
	                    $scope.salesRepList = response.data;
	                    $("#list-group-sales").addClass("searchResults");
	                }else{
	                    $scope.salesRepList = [{"ID":"","NAME":"No Data Found"}];
	                }
	            });

	        }else{
	        	$scope.hidethissales = true;
	            $("#list-group-sales").removeClass("searchResults");
	        }
	    };

	 
	//onchange company dropdown to get company details
	    $scope.fnchangeCompany = function (strCompId) {
	        $scope.strCompanyId = strCompId;
	        
	        $http.get(microappUrl + 'loadCompanyInfo/' + strCompId).then(function (response) {
	            if (response.data != undefined) {
	                $scope.compInfoList = response.data;
	                $scope.strCurrency = $scope.compInfoList[0].currency;
	                $scope.strcurrencyNm = $scope.compInfoList[0].currencyNm;
	                $scope.strDtFormat = $scope.compInfoList[0].dtFormat;
	                $scope.strCmpCurrFmt = $scope.compInfoList[0].cmpCurrFmt;
	                $scope.strCompanyId = $scope.compInfoList[0].companyId;
	                $scope.strCompanyZone = $scope.compInfoList[0].compZone;
	                $scope.strLanguageId = $scope.compInfoList[0].languageId;
	                $scope.strCompanyLocale = $scope.compInfoList[0].companyLocale;
	             
	                // Calling dashboard  widget 
                    $scope.fnFetchDashboardWidgets();
                    $scope.fnSetDates($scope.strDtFormat);
                    $scope.fnClearAllField();
                    
                    /* This function used to get the dropdown values for state DropDown */    
                    fnCodeLookUpValuesExclude($http,$scope,'STATE','',strCompId,function(response){
                         $scope.statelist = response.data;
                    });
	            }
	        });
	    };
	    
	    // Function used to fetch iPad DO pdf file in popup 
	    $scope.fnDOPdfViewer = function (strOrderId) {
	    	 var strCompanyLocale =  $scope.strCompanyLocale;
	    	 $('#pdfDrillDownModal').modal();
	    	 $('#preloader-pdfdrildown').show();
	    	var dourl = microappUrlFile+'downloadFile/ipad-do-download/'+strCompanyLocale+'/'+strOrderId+'.pdf';
	    	 $('#do-pdf-download').attr("href", dourl);
	    
	    	 var myCanavs = document.getElementById("holder");
	    	 var url = microappUrlFile+'fileViewer/ipad-do-download/'+strCompanyLocale+'/'+strOrderId+'.pdf';
	    	 myCanavs.innerHTML = '';
	    	 fnPdfViewer('#pdfDrillDownModal','#holder',url,'#preloader-pdfdrildown',myCanavs);
	    	 
	         $('.dismiss').click();
	    }
	    
		    
	    
   /*toggle button class add and remove*/
    $scope.fnColapseFilter = function () {
        if ($('#collapseFilter').hasClass('collapse show')) {
            $('#collapseFilter').removeClass('show');
        } else {
            $('#collapseFilter').addClass('collapse show');
        }

        if ($('#btnCollapseFilter').hasClass('btn btn-xs btn-outline-dark collapsed')) {
            $('#btnCollapseFilter').removeClass('collapsed');
        } else {
            $('#btnCollapseFilter').addClass('btn btn-xs btn-outline-dark collapsed');
        }

    };

 

/*Web Service Call for Rep Account details  based on login user*/
    $scope.fnSearchRepAccount = function () {
        var strSearchValue = $scope.strRepAccId.toLowerCase();
        if (strSearchValue.length >= 3) {
            $scope.hidethisrep = false;
            var strScreenRedisKey = 'repAccountRedis';
            $http.get(microappUrl + 'loadRedisDtls' + '/' + strScreenRedisKey + '/' + strSearchValue + '/' + '100' + '/' + $scope.strCompanyId).then(function (response) {
                if (response.data != "") {
                    $scope.repAccList = response.data;
                    $("#list-group-rep").addClass("searchResults");
                } else {
                    $scope.repAccList = [{
                        "ID": "",
                        "NAME": "No Data Found"
                    }];
                }
            });

        } else {
            $scope.hidethisrep = true;
            $("#list-group-rep").removeClass("searchResults");
        }
    };


/* Set id for Rep account search auto search */
    $scope.fnRepAccItem = function ($event) {
        $scope.strRepAcc = angular.element($event.target).attr("data-id");
        $scope.strRepAccId = angular.element($event.target).attr("data-name");
        $scope.hidethisrep = true;

    };




// Function to fetch revenue report from dashboard count
 $scope.fnRevenueReport = function(aging,category){
	/*Web service call for revenue data report */
	         //$scope.strCategoryId = category;
	        // $scope.strAgingId = "";
             $scope.selectedState ="";
             $scope.selectedAging=aging;
             $scope.selectedCategory=category;
			 $scope.gmARRevenueTrackModel = {
						 strCompanyId: $scope.strCompanyId,
						 strAging    : aging, 
						 strCategory : category,
						 strCompZone: $scope.strCompanyZone,
			             strPlantId: $scope.strPlantId,
			             strDtFormat:  $scope.strDtFormat
				 };
			    $('.overlldiv').hide();
                $('.reportdiv').show();
                $("#preloader-spin").show();
                $('#no-data-found').hide();
				 $http.post(url+'fetchRevenueTrackRpt/',angular.toJson($scope.gmARRevenueTrackModel)).then(function(response){
			            if (response.data != undefined) {
							if(response.data.length>0){
							  $scope.revenueTrackReportLoad(response.data);
					           $("#btn-approve-div").show();
					           $('#excel-export-div').show();
							}else{
								$('#no-data-found').show();
							}
							 $("#preloader-spin").hide();
			            }
			        });
	
};


/*This is used for set the date picker details */
    $scope.fnSetDates = function (strDateFormat) {
        var strDtFormat = (strDateFormat).toLowerCase();
        
        
        $(".inputDatePicker").datepicker('destroy');
        $('.inputDatePicker').attr("placeholder", strDtFormat);
        $('.inputDatePicker').datepicker({
            format: strDtFormat,
	    todayHighlight: true,
            autoclose: true,
        });
        
        /*Calendar icon click focus input fields*/
        $('.orderFDate').click(function () {
            $("#orderFDate").focus();
        });

        $('.orderTDate').click(function () {
            $("#orderTDate").focus();
        });

    };


    
    
  //Clear redis dropdown id 
    $scope.fnClearRedisId = function () {
   	 var strRepAccNm = $scope.strRepAccId;
   	 var strFieldSalesNm = $scope.strFieldSalesId;
   	 var strSalesRepNm = $scope.strSalesRepId;
   	 
   	 if(strRepAccNm == '' ){
   		 $scope.strRepAcc = ""; 
   	 }
   	 if(strFieldSalesNm == '' ){
   		 $scope.strFieldSales = "";
   	 }
   	 if(strSalesRepNm == '' ){
   		 $scope.strSalesRep = "";
   	 }
   	 
    };
    
    $scope.fnValidateFOrdDate = function () {
    	$("#span-empty_ordFdt-err").hide();
    };
    $scope.fnValidateTOrdDate = function () {
		$("#span-empty_ordTdt-err").hide();
		$("#span-dt_compare_ordFdt-err").hide();
    };
    
    
/*Function to fetch report click on load button */
$scope.fnRevenueReportModel= function(){
	
	 var varOrdFromDate = "";
     var varOrdToDate = "";
     var varOrderValidateFl = false;
     var varValidateMsg = "";
     var varOrdDtErrFl = false;

     var varOrderFromDtErrFl = false;
     var varOrderToDtErrFl = false;

     varOrdFromDate = $scope.strOrderFDate;
     varOrdToDate = $scope.strOrderTDate;
	
	/*Web service call for revenue data report */	
     
	     $("#span-empty_ordFdt-err").hide();
	     $("#span-empty_ordTdt-err").hide();
	     $("#span-dt_compare_surFdt-err").hide();
	     $("#span-dt_compare_ordFdt-err").hide();
	     
	     $scope.fnClearRedisId();
	     if (varOrdFromDate == undefined || varOrdFromDate == null || varOrdFromDate == '') {
	            varOrderFromDtErrFl = true;
	        }
	        //validate To Date of Entry Field and assign result to Flag
	        if (varOrdToDate == undefined || varOrdToDate == null || varOrdToDate == '') {
	            varOrderToDtErrFl = true;
	        }
	        
	        if ((varOrderFromDtErrFl) && (!varOrderToDtErrFl)) {
	            $("#span-empty_ordFdt-err").show();
	            return false;
	        }
	        if ((!varOrderFromDtErrFl) && (varOrderToDtErrFl)) {
	            $("#span-empty_ordTdt-err").show();
	            return false;
	        }
	        
	      //validate Entry Date and assign result to Flag
	        if (varOrdFromDate != null && varOrdFromDate != undefined && varOrdFromDate != '' && varOrdToDate != null && varOrdToDate != undefined && varOrdToDate != '') {
	            varOrderValidateFl = true;
	        }
	        
	        if (varOrderValidateFl) {
	          	
	        	//dateDiff method is used for identify the date difference for all format 
	            dateDiff(varOrdFromDate, varOrdToDate, $scope.strDtFormat,function (response) {
	                if (response != '') {
		                  if (response < 0) {
		                	  varOrdDtErrFl = true;
		                	  $("#span-dt_compare_ordFdt-err").show();
		                	  return false;
		                  }
	                } 
	            });
	            

	        }
	        
	        
	        if(varOrdDtErrFl == false){
	        	$("#preloader-spin").show();
	        	$('#revenu-track-report').hide();
	        	$('#excel-export-div').hide();
		        $('#no-data-found').hide();
		        $("#btn-approve-div").hide();
		        
		        /*toggle more button should collapse while click on load button*/
		        if($('#collapseFilter').hasClass('collapse show')){
		    		$('#collapseFilter').removeClass('show');
		    	}
		        
		        if($('#btnCollapseFilter').hasClass('btn btn-xs btn-outline-dark')){
		        	$('#btnCollapseFilter').addClass('btn btn-xs btn-outline-dark collapsed');
		    	}
	     
		        $scope.gmARRevenueTrackModel = {
				 strCompanyId: $scope.strCompanyId,
				 strAging    : $scope.selectedAging, 
				 strCategory : $scope.selectedCategory,
				 strCompZone: $scope.strCompanyZone,
	             strPlantId: $scope.strPlantId,
	             strDtFormat:  $scope.strDtFormat,
	             strRepAcc:$scope.strRepAcc,
	             strSalesRep:$scope.strSalesRep,
	             strFieldSales:$scope.strFieldSales,
	             strOrdStartDate:$scope.strOrderFDate,
	             strOrdEndDate:$scope.strOrderTDate,
	             strOrderId:	 $scope.strOrderId,
	             strState :  $scope.selectedState,
	             strAccid: $scope.strAccid
		 };
		 
		 $http.post(url+'fetchRevenueTrackRpt/',angular.toJson($scope.gmARRevenueTrackModel)).then(function(response){
	            if (response.data != undefined) {
		           if(response.data.length>0){
			            $scope.revenueTrackReportLoad(response.data);
                        $("#btn-approve-div").show();
                        $('#revenu-track-report').show();
        	        	$('#excel-export-div').show();
		           }else{
		        	   $("#revenu-track-report").hide();
		        	   $('#excel-export-div').hide();
		        	   $("#btn-approve-div").hide();
	                   $('#no-data-found').show();
		           }
	            } 
	            $("#preloader-spin").hide();
	        });
	        }else{
	        	 $("#preloader-spin").hide();
	        }
	       
};


/* Report Data processing and mapping in grid*/
$scope.revenueTrackReportLoad= function(response){
	datacollection = new dhx.DataCollection();
 
	datacollection.parse(response);
  if (grid) {
	    grid.destructor();
	  }
  
 grid = new dhx.Grid("revenu-track-report", {
 columns: [
{ width: 0, id: "revenueTrackId",hidden:true,header: [{ text: ""},{ text: ""}]},
{ width: 50, id: "chkFl", header: [{ text: ""},{ text: "<input id =\"checkAllMaster\" class=\"chckMaster\" checked  type=\"checkbox\"  style='background: #0288d1 !important;'> " }],editable:true, type: "boolean" ,sortable: false
},
{ width: 120, id: "strOrderId", header: [{ text: " Order Id ", align:"center" }, { content: "inputFilter" } ], sortable: true, align:"left"},
{ width: 300, id: "strRepAcc", header: [{ text: " Rep Account ", align:"center" }, { content: "inputFilter" } ], sortable: true, },
{ width: 120, id: "strOrderdate", header: [{ text: " Order Date ", align:"center" }, { content: "inputFilter" } ], sortable: true },
{ width: 140, id: "strState", header: [{ text: " State ", align:"center"  }, { content: "inputFilter" } ], sortable: true },
{ width: 160, id: "strCategory", header: [{ text: " Category ", align:"center"  }, { content: "inputFilter" } ], sortable: true },
{ width: 100, id: "strContractFlag", header: [{ text: " Contract Flag ", align:"center"  }, { content: "inputFilter" }, ],align:"center"},
{ width: 100, id: "strAgingNm", header: [{ text: " Aging ", align:"center"  }, { content: "inputFilter" } ],align:"left"},
{ width: 40, id: "stripad", header: [{ text: "PDF" },{align:"center"} ],
htmlEnable: true,
template: function (text, row, col) { 
	  return row.strCategory.toUpperCase()=="UNRECOGNIZED- NO PDF"?"":"<img  src='/images/pdf_icon.gif' class='pdfviewicon'  border='0' style='cursor:pointer' \">"; 
          },align:"center",editable:true,
 },
 // Updated for PC-2573, to display DO revenue columns before PO revenue columns 
 { width: 360, id: "strDoRevenue", header: [{ text: " DO Revenue by System ", align:"center"  }, { content: "inputFilter" } ]},
{ width: 360, id: "strOverideDo", header: [{ text: " Override DO Revenue ", align:"center"  }, { content: "inputFilter" }], editable:$scope.editFl,type: "string", editorType: "combobox", options: $scope.arrOverrideList },
{ width: 300, id: "strPoRevenue", header: [{ text: " PO Revenue by System ", align:"center"  }, { content: "inputFilter" } ],align:"left"},
{ width: 300, id: "strOveridePo", header: [{ text: " Override PO Revenue ", align:"center"  }, { content: "inputFilter" } ], editable:$scope.editFl,type: "string", editorType: "combobox", options: $scope.arrPoList},
{ width: 250, id: "strComments", header: [{ text: " Comments ", align:"center"  }, { content: "inputFilter" }],editable:$scope.editFl }],
                  keyNavigation: true,
                   resizable: true, 
                   tooltip: true,
                   height:380,
                   selection:"row",
                   data: datacollection,
                   splitAt: 2,
                   rowCss: function (row) { return row.strCustomcss}
				 });
 $scope.grid = grid;	


/*This function used for handling  events*/
const events = [
  "afterEditEnd",
   "headerCellClick",
   "cellClick",
   "beforeEditEnd",
   "beforeEditStart"
];


/*This function for grid event handler*/
function eventHandler(event, arguments) {

		// Cell click event  to open pdf viewer
		if(event == 'cellClick'){
			 if(arguments[1].id=="stripad" && arguments[0].strCategory.toUpperCase()!="UNRECOGNIZED- NO PDF"){
				$scope.fnDOPdfViewer(arguments[0].strOrderId);
			}
		}
		
		// Check all checkbox function
		if(event == 'headerCellClick'){
			 if(arguments[0].id=="chkFl"){
				 
				 $(".chckMaster").change(function() {
				 var chkflval = this.checked;
				 if(grid.data.getLength() == 1){
						 $(".dhx_checkbox__input").prop("checked", chkflval);
				 }else{
					 datacollection.forEach(function(item, index, array) {
						 datacollection.update(item.id, { chkFl: chkflval	});
					 	});
				 }
				 $(".chckMaster").off("change");
				 });
				
			}
		}
		
		// beforeEditEnd event is calling 2 times , before edit and after edit
		if(event == 'beforeEditStart'){
			if(arguments[1].id=="strOverideDo"){
				if(arguments[0].strOverideDo == ''){
					overrideDOBeforeEdit = 'Choose one';
				}else{
					overrideDOBeforeEdit = arguments[0].strOverideDo;
				}
			}else if(arguments[1].id=="strOveridePo"){
				if(arguments[0].strOveridePo == ''){
					overridePOBeforeEdit = 'Choose one';
				}else{
					overridePOBeforeEdit = arguments[0].strOveridePo;
				}
			}else if(arguments[1].id=="strComments"){
					commentsBeforeEdit = arguments[0].strComments;
			}
		}
		
		if(event == 'beforeEditEnd'){
			if(arguments[2].id=="strOverideDo"){
				if(overrideDOBeforeEdit != arguments[0]){
					datacollection.update(arguments[1].id, { strCustomcss: "rowsaveprogress"	});
					if(arguments[0] == 'Choose one'){
						overideVal =  '';
					}else{
						overideVal = $scope.doDtlList.find((a) => a.codeNm == arguments[0]).codeId;
					}
					$scope.fnSaveOverrideDORevenue(arguments[1].id,arguments[1].strOrderId,overideVal);
				}
			}else if(arguments[2].id=="strOveridePo"){
				if(overridePOBeforeEdit != arguments[0]){
					datacollection.update(arguments[1].id, { strCustomcss: "rowsaveprogress"	});
					if(arguments[0] == 'Choose one'){
						overideVal =  '';
					}else{
						overideVal = $scope.poDtlList.find((a) => a.codeNm == arguments[0]).codeId;
					}
					$scope.fnSaveOverridePORevenue(arguments[1].id,arguments[1].strOrderId,overideVal);
				}
			}else if(arguments[2].id=="strComments"){
				if(commentsBeforeEdit != arguments[0]){
					datacollection.update(arguments[1].id, { strCustomcss: "rowsaveprogress"	});
					$scope.fnSaveComments(arguments[1].id,arguments[1].strOrderId,arguments[0]);
				}
			} 
		}

		// event for checkall by line item checkbox
		if(event == 'afterEditEnd'){
			if(arguments[2].id=="chkFl"){
				 var cnt=0;
				 datacollection.forEach(function(item, index, array) {
					 if(item.chkFl){
						 cnt = parseInt(cnt) +1; 
					 }
				 });
				 if(grid.data.getLength() == cnt){
					 $(".chckMaster").prop("checked", true);
				 }else{
					 $(".chckMaster").prop("checked", false);
				 }
			}
		}

			
  }
events.forEach(function (event) {
  grid.events.on(event, function () {
    eventHandler(event, arguments);
  });
});



};    


//Function to save override PO details
$scope.fnSaveOverridePORevenue=function(gridid,ordid,overrideVal){
	/*Web service call for save override PO revenue data*/
	
	 $scope.gmARRevenueTrackModel = {
		         strUserId: localStorage.getItem('strUserId'),
				 strCompanyId: $scope.strCompanyId,
				 strOrderId    : ordid, 
				 strPoRevenue : overrideVal,
			     strCompZone: $scope.strCompanyZone,
	             strPlantId: $scope.strPlantId,
	             strDtFormat:  $scope.strDtFormat
		 };
	$http.post(url+'saveOrverridePORevenueTrack/',angular.toJson($scope.gmARRevenueTrackModel)).then(function(response){
	            if (response.data != undefined) {
		            datacollection.update(gridid, { strCustomcss: ""	});
	            } else{
	            	datacollection.update(gridid, { strCustomcss: "rowsavefailer"});
	            }
	        });
};

//Function to save override DO details
$scope.fnSaveOverrideDORevenue=function(gridid,ordid,overrideVal){
	/*Web service call for save override PO revenue data*/
	
	 $scope.gmARRevenueTrackModel = {
		         strUserId: localStorage.getItem('strUserId'),
				 strCompanyId: $scope.strCompanyId,
				 strOrderId    : ordid, 
				 strDoRevenue : overrideVal,
			     strCompZone: $scope.strCompanyZone,
	             strPlantId: $scope.strPlantId,
	             strDtFormat:  $scope.strDtFormat
		 };
	$http.post(url+'saveOrverrideDORevenueTrack/',angular.toJson($scope.gmARRevenueTrackModel)).then(function(response){
	            if (response.data != undefined) {
                  datacollection.update(gridid, { strCustomcss: ""	});
	            } else{
	            	datacollection.update(gridid, { strCustomcss: "rowsavefailer"});
	            }
	        });
};

//Function to save comments
$scope.fnSaveComments=function(gridid,ordid,comments){
	/*Web service call for save override PO revenue data*/
	 $scope.gmARRevenueTrackModel = {
		         strUserId: localStorage.getItem('strUserId'),
				 strCompanyId: $scope.strCompanyId,
				 strOrderId    : ordid, 
				 strComments : comments,
			     strCompZone: $scope.strCompanyZone,
	             strPlantId: $scope.strPlantId,
	             strDtFormat:  $scope.strDtFormat
		 };
	$http.post(url+'saveDoRevenueTrackComments/',angular.toJson($scope.gmARRevenueTrackModel)).then(function(response){
	            if (response.data != undefined) {
		            datacollection.update(gridid, { strCustomcss: ""}); 
	            } else{
	            	datacollection.update(gridid, { strCustomcss: "rowsavefailer"});
	            }
	        });
};



//Function to approve revenue details
$scope.fnRevenueTrackApprove=function(){
	var approveIds='';
	var errUnrecogIds='';

	datacollection.forEach(function(item, index, array) {
		const obj=datacollection.getItem(item.id);
		if(obj.chkFl == true){
			approveIds = approveIds + obj.revenueTrackId + ',';
			
			if(obj.strCategory.toUpperCase() == 'UNRECOGNIZED' && (obj.strOverideDo == '' || obj.strOverideDo == 'Choose one' )){
				errUnrecogIds = errUnrecogIds + obj.strOrderId + ',';
				datacollection.update(item.id, { strCustomcss: "rowsavefailer"});
			}
		}
 });
	
	$scope.approveIds = approveIds;
	$scope.confirmMsgVal='';
	msg = "Are you sure you want to Approve?";
	
	if(errUnrecogIds != ""){
		msg = "Please select override DO revenue for following orders - ";
		$scope.confirmMsgVal=errUnrecogIds.slice(0, -1);
	}
	
	if(approveIds == ""){
		msg = "Please select atleast one check box to proceed Approve "
	}
	
	$scope.confirmMsg = msg;
	
    $('#conFModalRevenue').modal();
    $('.dismiss').click();
    if(errUnrecogIds != "" || approveIds == ""){
    	$('.modal-footer').hide();
    }else{
    	$('.modal-footer').show();
    }
	 
	
};

// Approve confirm drilldown
$scope.confirmApproveMsg = function () {
	$scope.gmARRevenueTrackModel = {
	         strUserId: localStorage.getItem('strUserId'),
			 strCompanyId: $scope.strCompanyId,
			 strApproveIds    : $scope.approveIds, 
		     strCompZone: $scope.strCompanyZone,
            strPlantId: $scope.strPlantId,
            strDtFormat:  $scope.strDtFormat
	 };
	 	$http.post(url+'approveRevenueTrack/',angular.toJson($scope.gmARRevenueTrackModel)).then(function(response){
           if (response.data != undefined) {
           	$scope.fnRevenueReportModel();
           }
       });
	 	
}


//Set id for Field sales search auto search
$scope.fnFieldSalesItem = function ($event) {
    $scope.strFieldSales = angular.element($event.target).attr("data-id");
    $scope.strFieldSalesId = angular.element($event.target).attr("data-name");
    $scope.hidethisfield = true;    

};

//Set id for sales Rep search auto search
$scope.fnSalesRepItem = function ($event) {
    $scope.strSalesRep = angular.element($event.target).attr("data-id");
    $scope.strSalesRepId = angular.element($event.target).attr("data-name");
    $scope.hidethissales = true;           
};


/*This function for clear the master filter*/
$scope.fnClearAllField= function(){
	$scope.selectedState ="";
    $scope.selectedAging="";
    $scope.selectedCategory="";
    $scope.strOrderId="";
    $scope.strRepAccId="";
    $scope.strOrderFDate="";
    $scope.strOrderTDate="";
    $scope.strSalesRepId="";
    $scope.strFieldSalesId="";
};


//function to export excel
$scope.fnExportXlsx = function () {
	
	// Removing unwanted columns by array index
	grid.config.columns.splice(0, 2);
	
	grid.export.xlsx({
		    name:"Revenue-Report",
		    url: "//export.dhtmlx.com/excel"
		  });
	
	// After export excel, render grid data
	$scope.fnRevenueReportModel();
	
};

	 });