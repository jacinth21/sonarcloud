//This initDHTMLXValut() definition used for initialize dhtmlxVault file drag and drop div element and supports file upload, delete, download process.
/*
 * divRef - Reference Id for div element in HTML 
 * uploadURL - URL to upload file into directory
 * saveInfoURL - URL to save uploaded file details into DB 
 * fetchInfoURL - URL to fetch saved file details from DB
 * doUpdateFlURL - URL for update DO flag on t501 table when file delete
 * viewMode - display files on vault with specific view [list or grid]
 * scaleFactor - to mention the quality of preview image
 * autoSendFl - this flag used to send upload file to server automatically based on flag value[true or false]
 * divWidth - Width of vault
 * divHeight - Height of vault
 * refID - Order Id for file upload
 * refSubtype - DO document upload[26230732]
 * refGroupType - DO[26240412]
 * fileDetArr - uploaded file details used to parse initialization
 * deleteInfoURL - URL for file delete
 * AccessFl - User access flag
 * fileNameArr - Uploaded file names to check for validation
 * strDtFormat - Company date format
 * strCompTimeZone - Company time zone
 * uploadPath - directory to store file on server
 * strUserid - login user id
*/
function initDHTMLXValut(divRef, uploadURL, downloadURL, saveInfoURL, fetchInfoURL, doUpdateFlURL, viewMode, scaleFactor, autoSendFl, divWidth, divHeight, refID, refType, refSubtype, refGroupType, fileDetArr, deleteInfoURL, AccessFl, fileNameArr, strDtFormat, strCompTimeZone, uploadPath, strUserid) {
	$.support.cors = true;
	var fileInfoArr = fileDetArr;

	if (divWidth != "") {
		$("#" + divRef).height(divWidth);
	}
	if (divHeight != "") {
		$("#" + divRef).height(divHeight);
	}

	//initialization of vault to display file drag and drop screen
	var vault = new dhx.Vault(divRef, {
		uploader: {
			target: uploadURL,
			autosend: autoSendFl,
			params: {
				uploadDir: uploadPath,
			}
		},
		downloadURL: downloadURL,
		mode: viewMode,
		scaleFactor: scaleFactor
	});


	if (AccessFl != 'Y') {
		vault.toolbar.data.remove("add");
		vault.toolbar.data.remove("remove-all");
	}

	vault.data.parse(fileDetArr);


	//This events is called when file is added to upload
	vault.events.on("BeforeAdd", function (file) {
		var inputFileName = file.file.name;
		var extension = file.file.name.split(".").pop().toLowerCase();
		var filePredicate = true;
		var size = file.file.size;
		var sizeLimit = '20000000'; // maximum file size limits
		if (AccessFl == 'Y') {
			if (fileNameArr.length > 0) {
				for (var i = 0; i < fileNameArr.length; i++) {
					if (inputFileName == fileNameArr[i]) {
						filePredicate = false;
						break;
					}
				}
				// this condition checks file name flag and shows validation when file name already exist 
				if (!filePredicate) {
					dhx.message({
						text: "File " + inputFileName + " already exist, please upload new file",
						css: "dhx-error",
						expire: 6000
					});

					return filePredicate;
				}

			}
			// this condition checks file extension flag and shows validation when the extension is not allowed
			if (filePredicate) {
				//allowed file extension
				filePredicate = (extension == "png" || extension == "txt" || extension == "docx" || extension == "pdf" || extension == "xlsx" || extension == "xls" || extension == "doc" || extension == "jpg" || extension == "msg" || extension == "jpeg");
				if (!filePredicate) {
					dhx.message({
						text: "Invalid file format, please upload valid file",
						css: "dhx-error",
						expire: 6000
					});
				}
			}
			// this condition checks file size flag and shows validation when the size greater than the limit
			if (filePredicate) {
				filePredicate = size < sizeLimit;
				if (!filePredicate) {
					dhx.message({
						text: "File size is greater than the limit, upload valid file",
						css: "dhx-error",
						expire: 6000
					});
				}
			}
		}
		else {
			filePredicate = false;
		}

		return filePredicate;
	});

	//This events is called when all files are removed from uploaded list
	vault.events.on("removeAll", function () {
		var successFl = false;
		fnFileDelteAllModalConfirm(function (strConfirmFlag) {
			 if (strConfirmFlag) {
				 if (fileInfoArr.length > 0) {
					for (var i = 0; i < fileInfoArr.length; i++) {
						var fileDelInput = {
							strUploadFileListId: fileInfoArr[i].filelistid, // File Upload list Id
							strDateFormat: strDtFormat,// Company date format
							strCompanyZone: strCompTimeZone, // Company time zone	
							strUserId: strUserid, // Login user name
							strDeleteFl: "Y"  // Delete Flag   
						};

						fnDeleteInfo(fileDelInput, deleteInfoURL, function (dt) {
							fileInfoArr = [];
							fileNameArr = [];
							vault.data.parse(fileInfoArr);
						});
					}
				} 
			}
			else {
				vault.data.parse(fileInfoArr);
			} 

		});
		vault.data.parse(fileInfoArr);

	});

	//This events is called when file is removed from uploaded list
	vault.events.on("BeforeRemove", function (file) {
		var filelistId = file.filelistid;
		var filename = file.name;
		var successFl = false;
		var status = file.status;
		if (AccessFl == 'Y') {
			if (status == 'uploaded') {
				  fnFileDelteModalConfirm(function (strConfirmFlag) {
					if (strConfirmFlag) {
						 var fileDelInput = {
							strUploadFileListId: filelistId, // File Upload list Id
							strDateFormat: strDtFormat,// Company date format
							strCompanyZone: strCompTimeZone, // Company time zone
							strUserId: strUserid, // Login user id
							strDeleteFl: "Y"  // Delete Flag   
						};
						fnDeleteInfo(fileDelInput, deleteInfoURL, function (dt) {
							if (fileNameArr.length > 0) {
								for (var i = 0; i < fileNameArr.length; i++) {
									if (fileNameArr[i] == filename) {
										fileNameArr.splice(i, 1);
										break;
									}
								}
							}
							if (fileInfoArr.length > 0) {
								for (var i = 0; i < fileInfoArr.length; i++) {
									if (fileInfoArr[i].name == filename) {
										fileInfoArr.splice(i, 1);
										break;
									}
								}
							}
							vault.data.parse(fileInfoArr);
						});
					}
					else{
						strCallbackCalled = true;
					} 
				});
			}
			else {
				return true;
			}

		}
		return successFl;
	});
	//This events is called when file uploaded completed successfully
	vault.events.on("UploadComplete", function (files) {
		console.log("UploadCompleted >>>");
	});
	//This events is called when file uploaded failed
	vault.events.on("UploadFail", function (file) {
		console.log("Upload failed >>>");
	});

	//This events is called when new file is uploaded to list
	vault.events.on("UploadFile", function (file) {
		var fileid = file.id;
		file.link = file.name;
		var fileName = file.name;
		var fileSize = file.size;
		var fileTitle = fileName.substr(0, fileName.lastIndexOf('.'));
		var fileSavInput = {
			strRefId: refID, // ORder Id
			strUserId: strUserid, // Login user id
			strFileName: fileName, // File name 
			strType: refType, // DO
			strSubType: refSubtype, // DO document upload
			strRefGrp: refGroupType, // DO
			strFileSize: fileSize, // File size
			strFileTitle: fileTitle // File title
		};

		fnSaveFileInfo(fileSavInput, saveInfoURL, function (dt) {
			var doUpdateInput = {
				strRefId: refID, // Order Id
				strUserId: strUserid, // Login user id
				strDeleteFl: 'Y'   // Delete flag value
			}
			// this condition is used to update the DO flag in t501 table if the type is do document
			if (refSubtype == '26230732') { // Do document upload
				fnDoUpdateFlag(doUpdateInput, doUpdateFlURL); // to update do flag in t501 table
			}

			var doFileUploadInput = {
				strRefGrp: refGroupType, // DO
				strRefId: refID, // Order Id
				strRefType: refType, // DO
				strType: "0", // DO document upload
				strFileTitle: "0", // Upload File Title
				strFileName: "0", // Upload File Name
				strPartNumberId: "0", // Part Number Id
				strDateFormat: strDtFormat // Company Date Format
			};
			fnGetUploadedFiles(doFileUploadInput, fetchInfoURL, function (serverFiles,serverFileName) {
				vault.data.parse(serverFiles);
				fileInfoArr = serverFiles; // serverFiles info stored to fileInfoArr to use in remove all event
				fileNameArr = serverFileName; // serverFileName info stored to fileNameArr to use in file name duplicate check in vault
			});

		});

	});
}

/*This method used to delete selected files in uploaded list*/
function fnDeleteInfo(inputdata, fileURL, callback) {
	$.support.cors = true;
	$.ajax({
		async: true,
		url: fileURL,
		method: "POST",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(inputdata),
		success: function (data) {
			callback(data);
		},
		error: function (model, response, errorReport) {
			callback(model.responseText);
		}
	});
}

/* This function used save uploaded files details into database*/
function fnSaveFileInfo(inputdata, fileURL, callback) {
	$.support.cors = true;
	$.ajax({
		async: true,
		url: fileURL,
		method: "POST",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(inputdata),
		success: function (data) {
			callback(data);

		},
		error: function (model, response, errorReport) {
			callback(model);
		}
	});
}


/* This function used save uploaded files details into database*/
function fnDoUpdateFlag(inputdata, fileURL) {
	$.support.cors = true;
	$.ajax({
		async: true,
		url: fileURL,
		method: "POST",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(inputdata),
		success: function (data) {
		},
		error: function (model, response, errorReport) {
			console.log("Model ResponseText: " + model.responseText);
		}
	});
}


/* This function used get uploaded files for selected orderid*/
function fnGetUploadedFiles(inputdata, inputURL, callback) {
	var strArrServerFile = [];
	var strArrFileNames = [];
	var fileSize = "";

	$.ajax({
		async: true,
		url: inputURL,
		method: "POST",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(inputdata),
		success: function (data) {
			var results = data;
			if (results.length > 0) {
				for (var i = 0; i < results.length; i++) {
					var fileObj = results[i];
					if (fnCheckSpecialChracter(fileObj.strFileSizeDtls)) {
						fileSize = fileObj.strFileSizeDtls.substring(0, fileObj.strFileSizeDtls.indexOf('.'));
						fileSize = fileSize.replace(/[ ,.]/g, "");
					}
					fileSize = fileSize * 1024; // file size is multiplied with size 1024 to show file storage units in KB and MB[ex 6KB, 5MB]
					strArrServerFile.push({
						"name": fileObj.strFileName, // Uploaded file name
						"size": fileSize, // Uploaded file size
						"type": "image/jpeg",
						"link": fileObj.strFileName, // Uploaded file download link
						"status": "uploaded", // Status of uploaded file
						"filelistid": fileObj.strUploadFileListId // File list of uploaded file
					});
					strArrFileNames.push(fileObj.strFileName);
				}
			}
			callback(strArrServerFile,strArrFileNames);

		},
		error: function (model, response, errorReport) {
			console.log("Model ResponseText: " + model.responseText);
			strArrServerFile = [];
			strArrFileNames = [];
			callback(strArrServerFile,strArrFileNames);
		}
	});

}

//this callback function used to show modal for file delete confirmation
var fnFileDelteModalConfirm = function (callback) {
	$("#fileDeleteConfirm").modal('show');
	$("#modal-btn-fdelete_confirm").off().on("click", function () {	
		$("#fileDeleteConfirm").modal('hide');
		callback(true);	
	});

	$("#modal-btn-fdelete_cancel").on("click", function () {
		$("#fileDeleteConfirm").modal('hide');	
		callback(false);	
	});
};

//this callback function used to show modal for all file delete confirmation
 var fnFileDelteAllModalConfirm = function (callback) {
	$("#fileDeleteAllConfirm").modal('show');
	$("#modal-btn-falldelete_confirm").off().on("click", function () {
		$("#fileDeleteAllConfirm").modal('hide');	
		callback(true);
	});

	$("#modal-btn-falldelete_cancel").off().on("click", function () {
		$("#fileDeleteAllConfirm").modal('hide');	
	});
}; 

