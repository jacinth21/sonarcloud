/* 
 * GmDiscDetailReportController.js : Controller Class used to fetch and show the A/R Discrepancy Details Report functionalities
 * @author T.S Ramachandiran
 */
app.controller('GmDiscrepancyDtlReportController', function ($scope, $http, $q,$filter) {

    var AfterAuthentication = function () {
        var deferred = $q.defer();
        while (!KeycloakService.keycloak.authenticated) { }
        deferred.resolve(KeycloakService.keycloak.authenticated);
        return deferred.promise;
    }


    $(document).ready(function () {

        $("#preloader-spin").hide();
        $('#no-data-found').hide();
        $('.dhx_sample-controls').hide();

        var varLoginName = localStorage.getItem('strLoginName');
        var strPartyId = "";
        var strUserId = "";
        var strCompId = "";
        $scope.AccessFl = 'N';


        /*Web Service Call for party details from user name*/
        fnLoadUserInfo($http, varLoginName, function (response) {
            if (response.data != undefined) {
                strCompId = response.data[0].strCompanyId;
                strPartyId = response.data[0].strPartyId;
                strUserId = response.data[0].strUserId;
                localStorage.setItem("strPartyId", strPartyId);
                localStorage.setItem("strCompId", strCompId);
                localStorage.setItem("strUserId", strUserId);
                /*Web Service Call for Load Company details  based on login user*/
                fnLoadCompany($http, strPartyId, function (response) {
                    if (response.data != undefined) {
                        $scope.compList = response.data;
                        /*Web Service Call for company information from company id*/
                        fnLoadCompanyInfo($http, strCompId, function (response) {
                            if (response.data != undefined) {
                                $scope.selectedCompany = strCompId;
                                $scope.compInfoList = response.data;
                                $scope.strCompanyZone = $scope.compInfoList[0].compZone;
                                $scope.strDtFormat = $scope.compInfoList[0].dtFormat;
                                $scope.strCurrency = $scope.compInfoList[0].currency;
                                $scope.strcurrencyNm = $scope.compInfoList[0].currencyNm;
                                $scope.strCmpCurrFmt = $scope.compInfoList[0].currencyFormat;
                                $scope.strCompanyId = $scope.compInfoList[0].companyId;
                                $scope.strLanguageId = $scope.compInfoList[0].languageId;
                                $scope.strCompanyLocale = $scope.compInfoList[0].companyLocale;
                                $scope.strPlantId = $scope.compInfoList[0].plantId;
                                localStorage.setItem("strLangId", $scope.strLanguageId);
                                localStorage.setItem("strPlantId", $scope.strPlantId);
                                $scope.fnSetDates($scope.strDtFormat);
                            }
                        });
                    } else {
                        $scope.errorMsg = response.data.message;
                        sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                        $scope.errorHandler();
                    }
                });

            } else {
                $scope.errorMsg = response.data.message;
                sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                $scope.errorHandler();
            }
        });


        /* This function used to get the dropdown values for Collector*/
        fnCodeLookUpValues($http, $scope, 'COLLTR', function (podropres) {
            $scope.collectorList = podropres.data;
        });

        /* This function used to get the dropdown values for Resolution*/
        fnCodeLookUpValues($http, $scope, 'DOPORE', function (podropres) {
            $scope.DOPORE = podropres.data;
        });

        /* This function used to get the dropdown values for Category*/
        fnCodeLookUpValues($http, $scope, 'DOPOCA', function (podropres) {
            $scope.DOPOCA = podropres.data;

        });

        /* This function used to get the dropdown values for Resolution Status*/
        fnCodeLookUpValues($http, $scope, 'DISSTS', function (podropres) {
            $scope.DISSTS = podropres.data;

        });

        /*Web Service Call for Load Division details*/
        fnLoadDivision($http, function (response) {
            if (response.data.status == undefined) {
                $scope.divisionList = response.data;
            } else {
                $scope.errorMsg = response.data.message;
                sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                $scope.errorHandler();
            }
        });

    });


    /*toggle button class add and remove*/
    $scope.fnColapseFilter = function () {
        if ($('#collapseFilter').hasClass('collapse show')) {
            $('#collapseFilter').removeClass('show');
        } else {
            $('#collapseFilter').addClass('collapse show');
        }

        if ($('#btnCollapseFilter').hasClass('btn btn-xs btn-outline-dark collapsed')) {
            $('#btnCollapseFilter').removeClass('collapsed');
        } else {
            $('#btnCollapseFilter').addClass('btn btn-xs btn-outline-dark collapsed');
        }

    };


    /* Redis
        * Web Service Call for Parent Account details  based on selected Company in company dropdown*/
    $scope.fnSearchParentAccount = function () {
        var strSearchValue = $scope.strParentAccId.toLowerCase();
        if (strSearchValue.length >= 3) {
            $scope.hidethisacc = false;
            var strScreenRedisKey = 'parentAccountRedis';
            $http.get(microappUrl + 'loadRedisDtls' + '/' + strScreenRedisKey + '/' + strSearchValue + '/' + '100' + '/' + $scope.strCompanyId).then(function (response) {
                if (response.data != "") {
                    $scope.parentAccList = response.data;
                    $("#list-group-acc").addClass("searchResults");
                } else {
                    $scope.parentAccList = [{
                        "ID": "",
                        "NAME": "No Data Found"
                    }];
                }
            });
        } else {
            $scope.hidethisacc = true;
            $("#list-group-acc").removeClass("searchResults");
        }
    };
    /* Redis
     * Web Service Call for Rep Account details  based on selected Company in company dropdown*/
    $scope.fnSearchRepAccount = function () {
        var strSearchValue = $scope.strRepAccId.toLowerCase();
        if (strSearchValue.length >= 3) {
            $scope.hidethisrep = false;
            var strScreenRedisKey = 'repAccountRedis';
            $http.get(microappUrl + 'loadRedisDtls' + '/' + strScreenRedisKey + '/' + strSearchValue + '/' + '100' + '/' + $scope.strCompanyId).then(function (response) {
                if (response.data != "") {
                    $scope.repAccList = response.data;
                    $("#list-group-rep").addClass("searchResults");
                } else {
                    $scope.repAccList = [{
                        "ID": "",
                        "NAME": "No Data Found"
                    }];
                }
            });

        } else {
            $scope.hidethisrep = true;
            $("#list-group-rep").removeClass("searchResults");
        }
    };
    /* Redis
     * Web Service Call for Field Sales details  based on selected Company in company dropdown*/
    $scope.fnSearchFieldSales = function () {
        var strSearchValue = $scope.strFieldSalesId.toLowerCase();
        if (strSearchValue.length >= 3) {
            $scope.hidethisfield = false;
            var strScreenRedisKey = 'distRedis';
            $http.get(microappUrl + 'loadRedisDtls' + '/' + strScreenRedisKey + '/' + strSearchValue + '/' + '100' + '/' + $scope.strCompanyId).then(function (response) {
                if (response.data != "") {
                    $scope.fieldSalesList = response.data;
                    $("#list-group-field").addClass("searchResults");
                } else {
                    $scope.fieldSalesList = [{
                        "ID": "",
                        "NAME": "No Data Found"
                    }];
                }
            });

        } else {
            $scope.hidethisfield = true;
            $("#list-group-field").removeClass("searchResults");
        }
    };
    /* Redis
     * Web Service Call for Sales Rep details  based on selected Company in company dropdown*/
    $scope.fnSearchSalesRep = function () {
        var strSearchValue = $scope.strSalesRepId.toLowerCase();
        if (strSearchValue.length >= 3) {
            $scope.hidethissales = false;
            var strScreenRedisKey = 'salesRepRedis';
            $http.get(microappUrl + 'loadRedisDtls' + '/' + strScreenRedisKey + '/' + strSearchValue + '/' + '100' + '/' + $scope.strCompanyId).then(function (response) {
                if (response.data != "") {
                    $scope.salesRepList = response.data;
                    $("#list-group-sales").addClass("searchResults");
                } else {
                    $scope.salesRepList = [{
                        "ID": "",
                        "NAME": "No Data Found"
                    }];
                }
            });

        } else {
            $scope.hidethissales = true;
            $("#list-group-sales").removeClass("searchResults");
        }
    };

    //Set id for Parent account search auto search
    $scope.fnParentAccItem = function ($event) {
        $scope.strParentAcc = angular.element($event.target).attr("data-id");
        $scope.strParentAccId = angular.element($event.target).attr("data-name");
        $scope.hidethisacc = true;

    };

    //Set id for Rep account search auto search
    $scope.fnRepAccItem = function ($event) {
        $scope.strRepAcc = angular.element($event.target).attr("data-id");
        $scope.strRepAccId = angular.element($event.target).attr("data-name");
        $scope.hidethisrep = true;

    };

    //Set id for Field sales search auto search
    $scope.fnFieldSalesItem = function ($event) {
        $scope.strFieldSales = angular.element($event.target).attr("data-id");
        $scope.strFieldSalesId = angular.element($event.target).attr("data-name");
        $scope.hidethisfield = true;

    };

    //Set id for sales Rep search auto search
    $scope.fnSalesRepItem = function ($event) {
        $scope.strSalesRep = angular.element($event.target).attr("data-id");
        $scope.strSalesRepId = angular.element($event.target).attr("data-name");
        $scope.hidethissales = true;
    };



    /*This is used for set the date picker details */
    $scope.fnSetDates = function (strDateFormat) {
        var strDtFormat = (strDateFormat).toLowerCase();

        var today = new Date();


        $(".inputDatePicker").datepicker('destroy');
        $('.inputDatePicker').attr("placeholder", strDtFormat);
        $('.inputDatePicker').datepicker({
            format: strDtFormat,
            todayHighlight: true,
            autoclose: true,
        });


        $("#discrepancyFDate").datepicker();
        $("#discrepancyTDate").datepicker();
        $("#resolutionFDate").datepicker();
        $("#resolutionTDate").datepicker();

        /*Calendar icon click focus input fields*/
        $('.discrepancyFDate').click(function () {
            $("#discrepancyFDate").focus();
            $scope.fnValidateFDisDate();
        });

        $('.discrepancyTDate').click(function () {
            $("#discrepancyTDate").focus();
            $scope.fnValidateTDisDate();
        });

        $('.resolutionFDate').click(function () {
            $("#resolutionFDate").focus();
            $scope.fnValidateFResDate();
        });

        $('.resolutionTDate').click(function () {
            $("#resolutionTDate").focus();
            $scope.fnValidateTResDate();
        });

    };


    //Function is used to validate Discrepency From date 
    $scope.fnValidateFDisDate = function () {
        $("#span-empty_disFdt-err").hide();
    };
    //Function is used to validate Discrepency To date 
    $scope.fnValidateTDisDate = function () {
        $("#span-empty_disTdt-err").hide();
        $("#span-dt_compare_disFdt-err").hide();
    };
    //Function is used to validate Resolution From date 
    $scope.fnValidateFResDate = function () {
        $("#span-empty_resFdt-err").hide();
    };
    //Function is used to validate resolution To date 
    $scope.fnValidateTResDate = function () {
        $("#span-empty_resTdt-err").hide();
        $("#span-dt_compare_resFdt-err").hide();
    };


    /*Function is used to load the Discrepency Detail report*/
    $scope.fnLoadDisrepancyDtlReport = function () {

        var disFromDate = "";
        var disToDate = "";
        var resFromDate = "";
        var resToDate = "";
        var ResolutionValidateFl = false;
        var DiscrepancyValidateFl = false;
        var DisDtErrFl = false;
        var ResDtErrFl = false;


        var DiscrepancyFromDtErrFl = false;
        var DiscrepancyToDtErrFl = false;
        var ResolutionFromDtErrFl = false;
        var ResolutionToDtErrFl = false;

        disFromDate = $scope.strDisStartDate;
        disToDate = $scope.strDisEndDate;
        resFromDate = $scope.strResStartDate;
        resToDate = $scope.strResEndDate;

        $("#span-empty_disFdt-err").hide();
        $("#span-empty_disTdt-err").hide();
        $("#span-empty_resFdt-err").hide();
        $("#span-empty_resTdt-err").hide();
        $("#span-dt_compare_disFdt-err").hide();
        $("#span-dt_compare_resFdt-err").hide();


        //validate From Date of Discrepancy Field and assign result to Flag
        if (!parseNull(disFromDate)) {

            DiscrepancyFromDtErrFl = true;
        }
        //validate To Date of Discrepancy Field and assign result to Flag
        if (!parseNull(disToDate)) {

            DiscrepancyToDtErrFl = true;
        }
        //validate From Date of Resolution Field and assign result to Flag
        if (!parseNull(resFromDate)) {
            ResolutionFromDtErrFl = true;
        }
        //validate To Date of Resolution Field and assign result to Flag
        if (!parseNull(resToDate)) {
            ResolutionToDtErrFl = true;
        }


        //Check each date field and get validation message for missing input field
        if ((DiscrepancyFromDtErrFl) && (!DiscrepancyToDtErrFl)) {
            $("#span-empty_disFdt-err").show();
            return false;
        }
        if ((!DiscrepancyFromDtErrFl) && (DiscrepancyToDtErrFl)) {
            $("#span-empty_disTdt-err").show();
            return false;
        }
        if ((ResolutionFromDtErrFl) && (!ResolutionToDtErrFl)) {
            $("#span-empty_resFdt-err").show();
            return false;
        }
        if ((!ResolutionFromDtErrFl) && (ResolutionToDtErrFl)) {
            $("#span-empty_resTdt-err").show();
            return false;
        }



        //validate Discrepancy Date and assign result to Flag
        if (parseNull(disFromDate) && parseNull(disToDate)) {
            DiscrepancyValidateFl = true;
        }

        //validate Resolution Date and assign result to Flag
        if (parseNull(resFromDate) && parseNull(resToDate)) {
            ResolutionValidateFl = true;
        }

        //This condition will execute if Validation Discrepancy flag is true
        if (DiscrepancyValidateFl) {
            //dateDiff method is used for identify the date difference for all format 
            dateDiff(disFromDate, disToDate, $scope.strDtFormat, function (response) {
                if (response != '') {
                    if (response < 0) {
                        DisDtErrFl = true;
                        $("#span-dt_compare_disFdt-err").show();
                        return false;
                    }
                }
            });

        }

        //This condition will execute if Validation Entry flag is true
        if (ResolutionValidateFl) {

            //dateDiff method is used for identify the date difference for all format 
            dateDiff(resFromDate, resToDate, $scope.strDtFormat, function (response) {
                if (response != '') {
                    if (response < 0) {
                        ResDtErrFl = true;
                        $("#span-dt_compare_resFdt-err").show();
                        return false;
                    }
                }
            });


        }

        //Load the Discrepency PO reports
        if ((DisDtErrFl == false) && (ResDtErrFl == false)) {
            $("#discrepency-detail-report").html("<div class='w-100 text-center' id='preloader-rpt'><i class='fas fa-spinner fa-pulse fa-2x'></i></div>");
            $('#discrepency-detail-report').show();
            $('#no-data-found').hide();

            /*toggle more button should collapse while click on load button*/
            if ($('#collapseFilter').hasClass('collapse show')) {
                $('#collapseFilter').removeClass('show');
            }

            if ($('#btnCollapseFilter').hasClass('btn btn-xs btn-outline-dark')) {
                $('#btnCollapseFilter').addClass('btn btn-xs btn-outline-dark collapsed');
            }

            var strPONumber = $scope.strCustPONumber;

            if (strPONumber != "" && strPONumber != undefined) {
                var lastChar = strPONumber.slice(-1);
                if (lastChar == ',') {
                    $scope.strCustPONumber = strPONumber.slice(0, -1);
                }
            }

            $scope.gmARDiscrepencyModel = {
                strCompanyId: $scope.strCompanyId,
                strParentAcc: $scope.strParentAcc,
                strRepAcc: $scope.strRepAcc,
                strFieldSales: $scope.strFieldSales,
                strSalesRep: $scope.strSalesRep,
                strDivisionId: $scope.strDivisionId,
                strCustPONumber: $scope.strCustPONumber,
                strDateType: $scope.strDateType,
                strDisStartDate: $scope.strDisStartDate,
                strDisEndDate: $scope.strDisEndDate,
                strOrderId: $scope.strOrdId,
                strResStartDate: $scope.strResStartDate,
                strResEndDate: $scope.strResEndDate,
                strAccid: $scope.strAccid,
                strCurrency: $scope.strCurrency,
                strCmpCurrFmt: $scope.strCmpCurrFmt,
                strDtFormat: $scope.strDtFormat,
                strLanguageId: $scope.strLanguageId,
                strCollectorId: $scope.strCollectorId,
                strCategoryId: $scope.strCategoryId,
                strResolutionId: $scope.strResolutionId,
                strStatusId: $scope.strStatusId,
                strCompZone: $scope.strCompanyZone
            };

            $http.post(url + 'loadDiscrepancyDtlReport/', angular.toJson($scope.gmARDiscrepencyModel)).then(function (response) {

                if (response.data != undefined) {
                    var results = response.data;
                    $scope.fnLoadDiscrepncyDtlReportGrid(results);
                } else {
                    $scope.errorMsg = response.data.message;
                    sessionStorage.setItem("errorMsg", ($scope.errorMsg));
                    $scope.errorHandler();
                }

            });


        }
    };


    // this is to load the Discrepency Details grid
    $scope.fnLoadDiscrepncyDtlReportGrid = function (response) {

        if (response != '') {
            console.log(response);
            $('#discrepency-detail-report').empty();
            // Updating new dhtmlx Grid - 6.5.2
            datacollection = new dhx.DataCollection();
            datacollection.parse(response);
            grid = new dhx.Grid("discrepency-detail-report", {
                columns: [
                    {width: 150,id: "strPONumber",header: [{text: "PO Number",align: "center"}, {
                            content: "inputFilter"}], align: "left"},
                    {width: 100,id: "strCategoryNm",header: [{text: "Category",align: "center"}, {
                            content: "selectFilter"}],align: "left"},
                    {width: 100,id: "strCollectorNm",header: [{text: "Collector",align: "center"}, {
                            content: "selectFilter"}],align: "left"},
                    {width: 150,id: "strOrderId",header: [{text: "DO ID",align: "center"}, {
                            content: "inputFilter"}],align: "center",htmlEnable: true,template: function (text, row, col) {
                            return "<a id='strOrdId' data-orderid='" + text + "' href='javascript:;'><span class='po-update-btn'>" + text + "</span></a></div>";
                        }, align: "left"},
                    {width: 120,id: "strDOPartnum",header: [{text: "DO Part",align: "center" }, {content: "inputFilter"}],align: "left"},
                    {width: 250,id: "strPartDesc",header: [{text: "Part Description",align: "center"}, {
                            content: "inputFilter"}],align: "left"},
                    {width: 60,id: "strDOPartQty",header: [{text: "DO Qty",align: "center"}, {
                            content: "inputFilter"}],align: "right",footer: [{text: '<div class="custom_footer">Total</div>'}]},
                    {width: 120,id: "strDOItemPrice",header: [{text: "DO Amount",align: "center"}, {
                            content: "inputFilter"}],type: "number",align: "right",template: function (text, row, col) {if (text == 0) {
                                return "0.00"
                            } else if (text < 0) {
                               row.strDOItemPrice =  row.strDOItemPrice.slice(1);
                                if (row.strDOItemPrice % 1 != 0) {
                                    row.strDOItemPrice = parseFloat(row.strDOItemPrice + '0');
                                    return (row.strDOItemPrice).toFixed(2);
                                } else {
                                    return row.strDOItemPrice + ".00";
                                }
                            } else {
                               if (row.strDOItemPrice % 1 != 0) {
                                    row.strDOItemPrice = parseFloat(row.strDOItemPrice + '0');
                                    return (row.strDOItemPrice).toFixed(2);
                                } else {
                                    return row.strDOItemPrice + ".00";
                                }
                            } }, mark: function (cell, data, row, col) { // add class for cell (Minimum Amount Css)
                            if (row.strDOItemPrice < 0)
                                return "amt-min";
                        },footer: [{content: "sum"}] },
                    {width: 120,id: "strExtDoAmt",header: [{text: "Ext DO Amount",align: "center"}, {
                            content: "inputFilter"}],type: "number",align: "right",template: function (text, row, col) {if (text == 0) {
                                return "0.00"
                            } else if (text < 0) {
                               row.strExtDoAmt =  row.strExtDoAmt.slice(1);
                                if (row.strExtDoAmt % 1 != 0) {
                                    row.strExtDoAmt = parseFloat(row.strExtDoAmt + '0');
                                    return (row.strExtDoAmt).toFixed(2);
                                } else {
                                    return row.strExtDoAmt + ".00";
                                }
                            } else {
                               if (row.strExtDoAmt % 1 != 0) {
                                    row.strExtDoAmt = parseFloat(row.strExtDoAmt + '0');
                                    return (row.strExtDoAmt).toFixed(2);
                                } else {
                                    return row.strExtDoAmt + ".00";
                                }
                            } },mark: function (cell, data, row, col) { // add class for cell (Minimum Amount Css)
                            if (row.strExtDoAmt < 0)
                                return "amt-min";
                        }, footer: [{content: "sum" }] },
                    {width: 120,id: "strPOPartNum",header: [{text: "PO Part",align: "center" }, {
                            content: "inputFilter"}], align: "left"},
                    {width: 60,id: "strPOPartQty", header: [{text: "PO Qty",align: "center" }, {
                            content: "inputFilter"}],align: "right",footer: [{text: '<div class="custom_footer">Total</div>'}]},
                    {width: 120,id: "strPOItemPrice",header: [{text: "PO Price",align: "center"}, {
                            content: "inputFilter"}],type: "number",align: "right",template: function (text, row, col) {if (text == 0) {
                                return "0.00"
                            } else if (text < 0) {
                                row.strPOItemPrice =  row.strPOItemPrice.slice(1);
                                if (row.strPOItemPrice % 1 != 0) {
                                    row.strPOItemPrice = parseFloat(row.strPOItemPrice + '0');
                                    return (row.strPOItemPrice).toFixed(2);
                                } else {
                                    return row.strPOItemPrice + ".00";
                                }
                            } else {
                               if (row.strPOItemPrice % 1 != 0) {
                                    row.strPOItemPrice = parseFloat(row.strPOItemPrice + '0');
                                    return (row.strPOItemPrice).toFixed(2);
                                } else {
                                    return row.strPOItemPrice + ".00";
                                }
                            }},mark: function (cell, data, row, col) { //add class for cell (Minimum Amount Css)
                            if (row.strPOItemPrice < 0)
                                return "amt-min";
                        },footer: [{content: "sum" }]},
                    {width: 120,id: "strExtPoAmt",header: [{text: "Ext PO Amount",align: "center"}, {
                            content: "inputFilter"}],type: "number",align: "right",template: function (text, row, col) {if (text == 0) {
                                return "0.00"
                            } else if (text < 0) {
                                row.strExtPoAmt =  row.strExtPoAmt.slice(1);
                                if (row.strExtPoAmt % 1 != 0) {
                                    row.strExtPoAmt = parseFloat(row.strExtPoAmt + '0');
                                    return (row.strExtPoAmt).toFixed(2);
                                } else {
                                    return row.strExtPoAmt + ".00";
                                }
                            } else {
                                if (row.strExtPoAmt % 1 != 0) {
                                    row.strExtPoAmt = parseFloat(row.strExtPoAmt + '0');
                                    return (row.strExtPoAmt).toFixed(2);
                                } else {
                                    return row.strExtPoAmt + ".00";
                                }
                            }},mark: function (cell, data, row, col) { //  add class for cell (Minimum Amount Css)
                            if (row.strExtPoAmt < 0)
                                return "amt-min";
                        },footer: [{content: "sum"}]},
                    {width: 120,id: "strDiscType",header: [{text: "Discrepancy Type",align: "center"}, {
                            content: "selectFilter"}],align: "left"},
                    {width: 120,id: "strDiscStatus",header: [{text: "Status",align: "center"}, {
                            content: "selectFilter"}],align: "left"},
                    {width: 200,id: "strRepAcc",header: [{text: "Rep Account",align: "center" }, {
                            content: "inputFilter"}],align: "left"},
                    {width: 200,id: "strSalesRep",header: [{text: "Sales Rep",align: "center"}, {
                            content: "inputFilter"}],align: "left"},
                    {width: 120,id: "strResDate",header: [{ text: "Resolution Date",align: "center"}, {
                            content: "selectFilter"}],align: "left"},
                    {width: 120,id: "strDisdate",header: [{text: "Discrepancy Date",align: "center"}, {
                            content: "selectFilter"}],align: "left"}],
                data: response,
                resizable: true,
                margin: 'auto',
                height: 350,
                minWidth: 1350,
                maxWidth: 1500,
                rowHeight: 30,
                dragItem: "column",
                keyNavigation: true,

            });
            $scope.grid = grid;
            $('#preloader-rpt').hide();
            $('.dhx_sample-controls').show();

            /*This function used for handling  events*/
            const events = [
  "afterEditEnd",
   "headerCellClick",
   "cellClick",
   "beforeEditEnd",
   "beforeEditStart"
];
            /*This function for grid event handler*/
            function eventHandler(event, arguments) {

                // Cell click event  to open DO Details
                if (event == 'cellClick') {
                    if (arguments[1].id == "strOrderId") {// while click order id column values open DO details
                        $scope.strOrderId = arguments[0].strOrderId
                        $scope.fnSummaryDO($scope.strOrderId);
                    }
                }

            }
            events.forEach(function (event) {
                grid.events.on(event, function () {
                    eventHandler(event, arguments);
                });
            });
             //Used to show/hide default column
            $('input[class="chk_column"]').each(function () {
                if ($(this).prop("checked") == true) {
                    grid.showColumn($(this).val());

                } else {
                    grid.hideColumn($(this).val());
                }
            });
              //Used to show/hide based on checkbox
            $('.chk_column').click(function(){
                if($(this).prop("checked") == true){
                   grid.showColumn($(this).val());

                }
                else{
                   grid.hideColumn($(this).val());
                }
            });
            

        } else {
            $('#no-data-found').show();
            $('.dhx_sample-controls').hide();
            $('#discrepency-detail-report').hide();
        }

    };
    // This function is used to download excel 
    $scope.exportXlsx = function () {
        grid.export.xlsx({
            name: "Discrepancy-detail-report",
            url: "//export.dhtmlx.com/excel"
        });
    };


    //clear the all field
    $scope.fnClearAllField = function () {
        $scope.strDivisionId = "";
        $scope.strParentAccId = "";
        $scope.strFieldSalesId = "";
        $scope.strSalesRepId = "";
        $scope.strDisStartDate = "";
        $scope.strDisEndDate = "";
        $scope.strResStartDate = "";
        $scope.strResEndDate = "";
        $scope.strCustPONumber = "";
        $scope.strOrdId = "";
        $scope.strAccid = "";
        $scope.strRepAccId = "";
        $scope.strRepAcc = "";
        $scope.strFieldSales = "";
        $scope.strSalesRep = "";
        $scope.strParentAcc = "";
        $scope.strCollectorId = "";
        $scope.strResolutionId = "";
        $scope.strCategoryId = "";
        $scope.strStatusId = "";
        $("#span-empty_disFdt-err").hide();
        $("#span-empty_disTdt-err").hide();
        $("#span-empty_resFdt-err").hide();
        $("#span-empty_resTdt-err").hide();
        $("#span-dt_compare_disFdt-err").hide();
        $("#span-dt_compare_resFdt-err").hide();
        $scope.hidethisacc = true;
        $("#list-group-acc").removeClass("searchResults");
        $scope.hidethisrep = true;
        $("#list-group-rep").removeClass("searchResults");
        $scope.hidethisfield = true;
        $("#list-group-field").removeClass("searchResults");
        $scope.hidethissales = true;
        $("#list-group-sales").removeClass("searchResults");
    };


      //Summary Report DO Function for order id in Descrepency Detail grid
      $scope.fnSummaryDO = function (ordID) {
    	if(ordID.indexOf(',') != -1){
    		ordID = ordID.slice(0, -1);
    	}
         var strPartyId = localStorage.getItem("strPartyId");
        var strCompId = localStorage.getItem("strCompId");
        var strLangId = localStorage.getItem("strLangId");
        var strPlantId = localStorage.getItem("strPlantId");

        var companyInfo = {};
        //form the company info to pass the URL
        companyInfo.cmpid = strCompId;
        companyInfo.plantid = strPlantId;
        companyInfo.token = "";
        companyInfo.partyid = strPartyId;
        companyInfo.cmplangid = strLangId;
        var StringifyCompanyInfo = JSON.stringify(companyInfo);

        var features = 'directories=no,menubar=no,status=no,titlebar=no,toolbar=no,width=800,height=800';
        var url = 'GmEditOrderServlet?hMode=PrintPrice&hOrdId=' + ordID;
        window.open(encodeURI(window.location.protocol + '//' + window.location.hostname + '/' + url + '&companyInfo=' + StringifyCompanyInfo, 'DO Summary', features));
    };


    //Clear redis dropdown id 
    $scope.fnClearRedisId = function () {
        var strParentAccNm = $scope.strParentAccId;
        var strRepAccNm = $scope.strRepAccId;
        var strFieldSalesNm = $scope.strFieldSalesId;
        var strSalesRepNm = $scope.strSalesRepId;

        if (strParentAccNm == '') {
            $scope.strParentAcc = "";
        }
        if (strRepAccNm == '') {
            $scope.strRepAcc = "";
        }
        if (strFieldSalesNm == '') {
            $scope.strFieldSales = "";
        }
        if (strSalesRepNm == '') {
            $scope.strSalesRep = "";
        }

    };


    //onchange company dropdown to get company details
    $scope.fnchangeCompany = function (strCompId) {
        $scope.strCompanyId = strCompId;
        $http.get(microappUrl + 'loadCompanyInfo/' + strCompId).then(function (response) {
            if (response.data != undefined) {
                $scope.compInfoList = response.data;
                $scope.strCurrency = $scope.compInfoList[0].currency;
                $scope.strcurrencyNm = $scope.compInfoList[0].currencyNm;
                $scope.strDtFormat = $scope.compInfoList[0].dtFormat;
                $scope.strCmpCurrFmt = $scope.compInfoList[0].cmpCurrFmt;
                $scope.strCompanyId = $scope.compInfoList[0].companyId;
                $scope.strCompanyZone = $scope.compInfoList[0].compZone;
                $scope.strLanguageId = $scope.compInfoList[0].languageId;
                $scope.strCompanyLocale = $scope.compInfoList[0].companyLocale;
                $scope.strPlantId = $scope.compInfoList[0].plantId;
                localStorage.setItem("strLangId", $scope.strLanguageId);
                localStorage.setItem("strPlantId", $scope.strPlantId);
                localStorage.setItem("strCompId", $scope.strCompanyId);
                $scope.fnSetDates($scope.strDtFormat);
                $scope.fnClearAllField();
                $("#span-empty_disFdt-err").hide();
                $("#span-empty_disTdt-err").hide();
                $("#span-empty_resFdt-err").hide();
                $("#span-empty_resTdt-err").hide();
                $("#span-dt_compare_disFdt-err").hide();
                $("#span-dt_compare_resFdt-err").hide();

            }
        });

    };

});