var split_At = '';
var w1, dhxWins;

//this function is import the js files
function incJavascript(jsname) {
    var th = document.getElementsByTagName('head')[0];
    var s = document.createElement('script');
    s.setAttribute('type', 'text/javascript');
    s.setAttribute('src', jsname);
    th.appendChild(s);
}

//this function is import the css files
function incCss(cssName) {
    var th = document.getElementsByTagName('head')[0];
    var s = document.createElement('link');
    s.setAttribute('type', 'text/css');
    s.setAttribute('rel', 'stylesheet');
    s.setAttribute('href', cssName);
    th.appendChild(s);
}



incCss('/extweb/dhtmlx/dhtmlxGrid/dhtmlxgrid.css');
incCss('/extweb/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css');
incCss('/extweb/dhtmlx/dhtmlxCombo/dhtmlxcombo.css');
incCss('/extweb/dhtmlx/dhtmlxWindows/dhtmlxwindows.css');
incCss('/extweb/dhtmlx/dhtmlxWindows/skins/dhtmlxwindows_dhx_skyblue.css');

// Function for return the Empty value if the value passed as null/undefined/empty.
function parseNull(value) {
    return (value == undefined || value == null || value == "") ? "" : value.trim();
}


// Common Function To Display DTMLX Grid, Following Parameters as Input
// divRef - Id of the element, where the grid has to be displayed
// gridHeight - Height of the Grid, if it is empty then as 500
// gridData - JSON Object, Grid Data
// setHeader - Array, Grid Header Data
// setInitWidths - String, Width of Each Column in the Grid
// setColAlign - String, Alignment data of Each Column in the Grid (right,left,center,justify)
// setColTypes - String, Type of Data in Each Column in the Grid(ro-read only, ed - editable, txt- text, txttxt- plain text etc.. )
// setColSorting - String, Sort based on type of data in Each Column in the Grid(str, int, date, na or function object for custom sorting)
// enableTooltips - String, True/False to enable or disable tooltips
// setFilter - String, filters like #text_filter, #numeric_filter,..
// footerArry - Array, Grid Footer Data
// footerStyles - Array, Footer Data Styles
// footerExportFL - Boolean, true/false to attach footer icon to export as exdel and pdf
// Returns Grid Object gObj

function loadDHTMLXGrid(divRef, gridHeight, gridData, setColIds, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL, roeSplitAt) {
    var gObj = new dhtmlXGridObject(divRef);
    gObj.enableColSpan(true);
    gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
    if (setHeader != undefined && setHeader.length > 0) {
        gObj.setHeader(setHeader);
        var colCount = setHeader.length;
    }
    if (setColIds != "")
        gObj.setColumnIds(setColIds);
    
    var total       = gObj.getColIndexById("strTotal");
    var surgeryDt   = gObj.getColIndexById("strSurgeryDt");
    var ADName      = gObj.getColIndexById("strADName");
    var VPName      = gObj.getColIndexById("strVPName");
    var orderType   = gObj.getColIndexById("strOrdertype");
    var fieldsales  = gObj.getColIndexById("strFieldSales");
    var repAcc      = gObj.getColIndexById("strRepAcc");

    if (setInitWidths != "")
        gObj.setInitWidths(setInitWidths);
    if (setColAlign != "")
        gObj.setColAlign(setColAlign);
    if (setColTypes != "")
        gObj.setColTypes(setColTypes);
    if (setColSorting != "")
        gObj.setColSorting(setColSorting);
    if (enableTooltips != "")
        gObj.enableTooltips(enableTooltips);
    if (setFilter != "")
        gObj.attachHeader(setFilter);
    if (footerArry != undefined && footerArry.length > 0) {
        var footstr = eval(footerArry).join(",");
        if (footerStyles != undefined && footerStyles.length > 0)
            gObj.attachFooter(footstr, footerStyles);
        else
            gObj.attachFooter(footstr);
    }
    if (footerExportFL)
    	fnAttachExptFooter(divRef, colCount, gObj);
    if (gridHeight != "")
        gObj.enableAutoHeight(true, gridHeight, true);
    else
        gObj.enableAutoHeight(true, 500, true);
   
    gObj.setNumberFormat("0,000.00",total);
    gObj.setSkin("dhx_skyblue");
    gObj.init();
    gObj.setColumnHidden(surgeryDt, true);
    gObj.setColumnHidden(ADName, true);
    gObj.setColumnHidden(VPName, true);
    gObj.setColumnHidden(orderType, true);
    gObj.setColumnHidden(fieldsales, true);
    gObj.setColumnHidden(repAcc, true);
    gObj.enableHeaderMenu();


    var newJSONData = JSON.stringify(gridData);
    var dataResult = "{\"data\":" + newJSONData + "}";
    dataResult = JSON.parse(dataResult);
    var gridParseData = dataResult;

    
    gObj.parse(gridParseData, "js");
    return gObj;
}



//To attach Export Grid to excel option to the DHTMLX grid
//ColCount - add the icon in last two column
//gridData - to attach footer with this arg
function fnAttachExptFooter(divRef, colCount, gridData) {
  var footerExport = [];
  for (var j = 0; j < colCount; j++) {
      footerExport[j] = "";
      if (j == colCount - 12)
          footerExport[j] = "<section><span id='" + divRef + "-export' class='export-to-excel'><strong style='vertical-align:top;'>Export Excel</strong><i class='fa fa-download' aria-hidden='true' title='Export as Excel'></i></span></section> ";
      if (j == colCount - 11)
          footerExport[j] = "#cspan";
  }
  gridData.attachFooter(footerExport);
}



//To attach Export Grid to excel option to the DHTMLX grid on Ready for Invoice screen
//ColCount - add the icon in last two column
//gridData - to attach footer with this arg
function fnInvoiceAttachExptFooter(divRef, colCount, gridData) {
    var footerExport = [];
    for (var j = 0; j < colCount; j++) {
        footerExport[j] = "";
        if (j == colCount - 15)
            footerExport[j] = "<section><span id='" + divRef + "-export' class='export-to-excel'><strong style='vertical-align:top;'>Export Excel</strong><i class='fa fa-download' aria-hidden='true' title='Export as Excel'></i></span></section> ";
        if (j == colCount - 14)
            footerExport[j] = "#cspan";
    }
    gridData.attachFooter(footerExport);
  }


//Export DHTMLX Grid as Excel
//gridData - denotes which grid i have to export
//deleteIndexArr - Array, which are the columns are not need to export
function fnExportExcel(e, gridData, deleteIndexArr) {
  if (deleteIndexArr != undefined && deleteIndexArr.length > 0)
      _.each(deleteIndexArr, function (ind) {
          gridData.setColumnHidden(ind, true);
      });
  gridData.toExcel('/phpapp/excel/generate.php');
  //    gridData.toExcel("https://dhtmlxgrid.appspot.com/export/excel");
  if (deleteIndexArr != undefined && deleteIndexArr.length > 0)
      _.each(deleteIndexArr, function (ind) {
          gridData.setColumnHidden(ind, false);
      });
}


//This function is used to sort hyplink columns (String)
function sortLink(a, b, order) {
    a = a.substring(a.indexOf(">") + 1, a.lastIndexOf("<"));
    b = b.substring(b.indexOf(">") + 1, b.lastIndexOf("<"));
    return (a.toLowerCase() > b.toLowerCase() ? 1 : -1) * (order == "asc" ? 1 : -1);
};

//This function is used to sort hyper link column Id as number Ex. <a href="">HC-100</a> (String with Number or Number only)
function sortLinkID(a, b, order) {
    var x = parseInt(a.substring(a.indexOf("-") + 1, a.lastIndexOf("<")));
    var y = parseInt(b.substring(b.indexOf("-") + 1, b.lastIndexOf("<")));
    x = isNaN(x) ? 0 : x;
    y = isNaN(y) ? 0 : y;
    return (x > y ? 1 : -1) * (order == "asc" ? 1 : -1);
};


//Summary Report DO Function
function fnSummaryDO(ordID) {
    var that = this,
        id;
    var URL_Portal = "https://erppreprod.spineit.net/"; //PreProd Portal URL
    var userData = localStorage.getItem("userData");
    userData = JSON.parse(userData);
    var companyInfo = {};
    companyInfo.cmpid = userData.cmpid;
    companyInfo.plantid = userData.plantid;
    companyInfo.token = "";
    companyInfo.partyid = userData.partyid;
    var StringifyCompanyInfo = JSON.stringify(companyInfo);
    var toEncode = "/GmEditOrderServlet?hMode=PrintPrice&hParantForm=SALESDASHBOARD&hOrdId=" + ordID + "&companyInfo=" + StringifyCompanyInfo;
    var URL = URL_Portal + "GmLogonServlet?token=" + localStorage.getItem("token") + "&return_to=" + encodeURIComponent(toEncode) + "&randomid=" + Math.random();
    var summaryWindow = window.open(URL, '_blank', 'location=no,width=950,height=600');
    summaryWindow.addEventListener('loadstop', function () {
        summaryWindow.insertCSS({
            code: "body {font-family: 'Trebuchet MS'!important; -webkit-overflow-scrolling: touch;} a{display: none;} .RightDashBoardHeader {background:#999;} .ShadeRightTableCaption {background:#ccc;} TR.ShadeBlueBk {background:#eee}" + " table { margin:auto!important; } .button-red{ display:none; }"
        });
        summaryWindow.executeScript({
            code: "var meta = document.createElement('meta'); meta.name = 'format-detection'; meta.content = 'telephone=no'; document.getElementsByTagName('head')[0].appendChild(meta); " + that.scriptOptions
        });
    });
    summaryWindow.addEventListener('loaderror', function () {
        showNativeAlert("Error Loading Summary. Please try after sometime", "Error!", "OK", function (index) {
            summaryWindow.close();
        });
    });
}


/*Remove Cookies Values*/
function removeCookie(key) {
    setCookie(key, '', cookieExpiration(-100));
};

/*Calcluate the cookie Expiration Time*/
function cookieExpiration(minutes) {
    var exp = new Date();
    exp.setTime(exp.getTime() + (minutes * 60 * 1000));
    return exp;
};

/*get the cookie value by use Key*/
function getCookie(key) {
    var name = key + '=';
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return '';
};

/*Set the cookie key, value, expirationDate in Minute */
function setCookie(key, value, expirationDate) {
    var cookie = key + '=' + value + '; ' +
        'expires=' + cookieExpiration(expirationDate).toUTCString() + '; ';
    document.cookie = cookie;
};
/*
Function is used load the Grid For ready For invoicing
*/
function loadDHTMLXGridForInvoice(divRef, gridHeight, gridData, setColIds, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL, roeSplitAt) {
    var gObj = new dhtmlXGridObject(divRef);
    gObj.enableColSpan(true);
    gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
    if (setHeader != undefined && setHeader.length > 0) {
        gObj.setHeader(setHeader);
        var colCount = setHeader.length;

    }
    if (setColIds != "")
        gObj.setColumnIds(setColIds);
    var total    = gObj.getColIndexById("strTotal");
    var poamount = gObj.getColIndexById("strPOAmt");
    var diffamount = gObj.getColIndexById("strDiffAmt");
    var FieldSales = gObj.getColIndexById("strFieldSales");
    var repAccNm = gObj.getColIndexById("strRepAcc");

    var poDetailId = gObj.getColIndexById("strPONumDtlsId");
    var poStatus = gObj.getColIndexById("strPOStatus");
    var chkBoxStatus = gObj.getColIndexById("strChkHoldFl");
    var divisionNm = gObj.getColIndexById("strDivisionNm");
      var updatedBy = gObj.getColIndexById("strPOStatusUpdatedBy");
    var updatedDate = gObj.getColIndexById("strPOStatusUpdatedDate");
    var custPOUpdatedBy = gObj.getColIndexById("strCustPOUpdatedBy");
    
    if (setInitWidths != "")
        gObj.setInitWidths(setInitWidths);
    if (setColAlign != "")
        gObj.setColAlign(setColAlign);
    if (setColTypes != "")
        gObj.setColTypes(setColTypes);
    if (setColSorting != "")
        gObj.setColSorting(setColSorting);
    if (enableTooltips != "")
        gObj.enableTooltips(enableTooltips);
    if (setFilter != "")
        gObj.attachHeader(setFilter);
    if (footerArry != undefined && footerArry.length > 0) {
        var footstr = eval(footerArry).join(",");
        if (footerStyles != undefined && footerStyles.length > 0)
            gObj.attachFooter(footstr, footerStyles);
        else
            gObj.attachFooter(footstr);
    }
    if (footerExportFL)
    fnInvoiceAttachExptFooter(divRef, colCount, gObj);
    if (gridHeight != "")
        gObj.enableAutoHeight(true, gridHeight, true);
    else
        gObj.enableAutoHeight(true, 550, true);
   
    gObj.setNumberFormat("0,000.00",total);
    gObj.setNumberFormat("0,000.00",poamount);
    gObj.setNumberFormat("0,000.00",diffamount);
    gObj.setSkin("dhx_skyblue");
    gObj.init();
    
    gObj.setColumnHidden(repAccNm, true);
   //  gObj.setColumnHidden(ParentAcc, true);
   gObj.setColumnHidden(divisionNm, true);
     gObj.setColumnHidden(FieldSales, true);
     gObj.setColumnHidden(poDetailId, true);
     gObj.setColumnHidden(poStatus, true);
     gObj.setColumnHidden(chkBoxStatus, true);
     gObj.setColumnHidden(updatedBy, true);
     gObj.setColumnHidden(updatedDate, true);
     gObj.setColumnHidden(custPOUpdatedBy, true);
    gObj.enableHeaderMenu();


    var newJSONData = JSON.stringify(gridData);
    var dataResult = "{\"data\":" + newJSONData + "}";
    dataResult = JSON.parse(dataResult);
    var gridParseData = dataResult;

    
    gObj.parse(gridParseData, "js");
    return gObj;
}
/*
Function is used load the Grid For DiscrepencyDashboard
*/
function loadDHTMLXGridForDiscrepency(divRef, gridHeight, gridData, setColIds, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL, roeSplitAt) {
    var gObj = new dhtmlXGridObject(divRef);
    gObj.enableColSpan(true);
    gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
    if (setHeader != undefined && setHeader.length > 0) {
        gObj.setHeader(setHeader);
        var colCount = setHeader.length;
        colCount = 19;
    }
    if (setColIds != "")
        gObj.setColumnIds(setColIds);
    var orderamt     = gObj.getColIndexById("strTotal");
    var poamount = gObj.getColIndexById("strPOAmt");
    var diffamount = gObj.getColIndexById("strDiffAmt");
    var resDate     = gObj.getColIndexById("strResDate");
    var disDate = gObj.getColIndexById("strDisdate");
    var CollectorNm = gObj.getColIndexById("strCollectorNm");
    var PODetailId = gObj.getColIndexById("strPONumDtlsId");
    var defCollectorNm = gObj.getColIndexById("strDefaultCollector");
     var receiveMode = gObj.getColIndexById("strOrdReceiveMode");
     var parenordid = gObj.getColIndexById("strParentOrdId");
     var adName = gObj.getColIndexById("strADName");
     var vpName = gObj.getColIndexById("strVPName");
     var poEnteredDate = gObj.getColIndexById("strPOEnteredDate");
     var custPOUpdatedBy = gObj.getColIndexById("strCustPOUpdatedBy");
     var poResolvefl = gObj.getColIndexById("strPOResolvefl");
     var poDiscRemdate = gObj.getColIndexById("strPODiscRemdate");
    
    if (setInitWidths != "")
        gObj.setInitWidths(setInitWidths);
    if (setColAlign != "")
        gObj.setColAlign(setColAlign);
    if (setColTypes != "")
        gObj.setColTypes(setColTypes);
    if (setColSorting != "")
        gObj.setColSorting(setColSorting);
    if (enableTooltips != "")
        gObj.enableTooltips(enableTooltips);
    if (setFilter != "")
        gObj.attachHeader(setFilter);
    if (footerArry != undefined && footerArry.length > 0) {
        var footstr = eval(footerArry).join(",");
        if (footerStyles != undefined && footerStyles.length > 0)
            gObj.attachFooter(footstr, footerStyles);
        else
            gObj.attachFooter(footstr);
    }
    if (footerExportFL)
        fnAttachExptFooter(divRef, colCount, gObj);
    if (gridHeight != "")
        gObj.enableAutoHeight(true, gridHeight, true);
    else
        gObj.enableAutoHeight(true, 550, true);
   
    gObj.setNumberFormat("0,000.00",orderamt);
    gObj.setNumberFormat("0,000.00",poamount);
    gObj.setNumberFormat("0,000.00",diffamount);
    gObj.setSkin("dhx_skyblue");
    gObj.init();
    gObj.setColumnHidden(resDate, true);
    gObj.setColumnHidden(disDate, true);
    gObj.setColumnHidden(CollectorNm, true);
    gObj.setColumnHidden(PODetailId, true);
    gObj.setColumnHidden(defCollectorNm, true);
    gObj.setColumnHidden(receiveMode, true);
    gObj.setColumnHidden(parenordid, true);
    gObj.setColumnHidden(adName, true);
    gObj.setColumnHidden(vpName, true);
    gObj.setColumnHidden(poEnteredDate, true);
    gObj.setColumnHidden(custPOUpdatedBy, true);
    gObj.setColumnHidden(poResolvefl, true);
    gObj.setColumnHidden(poDiscRemdate, true);

    gObj.enableHeaderMenu();


    var newJSONData = JSON.stringify(gridData);
    var dataResult = "{\"data\":" + newJSONData + "}";
    dataResult = JSON.parse(dataResult);
    var gridParseData = dataResult;

    
    gObj.parse(gridParseData, "js");
    return gObj;
}

/*
Function is used load the Grid For DiscrepencyDashboard
*/
var combo;
function loadDHTMLXGridForLineItemEntry(divRef, gridHeight, gridData, setColIds, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, footerArry, footerStyles, footerExportFL, roeSplitAt, setFilter, gridPOArr, POArrStatus) {
    
	
    var gObj = new dhtmlXGridObject(divRef);
	
    gObj.enableColSpan(true);
    gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
    if (setHeader != undefined && setHeader.length > 0) {
        gObj.setHeader(setHeader);
        var colCount = setHeader.length;
    }
    if (setColIds != "")
        gObj.setColumnIds(setColIds);
    var type = gObj.getColIndexById("strDescTypeId");
    var status = gObj.getColIndexById("strStatusId");
    var doAmt = gObj.getColIndexById("strExtDoAmt");
    var poAmt = gObj.getColIndexById("strExtPoAmt");  
    var itemPrice = gObj.getColIndexById("strItemPrice");
    var poPrice = gObj.getColIndexById("strPOPrice");
    var orderId = gObj.getColIndexById("strOrderid");
    //set drop down values for grid body to show potype
  	combo = gObj.getCombo(type);
  	combo.clear();
  	for(var i=0;i<gridPOArr.length;i++){
  		combo.put(gridPOArr[i][0],gridPOArr[i][1]);
  	}
  	combo.save();
  	
  	combo = gObj.getCombo(status);
  	combo.clear();
  	for(var i=0;i<POArrStatus.length;i++){
  		combo.put(POArrStatus[i][0],POArrStatus[i][1]);
  	}
  	combo.save();
  	
    if (setInitWidths != "")
        gObj.setInitWidths(setInitWidths);
    if (setColAlign != "")
        gObj.setColAlign(setColAlign);
    if (setColTypes != "")
        gObj.setColTypes(setColTypes);
    if (setColSorting != "")
        gObj.setColSorting(setColSorting);
    if (enableTooltips != "")
        gObj.enableTooltips(enableTooltips);
    if (setFilter != "")
        gObj.attachHeader(setFilter);
    if (footerArry != undefined && footerArry.length > 0) {
        var footstr = eval(footerArry).join(",");
        if (footerStyles != undefined && footerStyles.length > 0)
            gObj.attachFooter(footstr, footerStyles);
        else
            gObj.attachFooter(footstr);
    }
    
    if (footerExportFL)
        fnAttachExptFooter(divRef, colCount, gObj);

    if (gridHeight != "")
        gObj.enableAutoHeight(true, gridHeight, true);
    else
        gObj.enableAutoHeight(true, 550, true);
    gObj.setNumberFormat("0,000.00",doAmt);
    gObj.setNumberFormat("0,000.00",poAmt);
    gObj.setNumberFormat("00,000.00",itemPrice);
    gObj.setNumberFormat("00,000.00",poPrice);
    gObj.setSkin("dhx_skyblue");
    gObj.init();
    gObj.setColumnHidden(orderId, true);
   

    var newJSONData = JSON.stringify(gridData);
    var dataResult = "{\"data\":" + newJSONData + "}";
    dataResult = JSON.parse(dataResult);
    var gridParseData = dataResult;
    
    gObj.parse(gridParseData, "js");
    return gObj;
}

/*
Function is used load the Grid For Discrepency Detail Report
*/
function loadDHTMLXGridForDiscrepencyDtlRpt(divRef, gridHeight, gridData, setColIds, setHeader, setInitWidths, setColAlign, setColTypes, setColSorting, enableTooltips, setFilter, footerArry, footerStyles, footerExportFL, roeSplitAt) {
    var gObj = new dhtmlXGridObject(divRef);
    gObj.enableColSpan(true);
    gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
    if (setHeader != undefined && setHeader.length > 0) {
        gObj.setHeader(setHeader);
        var colCount = setHeader.length;
        colCount = 19;
    }
    if (setColIds != "")
        gObj.setColumnIds(setColIds);
    var strDOItemPrice  = gObj.getColIndexById("strDOItemPrice");
    var strExtDoAmt = gObj.getColIndexById("strExtDoAmt");
    var strPOItemPrice = gObj.getColIndexById("strPOItemPrice");
    var strExtPoAmt = gObj.getColIndexById("strExtPoAmt");
    var strResDate = gObj.getColIndexById("strResDate");
    var strDisdate = gObj.getColIndexById("strDisdate");
    
    
    if (setInitWidths != "")
        gObj.setInitWidths(setInitWidths);
    if (setColAlign != "")
        gObj.setColAlign(setColAlign);
    if (setColTypes != "")
        gObj.setColTypes(setColTypes);
    if (setColSorting != "")
        gObj.setColSorting(setColSorting);
    if (enableTooltips != "")
        gObj.enableTooltips(enableTooltips);
    if (setFilter != "")
        gObj.attachHeader(setFilter);
    if (footerArry != undefined && footerArry.length > 0) {
        var footstr = eval(footerArry).join(",");
        if (footerStyles != undefined && footerStyles.length > 0)
            gObj.attachFooter(footstr, footerStyles);
        else
            gObj.attachFooter(footstr);
    }
    if (footerExportFL)
        fnAttachExptFooter(divRef, colCount, gObj);
    if (gridHeight != "")
        gObj.enableAutoHeight(true, gridHeight, true);
    else
        gObj.enableAutoHeight(true, 550, true);
   
     gObj.setNumberFormat("0,000.00",strDOItemPrice);
     gObj.setNumberFormat("0,000.00",strPOItemPrice);
     gObj.setNumberFormat("00,000.00",strExtDoAmt);
     gObj.setNumberFormat("00,000.00",strExtPoAmt);
     gObj.setSkin("dhx_skyblue");
     gObj.init();
     gObj.setColumnHidden(strResDate, true);
     gObj.setColumnHidden(strDisdate, true);

     gObj.enableHeaderMenu();


    var newJSONData = JSON.stringify(gridData);
    var dataResult = "{\"data\":" + newJSONData + "}";
    dataResult = JSON.parse(dataResult);
    var gridParseData = dataResult;

    
    gObj.parse(gridParseData, "js");
    return gObj;
}