var varmonth = ["JAN", "FEB", "MAR","APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];

var varCurrent_datetime = new Date();
var dd = varCurrent_datetime.getDate();
var mm = varCurrent_datetime.getMonth()+1; 
var yyyy = varCurrent_datetime.getFullYear();
if(dd<10) 
{
    dd='0'+dd;
} 

if(mm<10) 
{
    mm='0'+mm;
} 
var currentDate = mm+'/'+dd+'/'+yyyy;
var startDate = mm+'/'+"01"+'/'+yyyy;


var date = new Date();
var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
var am_pm = date.getHours() >= 12 ? "PM" : "AM";
hours = hours < 10 ? "0" + hours : hours;
var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
var time = hours + ":" + minutes + ":" + seconds + " " + am_pm;
var varFormatted_date = varCurrent_datetime.getDate() + "-" + varmonth[varCurrent_datetime.getMonth()] + "-" + varCurrent_datetime.getFullYear();
var dateTime = varFormatted_date+' '+time;
var varCTradeshowName  = '';
var varCStartDate      = '';
var varCEndDate      = '';
var varCDateComparison = '';
var varCDateComAlert = '';
var varCEndDate = '';
var varCStartDate = '';
var varCChooseOne = '';
var varCPending = '';
var varCApproved='';
var varCInprogress='';
var varCRejected='';
var varCCompleted='';
var varTSUpdate='';
var varCMemberStatus = '';
var varCFirstName = '';
var varCLastName = '';
var varCEmail = '';
var varCNewTitle='';
var varCEditTitle='';
var varCAlreadyExists='';
var varCVoidFlag='';
var varCRegistered='';
var varCCheckedIn='';
var varCTSInsert='';
var varCRemove='';
var varCBrowser='';
var varCInvalid='';
var varCValidEmail='';
var varCDate='';
var varCTemplate = '';
var varCRequired = '';
var varCFname = '';
var varCLname = '';
var varCDuplicateCSV = '';
var wikiUrl = "https://geni.globusmedical.com";
var wikipendingpo = "/pages/viewpage.action?pageId=49283830";
var wikiinvoicedashboard ="/display/ER/ARPO_002-Ready+for+Invoicing+Dashboard";
var wikidiscrepancydashboard = "/display/ER/ARPO_003.1-Discrepant+PO+Detail+Entry";
var wikilineitemdetails = "/display/ER/ARPO_003.2-PO+Discrepancy-Line+Item+Entry";
var wikidiscrepancydetailreport="/display/ER/ARPO_003.3-PO+Discrepancy-Detailed+Report";
  