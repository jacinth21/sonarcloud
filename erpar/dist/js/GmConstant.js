var urlIndex = "https://erptest.globusone.com/erpar/";
var urlMicroapp = "https://erptest.globusone.com/erpmicroappcommon/";
var url = urlIndex+"api/";
var microappUrl = urlMicroapp+"api/";
var microappUrlFile = urlMicroapp+"apiFiles/";
var varJsonURL = urlIndex+"dist/app/GmMessage.json";
var accessTokenLifespanInMinutes = 5; // In Minutes accessTokenLifespanInMinutes
var fileUploadPath = "////sw80app03-t//spineitfiles//Appnode//do_document//";
/* Keycloak Values*/
var keycloakConfig = {url: "https://logintest.globusone.com/auth/", realm: "globus-medical", clientId: "AR-Client"};